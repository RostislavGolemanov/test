package sfs2x.extensions.games.ludo;

import java.util.Map;

import com.smartfoxserver.v2.core.ISFSEvent;
import com.smartfoxserver.v2.core.SFSEventParam;
import com.smartfoxserver.v2.core.SFSEventType;
import com.smartfoxserver.v2.entities.Room;
import com.smartfoxserver.v2.entities.User;
import com.smartfoxserver.v2.entities.data.ISFSObject;
import com.smartfoxserver.v2.entities.data.SFSObject;
import com.smartfoxserver.v2.exceptions.SFSException;
import com.smartfoxserver.v2.extensions.BaseServerEventHandler;
import java.util.ArrayList;
import java.io.*;
import java.util.logging.Level;
import java.util.logging.Logger;

public class OnUserGoneHandler extends BaseServerEventHandler
{
	@SuppressWarnings("unchecked")
        @Override 
        public void handleServerEvent(ISFSEvent event) throws SFSException
	{
		LudoExtension gameExt = (LudoExtension) getParentExtension();
		 
		// Get event params
		User user = (User) event.getParameter(SFSEventParam.USER);
                Integer oldPlayerId=null;
		int rId=user.getVariable("playIn").getIntValue();
                
                gameExt.setUserGone(user,rId);
                
               
	}
}
