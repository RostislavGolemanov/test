package sfs2x.extensions.games.ludo;

import com.smartfoxserver.v2.SmartFoxServer;
import com.smartfoxserver.v2.annotations.Instantiation;
import com.smartfoxserver.v2.annotations.Instantiation.InstantiationMode;
import com.smartfoxserver.v2.api.ISFSBuddyApi;
import com.smartfoxserver.v2.buddylist.BuddyList;
import com.smartfoxserver.v2.entities.Room;
import com.smartfoxserver.v2.entities.User;
import com.smartfoxserver.v2.entities.data.ISFSArray;
import com.smartfoxserver.v2.entities.data.ISFSObject;
import com.smartfoxserver.v2.entities.data.SFSArray;
import com.smartfoxserver.v2.entities.data.SFSObject;
import com.smartfoxserver.v2.entities.variables.RoomVariable;
import com.smartfoxserver.v2.entities.variables.SFSRoomVariable;
import com.smartfoxserver.v2.entities.variables.SFSUserVariable;
import com.smartfoxserver.v2.entities.variables.UserVariable;
import com.smartfoxserver.v2.exceptions.SFSBuddyListException;
import com.smartfoxserver.v2.exceptions.SFSRuntimeException;
import com.smartfoxserver.v2.exceptions.SFSVariableException;
import com.smartfoxserver.v2.extensions.BaseClientRequestHandler;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.Iterator;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

@Instantiation(InstantiationMode.SINGLE_INSTANCE)
public class acceptInvite extends BaseClientRequestHandler {

	@Override
	public void handleClientRequest(User user, ISFSObject params) {
		// Check params
		LudoExtension gameExt = (LudoExtension) getParentExtension();

		int RID = user.getVariable("playIn").getIntValue();

		roomInfo ROOM = gameExt.gRooms.get(RID);
		ISFSObject respObj2 = new SFSObject();
		if (ROOM != null) {

			if (!ROOM.gameStarted) {
				Room gameRoom = ROOM.ROOM;
				ROOM.bet[user.getPlayerId() - 1] = 100;
				respObj2.putUtfString("u", user.getName());
				respObj2.putUtfString("un", user.getVariable("uName").getStringValue());
				send("AG", respObj2, gameRoom.getUserList());
			} else {
				respObj2.putInt("NA", 1);
				send("AG", respObj2, user);
			}

		} else {
			respObj2.putInt("NA", 2);
			send("AG", respObj2, user);
		}

	}

}
