/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package sfs2x.extensions.games.ludo;

/**
 *
 * @author CT
 */
import com.smartfoxserver.v2.SmartFoxServer;
import com.smartfoxserver.v2.api.CreateRoomSettings;
import com.smartfoxserver.v2.api.ISFSBuddyApi;
import java.util.HashMap;
import java.util.ArrayList;

import com.smartfoxserver.v2.db.IDBManager;
import com.smartfoxserver.v2.core.SFSEventType;
import com.smartfoxserver.v2.entities.Room;
import com.smartfoxserver.v2.entities.SFSRoomRemoveMode;
import com.smartfoxserver.v2.entities.SFSRoomSettings;
import com.smartfoxserver.v2.entities.User;
import com.smartfoxserver.v2.entities.data.ISFSArray;

import com.smartfoxserver.v2.entities.data.ISFSObject;
import com.smartfoxserver.v2.entities.data.SFSArray;

import com.smartfoxserver.v2.entities.data.SFSObject;
import com.smartfoxserver.v2.entities.variables.RoomVariable;
import com.smartfoxserver.v2.entities.variables.SFSRoomVariable;
import com.smartfoxserver.v2.entities.variables.SFSUserVariable;
import com.smartfoxserver.v2.entities.variables.UserVariable;
import com.smartfoxserver.v2.exceptions.SFSBuddyListException;
import com.smartfoxserver.v2.exceptions.SFSCreateRoomException;
import com.smartfoxserver.v2.exceptions.SFSJoinRoomException;
import com.smartfoxserver.v2.extensions.SFSExtension;
import java.io.DataOutputStream;
import java.io.IOException;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.UUID;
import java.sql.SQLException;
import java.util.Arrays;
import java.util.Date;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.TimerTask;
import java.util.Timer;
import java.util.logging.Level;
import java.util.logging.Logger;

public class LudoExtension extends SFSExtension{

       
	 
	private volatile boolean gameStarted;
	 
	public Room Lobby;
       // private HashMap<Integer,String> allUsers;
        public int Lobby_Id=0;
	private final String version = "1.0.7";
	//public ArrayList<User> lobbyUsers;
      //  public ArrayList<User> gameUsers;
        public ArrayList<User> AutoJoinList;
        public HashMap<Integer,roomInfo> gRooms;
        public HashMap<Integer,Integer> freeRooms;
        public HashMap<Integer,Integer> privateRooms;
        public SFSArray bArray=new SFSArray();
        int gID=5348;
        int roomPlayersNum=4;
        int usersCount=0;
        int squadNum=28;
        int shieldInt=3;
        int geminiInt=4;
        int numSticky=3;
        int gameTime=5;//min 5
        int longTime=6;//min
        int addPointsOut=5;
        int addPointsIn=30;
        int addPointsHit=15;
        int slotPrice=5000;
        int startCoins=1000;
        int ghostDuration=3;
        int waitTimeBoosters=15;
        int updateConfigTime=30;
        int TIME_TO_PAY = 90;
        int FREE_PAY_VALUE = 400;
        int MAX_GAMES_PLAYED_PER_DAY = 7;
        int MIN_MONEY_FOR_PAY = 50;
        int sixChance=4;
        int winBet=800;
        int[] teleports={3,6,10,13,17,20,24,27};
        int[] extraT={4,11,18,25};
        int[] boosterPrice={0,0,0,0,0,0,0,0,0,0};
        String bustpriceSend="";
        Timer countTimer;
        public int numPawns=3;
        public String StatsStr="";
         
       
        
        String urlS ="http://mobile.imperiaonline.org/mms/MMobileStats/index.php";//"https://fb.imperiaonline.org/ludo/jsonCheck.php";// 
       
        
       
	@Override
	public void init()
	{
		trace("Ludo game Extension 12/10 for SFS2X started, rel. " + version);
		 
              
                gRooms = new HashMap<>();
                freeRooms = new HashMap<>();
                privateRooms= new HashMap<>();
                 
		Lobby=this.getParentZone().getRoomByName("The_Lobby");
	        Lobby_Id=Lobby.getId();
                
                addRequestHandler("aJ", autoJoinGame.class);//Save setts
                addRequestHandler("jG", joinGame.class);//Save setts
                addRequestHandler("sC", setColor.class);//Save setts
                addRequestHandler("sCh", sendChat.class);//Save setts
                addRequestHandler("rD", rollDice.class);//Roll Dice
                addRequestHandler("CT", changeTurn.class);//Roll Dice
                addRequestHandler("mP", movePawn.class);//Move Pawn
                addRequestHandler("sB", setBomb.class);//Set Bomb
                addRequestHandler("sSt", setSticky.class);//Set Sticky
                addRequestHandler("sGm", setGemini.class);//Set Gemini
                addRequestHandler("sS", setShield.class);//Set Shield
                addRequestHandler("bK", busterKill.class);//Buster Kill
                addRequestHandler("WL", writeLog.class);//Write Log
                addRequestHandler("sG", startGame.class);//Set Bomb
                addRequestHandler("sI", setInvisible.class);//Buster Invisible
                addRequestHandler("sBs", setBust.class);//Set Busters
                addRequestHandler("buyB", buyBust.class);//Buy Busters
                addRequestHandler("buyA", buyAnim.class);//Buy Animation
                addRequestHandler("setA", setAnim.class);//Set Animation
                addRequestHandler("setSound", setSound.class);//Set Animation
                addRequestHandler("LG", leaveRoom.class);//Set Animation
                addRequestHandler("bS", buySlot.class);//Buy Slot
                addRequestHandler("wF", wheelOfFortune.class);//Set Animation
                addRequestHandler("hH", hideHints.class);//Set Animation
                addRequestHandler("gD", getDeck.class);//get DECK
                addRequestHandler("oM", oneMinute.class);//get DECK
                addRequestHandler("pRoom", privateRoom.class);//get DECK
                addRequestHandler("aB", addBuddies.class);//add Buddies
                addRequestHandler("getBusters", getBusters.class);//add Buddies
                addRequestHandler("getShop", getShop.class);//add Buddies
                addRequestHandler("getAnim", getAnim.class);//add Buddies
                addRequestHandler("purchase", Purchase.class);//add Buddies
                addRequestHandler("aR", acceptInvite.class);//add Buddies
                addRequestHandler("addErr", insertJS_Error.class);//add Buddies
                addRequestHandler("gM", getMoney.class);//displays user's money
                addEventHandler(SFSEventType.USER_LOGIN , OnUserLogInHandler.class);
                addEventHandler(SFSEventType.USER_JOIN_ZONE , setUserPropsHandler.class);
                addEventHandler(SFSEventType.USER_DISCONNECT, OnUserGoneHandler.class);
                addEventHandler(SFSEventType.USER_LEAVE_ROOM, OnUserLeaveRoom.class);
                addEventHandler(SFSEventType.USER_JOIN_ROOM, OnUserJoin.class);
                addEventHandler(SFSEventType.ROOM_REMOVED, OnRoomRemovedHandler.class);
                //addEventHandler(SFSEventType.BUDDY_LIST_INIT, OnBuddyInitHandler.class);
          
                getSetts();
                this.getParentZone().getBuddyListManager().setActive(true);
                setTimerUCount();
            
           
            
	}
	
        void setTimerUCount(){
            countTimer = new Timer();
            countTimer.schedule(new TimerTask() {
             @Override
                public void run() {
                 try {
                     checkConfig();
                     sendStatisticUserCount();
                     
                 } catch (IOException ex) {
                     trace("ERROR"+ex);
                     Logger.getLogger(LudoExtension.class.getName()).log(Level.SEVERE, null, ex);
                 }
                    
                }
             }, 0,60000);
        }
        
	
        
        @Override
	public void destroy() 
	{
            
             
             Set set = gRooms.entrySet();
             Iterator i = set.iterator();
             while(i.hasNext()) {
                 Map.Entry entry = (Map.Entry) i.next();
                 roomInfo ROOM = (roomInfo)entry.getValue();
                 ROOM.remove();
             }
             if(countTimer!=null){
                   countTimer.cancel();
                   countTimer.purge();
             }
           super.destroy();
	}
		
        
   
   private void checkConfig(){
       
       updateConfigTime--;
       if(updateConfigTime<1){
           updateConfigTime=30;
            getSetts();
       }
       
       
   }
        
        
   
   
   public String setUserStr(User user){
           //0-uid
           //1-uname
           //2-thumb
       String usrString=user.getId()+"|"+user.getName()+"|"+user.getVariable("uThumb").getValue();
       return  usrString;   
   } 
        
    
  
    public void setUserGone(User user, int rId){
        
         try{
           
             
             int rID=rId;
            
            if(rID!=Lobby_Id){// NOT LOBBY
                
                
             roomInfo ROOM=gRooms.get(rID);
             
             if(ROOM!=null){
                 
              int UID=ROOM.removeUser(user.getId());
              
              String fbId=ROOM.fbIds[UID-1];
              ROOM.fbIds[UID-1]="";
              Room gameRoom=getParentZone().getRoomById(rID);
              if(gameRoom!=null){
                    try {
                        sendStatistic(fbId,"sessionFinished",ROOM.GID,user.getVariable("LT").getIntValue());
                     } catch (IOException ex) {
                       Logger.getLogger(LudoExtension.class.getName()).log(Level.SEVERE, null, ex);
                     }
             if(ROOM.gameStarted){//GAME STARTED
             
               //////////////WRITE USER EXIT GAME REQUEST
                   
                 if(!"".equals(fbId)){
                        int myPoints=ROOM.MovesCount[UID-1];
                         
                        int maxPoints=myPoints;
                        for(int c=0;c<4;c++){
                            if(ROOM.MovesCount[c]>maxPoints){
                                maxPoints=ROOM.MovesCount[c];
                            }
                        }
                        int sendPoints=maxPoints-myPoints;
                       
                      try {
                       
                       sendStatisticF(fbId,"finishedBeforeEnd",ROOM.GID,sendPoints,user.getVariable("LT").getIntValue());
                       
                     } catch (IOException ex) {
                       Logger.getLogger(LudoExtension.class.getName()).log(Level.SEVERE, null, ex);
                     }
                  }
               ///////////////WRITE USER EXIT GAME REQUEST
                    if(ROOM.currPlayers>0&&ROOM.realPlayers>0){ 

                        int TURN=gameRoom.getVariable("Trn").getIntValue();
                        int NUMTOGO=1;
                        if(!ROOM.gameFinished){
                           ISFSObject respObj2 = new SFSObject();
                           respObj2.putInt("c", UID);

                           if(TURN==UID){
                                int N=0;
                               do{
                                     if(TURN<ROOM.currPlayers+1)TURN+=1;
                                     else                       TURN =1;
                                       N++;
                               }while("".equals(ROOM.players[TURN-1])&&N<6);

                              //  int[]  pawnsOut =convertSFSArray(gameRoom.getVariable("nOut").getSFSArrayValue());
                                int[]  pawnsFinished =convertSFSArray(gameRoom.getVariable("nFin").getSFSArrayValue());


                               if(ROOM.pawnsOUT[TURN-1]+pawnsFinished[TURN-1]==ROOM.numPawns){
                                        NUMTOGO=3;
                                    }
                           }

                            ROOM.players[UID-1]="0|"+user.getVariable("uName").getStringValue()+"|a|p";
                            ROOM.pIds[UID-1]=-1;



                           respObj2.putInt("t", TURN);
                           respObj2.putUtfString("n", user.getVariable("uName").getStringValue());
                           RoomVariable Trn=new SFSRoomVariable("Trn",TURN,true,true,true);    // Whose Turn
                           RoomVariable nG=new SFSRoomVariable("nG",NUMTOGO,true,true,true);
                           gameRoom.setVariables(Arrays.asList(Trn,nG));
                           send("LG", respObj2, gameRoom.getUserList());
                            ROOM.setTimerTurn(TURN);
                        }
                    }else{

                        ROOM.remove();
                        if(gRooms.get(rID)!=null){
                            gRooms.remove(rID);
                        }
                        if(freeRooms.get(rID)!=null){
                               freeRooms.remove(rID);
                           }

                      }
             
             }//GAME STARTED
             else{
                 
                 if(ROOM.currPlayers<1){
                     ROOM.remove();
                    if(gRooms.get(rID)!=null){
                        gRooms.remove(rID);
                    }
                    if(freeRooms.get(rID)!=null){
                        freeRooms.remove(rID);
                    }
                    if(privateRooms.get(rID)!=null){
                        privateRooms.remove(rID);
                    }
                  
                 }else{
                     ISFSObject respObj3 = new SFSObject();
                     String botName="Shiny";
                     if(!ROOM.isPrivate){
                      String[] bNames={"Shiny","Lucky","Friendly","Bossy","Dandy","Jolly","Risky","Gravy","Fizzy","Crunky","Spiffy","Skippy","Hoopy"};
                      int nameN=(int)(Math.random()*bNames.length);
                      botName=bNames[nameN];
                      ROOM.players[UID-1]="0|"+bNames[nameN]+"|a|p";
                      ROOM.pIds[UID-1]=-1;
                      
                     }else if(UID==1){
                         
                         List uList=gameRoom.getUserList();
                         for(int usr=0;usr<uList.size();usr++){
                             User userL=(User)uList.get(usr);
                             try {
                                 getApi().joinRoom(userL, Lobby, "",false,gameRoom,false,true);
                             } catch (SFSJoinRoomException ex) {
                                 Logger.getLogger(LudoExtension.class.getName()).log(Level.SEVERE, null, ex);
                             }
                         }
                     }else{
                          String[] bNames={"Shiny","Lucky","Friendly","Bossy","Dandy","Jolly","Risky","Gravy","Fizzy","Crunky","Spiffy","Skippy","Hoopy"};
                      int nameN=(int)(Math.random()*bNames.length);
                      botName=bNames[nameN];
                      ROOM.players[UID-1]="0|"+bNames[nameN]+"|a|p";
                      ROOM.pIds[UID-1]=-1;
                     }
                      //trace("USER GONE "+user.getName()+"|"+UID);
		     respObj3.putInt("c", UID);
                     respObj3.putInt("t", 0);
                     respObj3.putUtfString("un", user.getName());
                     respObj3.putUtfString("n", botName);
                     send("LG", respObj3, gameRoom.getUserList()); 
                     
                 }
               }
              }else{
                  if(freeRooms.get(rID)!=null){
                        freeRooms.remove(rID);
                    }
                  if(privateRooms.get(rID)!=null){
                        privateRooms.remove(rID);
                    }
              }
             }else{
                  if(freeRooms.get(rID)!=null){
                        freeRooms.remove(rID);
                    }
                  if(privateRooms.get(rID)!=null){
                        privateRooms.remove(rID);
                    }
                  return;
             }
            }else{// NOT LOBBY
                     try {
                         String fbId=user.getVariable("uId").getStringValue();
                         sendStatistic(fbId,"sessionFinished",null,user.getVariable("LT").getIntValue());
                     } catch (IOException ex) {
                       Logger.getLogger(LudoExtension.class.getName()).log(Level.SEVERE, null, ex);
                     }
           }
         }catch(Error e){trace("error removing user");}
         
          
           //  allUsers.remove(user.getId());
             
    }
    
    
        
     public void autoJoin(User user, int time, int bet, int b1, int b2, int b3, int a1, int a2, int a3, int a4, int a5){
        
      
        Set set = freeRooms.entrySet();
        Iterator i = set.iterator();
        Boolean found=false;
        int playTime=time*60;
        roomInfo room=null;
        int myMoney=user.getVariable("money").getIntValue();
        int real_money=user.getVariable("rM").getIntValue();
       //trace("AJ "+user.getVariable("uId").getValue()+"|"+user.getVariable("uName").getStringValue()+"|"+user.getVariable("uThumb").getValue()+"|"+user.getPlayerId()+"|"+user.getVariable("VIP").getIntValue());
         if(myMoney+real_money>=bet){   
        
        while(i.hasNext()) {
            
             Map.Entry entry = (Map.Entry) i.next();
             int RID = (int)entry.getValue();
             
              room = (roomInfo)gRooms.get(RID);
             
            
           if(room.numPlayers>room.currPlayers&&(room.gameTime==playTime||playTime==0)){
               
               Room sfsRoom=getParentZone().getRoomById(room.RID);
               try{
                  
                     getApi().joinRoom(user,sfsRoom, "",false,Lobby);
                     UserVariable playIn = new SFSUserVariable("playIn",room.RID,true);
                     // UserVariable money = new SFSUserVariable("money",myMoney-bet,true);
                     room.players[user.getPlayerId()-1]=user.getVariable("uId").getValue()+"|"+user.getVariable("uName").getStringValue()+"|"+user.getVariable("uThumb").getValue()+"|"+user.getPlayerId()+"|"+user.getVariable("VIP").getIntValue();
                     room.pIds[user.getPlayerId()-1]=user.getId();
                     room.fbIds[user.getPlayerId()-1]=user.getVariable("uId").getStringValue();
                     room.plIds[user.getPlayerId()-1]=user.getVariable("pId").getIntValue();
                     room.vips[user.getPlayerId()-1]=user.getVariable("VIP").getIntValue();
                     room.bet[user.getPlayerId()-1]=bet;
                     room.currPlayers++;
                     
                     room.animations[user.getPlayerId()-1][0]=a1;
                     room.animations[user.getPlayerId()-1][1]=a2;
                     room.animations[user.getPlayerId()-1][2]=a3;
                     room.animations[user.getPlayerId()-1][3]=a4;
                     room.animations[user.getPlayerId()-1][4]=a5;
                     getApi().setUserVariables(user, Arrays.asList(playIn));
                      
                     if(room.numPlayers==room.currPlayers){
                         room.setStartTime();
                         if(freeRooms.get(RID)!=null){
                            freeRooms.remove(RID);
                         }
                     }
                     found=true;
               }catch(SFSJoinRoomException err){
                    trace(err);
                }
               break;
           }
        }
        if(!found){
            if(time==0)time=gameTime;
            room= createGame(time,bet,user,b1,b2,b3,a1,a2,a3,a4,a5,false);
        }
        
                     try {
                       if(room!=null)sendStatistic(user.getVariable("uId").getStringValue(),"joinGame",room.GID,user.getVariable("LT").getIntValue());
                     } catch (IOException ex) {
                       Logger.getLogger(LudoExtension.class.getName()).log(Level.SEVERE, null, ex);
                     }
         }
    }
    
    
     
     public void joinPRoom(User user, int RID, int bet){
    
         roomInfo room = (roomInfo)gRooms.get(RID);
         Room sfsRoom=getParentZone().getRoomById(RID);
            
                   int a1=user.getVariable("A_1").getIntValue();
                   int a2=user.getVariable("A_2").getIntValue();
                   int a3=user.getVariable("A_3").getIntValue();
                   int a4=user.getVariable("A_4").getIntValue();
                   int a5=user.getVariable("A_5").getIntValue();
                     UserVariable playIn = new SFSUserVariable("playIn",room.RID,true);
                     
                     int plId=1;
                     for(int u=0;u<room.players.length;u++){
                         if(room.players[u]==""){
                             plId=u+1;
                             break;
                         }
                     }
                     room.players[plId-1]=user.getVariable("uId").getValue()+"|"+user.getVariable("uName").getStringValue()+"|"+user.getVariable("uThumb").getValue()+"|"+user.getPlayerId()+"|"+user.getVariable("VIP").getIntValue();
                     room.pIds[plId-1]=user.getId();
                     room.fbIds[plId-1]=user.getVariable("uId").getStringValue();
                     room.plIds[plId-1]=user.getVariable("pId").getIntValue();
                     room.vips[plId-1]=user.getVariable("VIP").getIntValue();
                     room.currPlayers++;
                     
                     room.animations[plId-1][0]=a1;
                     room.animations[plId-1][1]=a2;
                     room.animations[plId-1][2]=a3;
                     room.animations[plId-1][3]=a4;
                     room.animations[plId-1][4]=a5;
                     getApi().setUserVariables(user, Arrays.asList(playIn));
                     String Players=room.players[0];
                     
                      StringBuilder sb = new StringBuilder(room.players[0]);
                      sb.append(",");
                      sb.append(room.players[1]);
                      sb.append(",");
                      sb.append(room.players[2]);
                      sb.append(",");
                      sb.append(room.players[3]);
                     RoomVariable PLS=new SFSRoomVariable("PLS", sb.toString(),true,true,false);
                     sfsRoom.setVariables(Arrays.asList(PLS));
                    
                    try {
                        getApi().joinRoom(user,sfsRoom, "",false,Lobby);
                    } catch (SFSJoinRoomException ex) {
                        Logger.getLogger(LudoExtension.class.getName()).log(Level.SEVERE, null, ex);
                    }
                     
    }
    
    
     
     roomInfo createGame(int time, int bet,User user, int b1, int b2, int b3, int a1,int a2, int a3, int a4, int a5,Boolean isPrivate){
        CreateRoomSettings settings = new CreateRoomSettings();
         int myMoney=user.getVariable("money").getIntValue();
         int real_money=user.getVariable("rM").getIntValue();
         if(myMoney+real_money>=bet){
                long secs=new Date().getTime();
                settings.setName(secs+"_r");
                 
                settings.setMaxUsers(roomPlayersNum);
                settings.setMaxSpectators(10);
                 
		settings.setGame(true);
                settings.setDynamic(true);
                settings.setAutoRemoveMode(SFSRoomRemoveMode.WHEN_EMPTY);
                 
                settings.setMaxVariablesAllowed(30);
                               
                int Turn=1;
                int diceSide=6;
                 
                
                RoomVariable gS=new SFSRoomVariable("SG",0,true,true,false);                          // game Started
                RoomVariable nG=new SFSRoomVariable("nG",3,true,true,false);                          // num To Go
                RoomVariable dS=new SFSRoomVariable("dS",diceSide,true,true,false);                          // diceSide
                RoomVariable nP=new SFSRoomVariable("nP",numPawns,true,true,false);                          // Pawns per player
                RoomVariable nPl=new SFSRoomVariable("nPl",roomPlayersNum,true,true,false);                          // numPLayers
                RoomVariable hS=new SFSRoomVariable("hS",1,true,true,false);                          // Host
                RoomVariable sS=new SFSRoomVariable("sS","",true,true,false);                          // shieldSquads
                RoomVariable PR=new SFSRoomVariable("PR",isPrivate,false,true,false);                          // shieldSquads
                RoomVariable Trn=new SFSRoomVariable("Trn",Turn,true,true,false);                          // Whose Turn
                RoomVariable InArr=new SFSRoomVariable("InArr","0|0,0|0,0|0,0|0",true,true,false);                          // Whose Turn
                roomInfo ROOM=null;
                
		try{
                   
                   Room room=getApi().createRoom(getParentZone(), settings, null,false,null,false,true);
                
                   room.setFlag(SFSRoomSettings.USER_ENTER_EVENT, false);
                   room.setFlag(SFSRoomSettings.USER_EXIT_EVENT, false);
                   room.setFlag(SFSRoomSettings.USER_COUNT_CHANGE_EVENT, false);
                  
                    
                   ISFSArray outPawns = new SFSArray();
                   ISFSArray finishPawns = new SFSArray();
                   String DECK = "";
                   for(int d=0;d<squadNum;d++){  
                       DECK+="-1,";
                   }
                   RoomVariable DCK =new SFSRoomVariable("DCK",DECK,true,true,true);
                    
                      
                      for(int p=0;p<roomPlayersNum;p++){
                      outPawns.addInt(numPawns);
                      finishPawns.addInt(0);
                      }
                       RoomVariable nOut=new SFSRoomVariable("nOut",outPawns);
                       RoomVariable nFin=new SFSRoomVariable("nFin",finishPawns);// numPLayers
                  
                  room.setVariables(Arrays.asList(gS,hS,Trn,DCK,dS,nP,nG,nPl,nOut,nFin,InArr,sS,PR));
                       
                     getApi().joinRoom(user,room, "",false,Lobby);
                    
                      UserVariable playIn = new SFSUserVariable("playIn",room.getId(),true);
                      
                      getApi().setUserVariables(user, Arrays.asList(playIn));
                   
                    
                    ROOM=new roomInfo(room,isPrivate);
                    ROOM.gameTime=time*60;
                    ROOM.GID=UUID.randomUUID();
                    ROOM.bet[0]=bet;
                    ROOM.RID=room.getId();
                    ROOM.numPlayers=roomPlayersNum;
                    ROOM.currPlayers=1;
                    ROOM.players[0]=user.getVariable("uId").getValue()+"|"+user.getVariable("uName").getStringValue()+"|"+user.getVariable("uThumb").getValue()+"|"+user.getPlayerId()+"|"+user.getVariable("VIP").getIntValue();
                    
                    ROOM.pIds[0]=user.getId();
                    ROOM.vips[0]=user.getVariable("VIP").getIntValue();
                    ROOM.fbIds[0]=user.getVariable("uId").getStringValue();
                    ROOM.plIds[0]=user.getVariable("pId").getIntValue();
                   
                    if(b1>0)ROOM.busters[user.getPlayerId()-1][b1-1]++;
                    if(b2>0)ROOM.busters[user.getPlayerId()-1][b2-1]++;
                    if(b3>0)ROOM.busters[user.getPlayerId()-1][b3-1]++;
                    ROOM.isPrivate=isPrivate;
                    ROOM.animations[user.getPlayerId()-1][0]=a1;
                    ROOM.animations[user.getPlayerId()-1][1]=a2;
                    ROOM.animations[user.getPlayerId()-1][2]=a3;
                    ROOM.animations[user.getPlayerId()-1][3]=a4;
                    ROOM.animations[user.getPlayerId()-1][4]=a5;
                    for(int p=0;p<roomPlayersNum;p++){
                      ROOM.pawnsOUT[p]=numPawns;
                      }
                   gRooms.put(room.getId(), ROOM);
                   if(!isPrivate)freeRooms.put(room.getId(), room.getId());
                   else          privateRooms.put(room.getId(), room.getId());
                 
                  // AutoJoinList.clear();
                   
                  
                }catch(SFSCreateRoomException | SFSJoinRoomException err){
                    trace(err);
                }
                return ROOM;
         }else{
             return null;
         }
    }
    
     
     
    void lockRoom(int RID){
        if(freeRooms.get(RID)!=null){
               freeRooms.remove(RID);
         } 
         roomInfo ROOM=gRooms.get(RID);
          
         for(int u=0;u<ROOM.numPlayers;u++){
                        
                        int UID = ROOM.plIds[u];
                        if(UID!=0&&ROOM.pIds[u]!=-1){
                              
                        //String queryQF="UPDATE users, busters SET users.plays=users.plays+1, users.money=users.money-?, busters.b_1=busters.b_1-?, busters.b_2=busters.b_2-?, busters.b_3=busters.b_3-?, busters.b_4=busters.b_4-?, busters.b_5=busters.b_5-? WHERE users.fb_id=? and busters.fb_id=?;";
                         try{
                           User user=ROOM.ROOM.getUserByPlayerId(u+1);      
                          int money=user.getVariable("money").getIntValue();
                          int real_money=user.getVariable("rM").getIntValue();
                          int player_id=user.getVariable("pId").getIntValue();
                                 int virtualPrice;
                                 int realPrice;
                                 if(money<ROOM.bet[u]){
                                     virtualPrice=money;
                                     realPrice=ROOM.bet[u]-virtualPrice;
                                 }else{
                                     virtualPrice=ROOM.bet[u];
                                     realPrice=0;
                                 }
                                  int newMoney=money-virtualPrice;
                                  int newRMoney=real_money-realPrice;
                                  String TYPE="bet";
                                    
                                  
                                  
                                  
                               String query4="UPDATE `game_state`  SET `coins`=`coins`-?, plays=plays+1, `real_coins`=`real_coins`-?,`games_played`=`games_played`+1 WHERE `player_id`=?";
                                     Object[] array4 = new Object[3];
                                    array4[0] = virtualPrice;
                                    array4[1] = realPrice;
                                    array4[2] = player_id;
                                   // dbManager.executeUpdate(query4,array4);
                                    ISFSArray res=DBWRAPPER(query4, array4, 3);
                                insertPaymentLog Insert=new insertPaymentLog(user,ROOM.bet[u],"bet");
                           
                                   
                                   
                                 UserVariable uMoney = new SFSUserVariable("money",newMoney,true);
                                 UserVariable rMoney = new SFSUserVariable("rM",newRMoney,true);
                                  
                                 getApi().setUserVariables(user, Arrays.asList(uMoney,rMoney));
                         }catch(Exception e){}
                        }
                   }
    }
    
    
    
    
    
    void startGame(int RID){
        
            
                  if(freeRooms.get(RID)!=null){
                        freeRooms.remove(RID);
                    }
                   roomInfo ROOM=gRooms.get(RID);
                     
                   ROOM.gameStarted=true;
                   Date date=new Date();
                   int gIdInt=Math.round(date.getTime()/1000)-1420000000;
                   int randId=(int) Math.round(Math.random()*500);
                  
                    
                   ISFSObject resObj = new SFSObject();
		   resObj.putUtfString("ul", ROOM.players[0]+","+ROOM.players[1]+","+ROOM.players[2]+","+ROOM.players[3]);
                   resObj.putInt("np", ROOM.numPawns);
                   resObj.putInt("nt", ROOM.numPlayers);
                   resObj.putInt("ds", ROOM.diceSide);
                   String putArr=Arrays.toString(teleports);
                   putArr=putArr.replace(" ","");
                   resObj.putUtfString("tp", putArr);
                   String dirArr=Arrays.toString(ROOM.teleportDirection);
                   dirArr=dirArr.replace(" ","");
                   resObj.putUtfString("td",dirArr );
                   String animArr=Arrays.toString(ROOM.animations[0])+"|"+Arrays.toString(ROOM.animations[1])+"|"+Arrays.toString(ROOM.animations[2])+"|"+Arrays.toString(ROOM.animations[3]);
                   animArr=animArr.replace(" ","");
                   resObj.putUtfString("as", animArr);
                   String putArrE=Arrays.toString(extraT);
                   putArrE=putArrE.replace(" ","");
                   resObj.putUtfString("ex", putArrE);
                   int TURN=1;
                   ROOM.roomPlayers=ROOM.currPlayers;
                   if(ROOM.pIds[0]==0){
                       int N=0;
                        do{
                              if(TURN<ROOM.currPlayers+1)TURN+=1;
                              else                       TURN =1;
                                N++;
                        }while(ROOM.pIds[TURN-1]==0&&N<6);
                           RoomVariable Trn=new SFSRoomVariable("Trn",TURN,true,true,false);     
                         ROOM.ROOM.setVariables(Arrays.asList(Trn));
                   }
                   if(ROOM.currPlayers==4)ROOM.gameTime=60*longTime;
                   resObj.putInt("t", TURN);
                   resObj.putInt("gT", ROOM.gameTime);
                 
                  
                   
                   for(int u=0;u<ROOM.numPlayers;u++){
                        
                        int UID = ROOM.plIds[u];
                        if(UID!=0&&ROOM.pIds[u]!=-1){
                               
                              try {
                                   User user=ROOM.ROOM.getUserByPlayerId(u+1);
                                   noBusters(ROOM.busters[u],ROOM,user,u);
                                   int ludoType=user.getVariable("LT").getIntValue();
                                   String fbId=user.getVariable("uId").getStringValue();
                                   sendStatisticWinCoin(fbId,"BET",-1*ROOM.bet[u],ROOM.GID,ludoType);
                                   sendStatisticSG(user.getName(),"gameStarted",ROOM.GID,0,ROOM.numPlayers,user.getVariable("LT").getIntValue());
                              } catch (IOException ex) {
                               Logger.getLogger(LudoExtension.class.getName()).log(Level.SEVERE, null, ex);
                             } 
                         }
                   } 
                       
                   String bustArr=Arrays.toString(ROOM.busters[0])+"|"+Arrays.toString(ROOM.busters[1])+"|"+Arrays.toString(ROOM.busters[2])+"|"+Arrays.toString(ROOM.busters[3]);
                   bustArr=bustArr.replace(" ","");
                    
                   resObj.putUtfString("bs", bustArr);
                   trace("STRAT GAME");
                   send("SG", resObj, ROOM.ROOM.getUserList());
                   ROOM.setTimer();
                   ROOM.setTimerTurn(TURN);
                   
    }
    
     
    
    
    
    void noBusters(int[] arr,roomInfo ROOM,User user,int u){
         
         Boolean ans=true;
         int Size=0;
         int[] maxBust={0,0,0,0,0,0,0,0,0,0};
         int SS=0;
         int VIP=user.getVariable("VIP").getIntValue();
         int MaxB=(VIP==0)?3:4;
         for(int a=0;a<arr.length;a++){
             if(arr[a]!=0){
                 
                 Size+=arr[a];
                 SS=a+1;
                 maxBust[a]++;
              }
         }
         
         if(Size<MaxB){
             for(int S=Size;S<MaxB;S++){
                 maxBust=addDefaultBuster(ROOM,user,u,SS,maxBust);
             }
         }
         
     }
    
        
    
    
    
    int[] addDefaultBuster(roomInfo ROOM,User user,int u,int S,int[] MB){
       
        int SS=0;
        int MAX=0;
        int MaxBust=0;
        int MaxBust2=0;   
          
           for(int b=0;b<9;b++){
              int BB=b+1;
              int B=user.getVariable("B_"+BB).getIntValue()-MB[b];
                if(B>MAX){
                   if(MB[b]==0){
                         MAX=B;
                         MaxBust=b+1;
                         
                   // }else{
                     //    MaxBust2=b+1;
                    }
                }
             }
        
           if(MaxBust>0){
               ROOM.busters[u][MaxBust-1]++;
               MB[MaxBust-1]++;
               SS=MaxBust;
           }//else if(MaxBust2>0){
              // ROOM.busters[u][MaxBust2-1]++;
               // MB[MaxBust2-1]++;
              // SS=MaxBust2;
          // }
        
        return MB;
    }
    
    
    
    
    
    
    public int checkTeleport(int ENDPLACE,int TURN,roomInfo ROOM){
        int EP=ENDPLACE;
          int EPL=(TURN-1)*7-1;
          if(EPL==-1)EPL=27;
          int EPL2=(TURN-1)*7+3;
           int DIR=1;
         for(int a=0;a<teleports.length;a++){
            if(teleports[a]==ENDPLACE){
                DIR=ROOM.teleportDirection[a];
                        if((ENDPLACE==EPL&&DIR==1)||(ENDPLACE==EPL2&&DIR==-1)){
                            
                        }else{
                           if(a<teleports.length-1&&DIR>0){
                               EP=teleports[a+1];
                           }else if(a>0&&DIR<0){
                               EP=teleports[a-1];
                           }else if(DIR>0){
                               EP=teleports[0];
                           }else{
                               EP=teleports[teleports.length-1];
                           }
                           ROOM.teleportDirection[a]*=-1;
                           break;
                        }
            }
        } 
        return  EP*DIR;
    }
    
    
    
    
    
    
    
    
    public Boolean checkExtraThrow(int EP){
        
        Boolean isExtra=false;
        
        for(int a=0;a<extraT.length;a++){
            
            if(extraT[a]==EP){
                isExtra=true;
                break;
            }
        }
        
        return isExtra;
    }
    
    
    
    
    
    
    public int[] convertSFSArray(ISFSArray ARR){
        
        int[] newInt=new int[ARR.size()];
        
        for(int a=0;a<ARR.size();a++){
            newInt[a]=ARR.getInt(a);
        }
        return(newInt);
    }
    
    
    
    
    
    
    
   public ISFSArray convertToSFSARRAY(int[] ARR){
        
        ISFSArray newArr = new SFSArray();
        for (int i=0; i<ARR.length; i++)
            {
              newArr.addInt(ARR[i]);
            }
        return(newArr);
    }
    
   
   
   
   
     
  
   public int checkShield(int place, roomInfo ROOM, int color){
            
              
                if(ROOM.HAVESHIELD>0){
                    
                     for (int s_1=0;s_1<4;s_1++) {
                                              
                        for (int s_2=0;s_2<3;s_2++) {
                                                     
                                
                             int shieldOn=ROOM.shieldPlace[s_1][s_2];
                             int shieldC=s_1+1;
                             if(shieldOn==place&&shieldC!=color) {
                                return(1);
                                
                            }    
                      }
                                     
                 }
                
            }
                return(0);
   
   }
   
   
   
   
   
   
    public void logView(String str){
       // trace(str);
    }
	
	
    
    
    
    
	
        Room getLobbyRoom()
        {
            return Lobby;
        }
        
	
        
        
        
	 public void removeRoom(int rID){
                roomInfo ROOM=(roomInfo)gRooms.get(rID);
                if(ROOM!=null){
                    ROOM.remove();
                    gRooms.remove(rID);
                 }

            }
        
        
         
         
         
         
        
        public void getSetts(){
                
            
                      String query ="SELECT * FROM `booster_pack`";
                     // ResultSet res = stmt.executeQuery(query);
                      ISFSArray res=DBWRAPPER(query, null, 1);
                            for (int r=0; r<res.size();r++) {
                                ISFSObject RESSET3=res.getSFSObject(r);
                                 if(RESSET3.getInt("booster_type")==1)boosterPrice[0]=RESSET3.getInt("price");
                                 else if(RESSET3.getInt("booster_type")==2)boosterPrice[1]=RESSET3.getInt("price");
                                 else if(RESSET3.getInt("booster_type")==3)boosterPrice[2]=RESSET3.getInt("price");
                                 else if(RESSET3.getInt("booster_type")==4)boosterPrice[3]=RESSET3.getInt("price");
                                 else if(RESSET3.getInt("booster_type")==5)boosterPrice[4]=RESSET3.getInt("price");
                                 else if(RESSET3.getInt("booster_type")==6)boosterPrice[5]=RESSET3.getInt("price");
                                 else if(RESSET3.getInt("booster_type")==7)boosterPrice[6]=RESSET3.getInt("price");
                                 else if(RESSET3.getInt("booster_type")==8)boosterPrice[7]=RESSET3.getInt("price");
                                 else if(RESSET3.getInt("booster_type")==9)boosterPrice[8]=RESSET3.getInt("price");
                                 else if(RESSET3.getInt("booster_type")==10)boosterPrice[9]=RESSET3.getInt("price");
                             
                                 
                                 int bustPrice=RESSET3.getInt("price");
                                 int bustCount=RESSET3.getInt("count");
                                 int bustType=RESSET3.getInt("booster_type");
                                 String bName=RESSET3.getUtfString("booster_name");
                                 String Booster=bName+"|"+bustType+"|"+bustCount+"|"+bustPrice;
                                 
                                 bArray.addUtfString(Booster);
                                 
                             }
                             
                     query ="SELECT * FROM `game_config` WHERE `G_ID`=1";
                      ISFSArray res2=DBWRAPPER(query, null, 1);
                     
                      if (res2.size()>0) {
                           ISFSObject RESSET2=res2.getSFSObject(0);
                         sixChance=RESSET2.getInt("SIX_CHANCE");
                         waitTimeBoosters=RESSET2.getInt("WAIT_BOOSTERS_SCREEN_TIME");
                         winBet=RESSET2.getInt("WIN_BET");
                         slotPrice=RESSET2.getInt("SLOT_PRICE");
                         gameTime=RESSET2.getInt("GAME_TIME");
                         addPointsHit=RESSET2.getInt("POINTS_HIT");
                         addPointsIn=RESSET2.getInt("POINTS_IN");
                         addPointsOut=RESSET2.getInt("POINTS_OUT");
                         updateConfigTime=RESSET2.getInt("UPDATE_TIME");
                         TIME_TO_PAY=RESSET2.getInt("TIME_TO_PAY");
                         FREE_PAY_VALUE  =RESSET2.getInt("FREE_PAY_VALUE");
                         MIN_MONEY_FOR_PAY =RESSET2.getInt("MIN_MONEY_FOR_PAY");
                         MAX_GAMES_PLAYED_PER_DAY=RESSET2.getInt("MAX_GAMES_PLAYED_PER_DAY");
                      }
                             bustpriceSend=boosterPrice[0]+","+boosterPrice[1]+","+boosterPrice[2]+","+boosterPrice[3]+","+boosterPrice[4]+","+boosterPrice[5]+","+boosterPrice[6]+","+boosterPrice[7]+","+boosterPrice[8]+","+boosterPrice[9];
                             
                    
        }
        
        
        
        
        
        void checkRound()
	{
           
          
	}
        
        
        
        
        
        
    void ChangeTurn(int RID){
          
        if(gRooms.get(RID)!=null){
         
        roomInfo ROOM=gRooms.get(RID);
        Room gameRoom=ROOM.ROOM;
        ROOM.mustPlay=false;
        ROOM.CANPLAY=false;
        ROOM.PROCESSING=false;
        Boolean mustExit=false;
        
       // String DECK = gameRoom.getVariable("DCK").getStringValue();
       // String INARR = gameRoom.getVariable("InArr").getStringValue();
       // String LOGS = gameRoom.getVariable("LOG").getStringValue();
             int TURN=gameRoom.getVariable("Trn").getIntValue();
             int OTURN=TURN;
             int NUMTOGO=gameRoom.getVariable("nG").getIntValue();
            // int[]  pawnsOut =convertSFSArray(gameRoom.getVariable("nOut").getSFSArrayValue());
             int NTURN=TURN;
             
              
                      int N=0;
                        do{
                            
                              if(NTURN<ROOM.roomPlayers)NTURN+=1;
                              else                      NTURN =1;
                              N++;
                              
                        }while(0==ROOM.pIds[NTURN-1]&&N<6);
              
                     ISFSObject respObj2 = new SFSObject();
		     respObj2.putUtfString("D", Arrays.toString(ROOM.allPawns));
                     respObj2.putUtfString("I", Arrays.toString(ROOM.pawnsIn[0])+"|"+Arrays.toString(ROOM.pawnsIn[1])+"|"+Arrays.toString(ROOM.pawnsIn[2])+"|"+Arrays.toString(ROOM.pawnsIn[3]));
                     respObj2.putUtfString("H", ROOM.pawnsOUT[0]+"|"+ROOM.pawnsOUT[1]+"|"+ROOM.pawnsOUT[2]+"|"+ROOM.pawnsOUT[3]);
                     respObj2.putUtfString("GM", Arrays.toString(ROOM.tweenNum));
                     String shArr=Arrays.toString(ROOM.shieldPlace[0])+"|"+Arrays.toString(ROOM.shieldPlace[1])+"|"+Arrays.toString(ROOM.shieldPlace[2])+"|"+Arrays.toString(ROOM.shieldPlace[3]);
                            shArr=shArr.replace(" ","");
                     respObj2.putUtfString("SH", shArr);
                     String gsArr=Arrays.toString(ROOM.ghosts[0])+"|"+Arrays.toString(ROOM.ghosts[1])+"|"+Arrays.toString(ROOM.ghosts[2])+"|"+Arrays.toString(ROOM.ghosts[3]);
                            gsArr=gsArr.replace(" ","");
                     respObj2.putUtfString("GH", gsArr);
                     respObj2.putUtfString("TD", Arrays.toString(ROOM.teleportDirection));       
                    if(ROOM.missedCount[TURN-1]>1){
                        if(ROOM.pIds[TURN-1]!=-1){
                            User user=gameRoom.getUserByPlayerId(TURN);
                            mustExit=true;
                            //setUserGone(user,RID);
                            try {
                                getApi().joinRoom(user, Lobby, "",false,gameRoom,false,true);
                            } catch (SFSJoinRoomException ex) {
                                Logger.getLogger(leaveRoom.class.getName()).log(Level.SEVERE, null, ex);
                            }
                        }
                        //user.disconnect(null);
                        if(ROOM.currPlayers>1)ROOM.setTimerTurn(NTURN);
                    }else{
                         ROOM.missedCount[TURN-1]++;
                         if(ROOM.MovesCount[TURN-1]>10)ROOM.MovesCount[TURN-1]-=10;
                         else                          ROOM.MovesCount[TURN-1]=0;
                           respObj2.putInt("ot",TURN);
                           respObj2.putInt("tm",ROOM.gameTime);
                           respObj2.putInt("np",ROOM.MovesCount[TURN-1]);
                         int[]  pawnsFinished =convertSFSArray(gameRoom.getVariable("nFin").getSFSArrayValue());
                         
                        
                         
                        if(ROOM.pawnsOUT[NTURN-1]+pawnsFinished[NTURN-1]==ROOM.numPawns){
                                 NUMTOGO=3;
                             }
                        else{
                                 NUMTOGO=1;
                        }
                        
                        if(!mustExit){   
                          respObj2.putInt("t", NTURN);
                          RoomVariable Trn=new SFSRoomVariable("Trn",NTURN,true,true,true);    // Whose Turn
                          RoomVariable nG=new SFSRoomVariable("nG",NUMTOGO,true,true,true);
                          gameRoom.setVariables(Arrays.asList(Trn,nG));
                          send("CT", respObj2, gameRoom.getUserList()); 
                          
                          if(ROOM.currPlayers>1)ROOM.setTimerTurn(NTURN);
                        }
                        
                  }
                    ROOM.PROCESSING=false;
                    ROOM.ERRORNUM=0;
                    ROOM.missedPlayCount++;
                     IDBManager dbManager = getParentZone().getDBManager();
         
                    
         
                           String queryQW="INSERT INTO `game_log` (`missed_count`,`game_id`,`player_id`) values(?,?,?);";
                                    Object[] array = new Object[3];
                                    array[0] = ROOM.missedPlayCount;
                                    array[1] = ROOM.GID.toString();
                                    
                            
                                    
                                    
                                    if(OTURN>0&&OTURN<5)array[2] = ROOM.plIds[OTURN-1];
                                    else                array[2] = OTURN;
                                    
                                     ISFSArray resQW = DBWRAPPER(queryQW, array, 2);
        }
    }
    
    
    
    
    
    
    void finishGame(Room gameRoom,roomInfo ROOM,int winner, int ExtraWinner){
       // trace("FINISHED "+ROOM.gameFinished);
        if(ROOM!=null&&gameRoom!=null&&!ROOM.gameFinished){
         ISFSObject respObj2 = new SFSObject();
		    respObj2.putInt("w", winner);
                    respObj2.putInt("t", ROOM.gameTime);
          int ludoType=0;     
         int W=winner-1;
         int winCoins=ROOM.bet[W]*2;//winBet;//
         String uName=ROOM.players[W];
         if(ROOM.pIds[W]!=-1){
            try{
            User user=gameRoom.getUserByPlayerId(winner);
            uName=user.getVariable("uName").getStringValue();
            int wins=user.getVariable("wins").getIntValue();
            wins+=1;
            int UID=user.getVariable("pId").getIntValue();
            ludoType=user.getVariable("LT").getIntValue();
            String fbId=user.getVariable("uId").getStringValue();
            



            String queryQW="UPDATE `game_state` SET `coins`=`coins`+?,`score`=`score`+?,`wins`=`wins`+1 WHERE `player_id`=?";
                                       Object[] array = new Object[3];
                                       array[0] = winCoins;
                                       array[1] = ROOM.MovesCount[W];
                                       array[2] = UID;
            ISFSArray resQW = DBWRAPPER(queryQW, array, 3);

              /////SEND GAME FINISHED

                        try {
                          sendStatisticWinCoin(fbId,"WIN_GAME",winCoins,ROOM.GID,ludoType);
                        } catch (IOException ex) {
                          Logger.getLogger(LudoExtension.class.getName()).log(Level.SEVERE, null, ex);
                       }    

                int myMoney=user.getVariable("money").getIntValue();
                myMoney+=winCoins;
                UserVariable uMoney = new SFSUserVariable("money",myMoney,true);
                 UserVariable uWins = new SFSUserVariable("wins",wins);
                getApi().setUserVariables(user, Arrays.asList(uMoney,uWins));

                respObj2.putUtfString("fbId",fbId);  
            }catch(Exception e){}
           }    
         if(ExtraWinner>0){
           if(ROOM.pIds[ExtraWinner-1]!=-1){
                  try{
               User user2=gameRoom.getUserByPlayerId(ExtraWinner);
               uName=user2.getVariable("uName").getStringValue();
               int wins2=user2.getVariable("wins").getIntValue();
               wins2+=1;
               int UID=user2.getVariable("pId").getIntValue();
                int winCoins2=ROOM.bet[ExtraWinner-1]*2;//winBet;//
               String fbId2=user2.getVariable("uId").getStringValue();
                

               String queryQW="UPDATE `game_state` SET `coins`=`coins`+?,`score`=`score`+?,`wins`=`wins`+1 WHERE `player_id`=?";
                                          Object[] array = new Object[3];
                                          array[0] = winCoins2;
                                          array[1] = ROOM.MovesCount[ExtraWinner-1];
                                          array[2] = UID;

                   ISFSArray resQW = DBWRAPPER(queryQW, array, 3);




                   int myMoney2=user2.getVariable("money").getIntValue();
                   myMoney2+=winCoins2;
                   UserVariable uMoney2 = new SFSUserVariable("money",myMoney2,true);
                    UserVariable uWins2 = new SFSUserVariable("wins",wins2);
                   getApi().setUserVariables(user2, Arrays.asList(uMoney2,uWins2));

                   respObj2.putUtfString("extra",fbId2);
                    }catch(Exception e){}
             }
         }
          /////////////////SEND GAME FINISHED     
             respObj2.putUtfString("n", uName);
             respObj2.putInt("m", winCoins);
             
                                  
            for(int p=0;p<ROOM.fbIds.length;p++){
                if(!"".equals(ROOM.fbIds[p])){
                    int LTP=ludoType;
                    try {
                        if(gameRoom.getUserByPlayerId(p+1)!=null){
                            User US=gameRoom.getUserByPlayerId(p+1);
                            ludoType=US.getVariable("LT").getIntValue();
                        }
                      if(!ROOM.isPrivate)sendStatisticEG(ROOM.fbIds[p],"gameFinished",ROOM.GID,0,LTP);
                      else               sendStatisticEG(ROOM.fbIds[p],"gameFinished",ROOM.GID,1,LTP);
                     } catch (IOException ex) {
                     Logger.getLogger(LudoExtension.class.getName()).log(Level.SEVERE, null, ex);
                    }
                }
            }
          ROOM.remove();
          ROOM.gameStarted=false;
          ROOM.gameFinished=true;
          send("WG", respObj2, gameRoom.getUserList());    
              //  
                       
         
        }
    }
    
    
    
    
    
    
    
    
    public void ROLL(int fromRoom, String fbId,int plId, int DICEIS, int player_id){
            
                 
                  ISFSObject resObj = new SFSObject();
                  Room gameRoom=getParentZone().getRoomById(fromRoom);
                  
         if(gameRoom.isActive()){
                  
                  int TURN=gameRoom.getVariable("Trn").getIntValue();
                  int DICESIDES=gameRoom.getVariable("dS").getIntValue();
                  int NUMTOGO=gameRoom.getVariable("nG").getIntValue();
                  Boolean isBust=false;
                  Boolean canOut=false;
                  String SH=gameRoom.getVariable("sS").getStringValue();
                  roomInfo RI=gRooms.get(fromRoom);
                  if(RI.ERRORNUM>5)return;
             if(!RI.gameFinished){   ////////////////IF GAME NOT FINISHED //////////////////
                  
                   int NUMPLRS=RI.numPlayers;
                   int NUMPAWNS=RI.numPawns;
                   ISFSArray  pawnsFinished =gameRoom.getVariable("nFin").getSFSArrayValue();
                  
                   if(TURN==plId&&!RI.mustPlay&&!RI.PROCESSING){////////////////IF TURN EQUALS PLAYER///////////////////////
                       
                      NUMTOGO--;
                      int diceNum=(int) Math.floor(Math.random()*DICESIDES)+1;
                      
                     ///////////////////////////////////ADITIONAL CHANCE TO THROW SIX//////////////////////////////////
                      
                     if(RI.MovesCount[TURN-1]==0&&RI.pawnsOUT[TURN-1]==NUMPAWNS&&RI.Round>0&&RI.plIds[TURN-1]!=-1){
                         int SixChance= (int) Math.round(Math.random()*100);
                         if(SixChance<30+RI.Round*10){
                            diceNum=6; 
                         }
                      }else if(RI.plIds[TURN-1]>0){
                         int SixChance= (int) Math.round(Math.random()*100);
                        
                         if(SixChance<5+RI.NOTSIXES[TURN-1]*sixChance){
                              diceNum=6;
                         }
                      }
                      ////////////////////////////////////////////////////////////////////////////////////////
                      if(diceNum==6){//////////////REDUCE CHANCE FOR SECOND SIX IN A ROW
                          if(RI.SIXES[TURN-1]%10>0){
                              
                              int dNum=(int) Math.floor(Math.random()*100);
                              if(dNum>70)diceNum=6;
                              else{
                                  diceNum=(int) Math.floor(Math.random()*(DICESIDES-1))+1;
                              }
                              
                               
                          }
                      }
                      if(DICEIS!=0){/////////////////////GOT DICE FROM CLIENT
                          diceNum=DICEIS;
                          isBust=true;
                      
                            try {
                                User user=gameRoom.getUserByPlayerId(TURN);
                                int B4=user.getVariable("B_4").getIntValue();
                                     if(B4>0){
                                         B4-=1;
                                         UserVariable B_4 = new SFSUserVariable("B_4",B4,true);
                                         getApi().setUserVariables(user, Arrays.asList(B_4));
                                         int pId=user.getVariable("pId").getIntValue();
                                         sendStatisticB(fbId,pId,"dice",4,RI.GID,user.getVariable("LT").getIntValue());
                                     }
                            } catch (IOException ex) {
                               Logger.getLogger(LudoExtension.class.getName()).log(Level.SEVERE, null, ex);
                            }
                      }
                      
                      
                       Boolean CANPLAY=true;
                       RI.CANPLAY=false;
                      
                     if(diceNum==DICESIDES){  /// SIX THROWN
                          RI.SIXES[TURN-1]+=11;
                          RI.NOTSIXES[TURN-1]=0;
                          NUMTOGO=1;
                          CANPLAY=true;
                          
                          int STARTPOINT=7*(TURN-1)+1;
                          
                          if(RI.pawnsOUT[TURN-1]>0){
                              if(checkShield(STARTPOINT,RI,TURN)>0){
                                  if(RI.pawnsOUT[TURN-1]+pawnsFinished.getInt(TURN-1)==NUMPAWNS){
                                     CANPLAY=false;
                                     NUMTOGO=0;
                                  }
                              }else{
                                  canOut=true;
                                  CANPLAY=true;
                                  RI.CANPLAY=true;
                              }
                          }
                      }else{ ////////////////////NOT SIX THROWN
                          
                         RI.SIXES[TURN-1]=(int)Math.floor(RI.SIXES[TURN-1]/10)*10;
                        if(RI.pawnsOUT[TURN-1]+pawnsFinished.getInt(TURN-1)==NUMPAWNS){
                             
                             CANPLAY=false;
                             
                        }
                      }
                      
                      
                          

                     /////////////////////////CHECK CAN PLAY
                      Boolean canGo=false;
                      if(CANPLAY&&!canOut){
                           RI.CANPLAY=true;
                       
                       
                       if(RI.HAVESHIELD>0){   //////////HAVE SHIELD ON DECK
                           int[] DECKINT = RI.allPawns;
                       
                         for(int a=0;a<squadNum;a++){
                           
                             if(DECKINT[a]>0&&!canGo){
                                  
                                  int FOUNDCOLOR=(int) Math.floor(DECKINT[a]/10);
                                  int pawnNum=DECKINT[a]-FOUNDCOLOR*10;//DECKINT[a]%10
                                   
                                      if(FOUNDCOLOR==TURN){
                                           CANPLAY=true;
                                           RI.CANPLAY=true;
                                            
                                          int placeToGo=a+diceNum;
                                          int STARTPOINT=7*(TURN-1);
                                        
                                     
                                         
                                          if(placeToGo>27)placeToGo-=28;
                                       
                                    if(a<STARTPOINT+1&&placeToGo>STARTPOINT){ // IF NOT IN
                                    }else{
                                          for (int s_1=0;s_1<4;s_1++) {
                                              
                                              for (int s_2=0;s_2<3;s_2++) {
                                                  
                                                 int shieldOn=RI.shieldPlace[s_1][s_2];
                                                 int shieldC=s_1+1;
                                                 int shieldT=RI.shields[s_1][s_2];
                                                 
                                                     
                                                        if(shieldC==TURN){
                                                       
                                                            if(shieldT+shieldInt<=RI.Round&&shieldT!=-1) {

                                                                   
                                                                    ISFSObject endShield = new SFSObject();
                                                                    endShield.putInt("p", shieldOn);
                                                                    endShield.putInt("c", TURN);
                                                                    send("eS", endShield, gameRoom.getUserList());
                                                                    RI.shields[s_1][s_2]=-1;
                                                                    RI.shieldPlace[s_1][s_2]=-1;
                                                                    if(RI.HAVESHIELD>0)RI.HAVESHIELD--;
                                                             }//////////SEND END SHIELD
                                                        }
                                                       
                                                     
                                                   
                                                    if(shieldOn==placeToGo&&shieldC!=TURN&&RI.shields[s_1][s_2]>-1){
                                                        CANPLAY=false;
                                                        RI.CANPLAY=false;
                                                        canGo=false;
                                                     }
                                                 
                                                  canGo=CANPLAY;
                                               }
                                            }
                                          
                                             if(CANPLAY) break;
                                                    
                                        }   
                                      }
                                    
                                
                                   
                              }
                            }
                         
                           //////////////////////////////////////////////////////CHECK IF GOING OUT AND HAVE ENEMY SHIELD
                              if(!canGo){
                                int[][]INARRRINT=RI.pawnsIn;
                                int STARTPOINT=7*(TURN-1);
                                 for(int a=0;a<2;a++){
                                   int OUTPAWN=INARRRINT[TURN-1][a];
                                   
                                   if(OUTPAWN>0){
                                       //trace("CHECK SHIELD FROM IN "+a);
                                    CANPLAY=true;
                                   int endPlace=a+diceNum;
                                   if(endPlace>4){
                                       
                                       endPlace-=4;
                                       endPlace=STARTPOINT-(endPlace)+1;
                                       if(endPlace<0)endPlace+=27;
                                        for (int s_1=0;s_1<4;s_1++) {
                                              
                                              for (int s_2=0;s_2<3;s_2++) {
                                                  
                                                 int shieldOn=RI.shieldPlace[s_1][s_2];
                                                 int shieldC=s_1+1;
                                                 int shieldT=RI.shields[s_1][s_2];
                                                 
                                                    if(shieldOn==endPlace&&shieldC!=TURN&&RI.shields[s_1][s_2]>-1){
                                                        CANPLAY=false;
                                                        RI.CANPLAY=false;
                                                        canGo=false;
                                                     }
                                                     canGo=CANPLAY;
                                              }
                                        }
                                       
                                 }
                                   if(CANPLAY) break;
                               }
                            }
                         
                         }
                         
                        //////////////////////////////////////////////////////////////////////////////////////
                         
                          }  //////////////////END HAVE SHIELD ON DECK
                         
                           
                       
                           canGo=false;
                         if(RI.numSTICKY>0){//////////////HAVE STICKY ON DECK////////////////////
                           int[] DECKINT = RI.allPawns;
                                 
                          if(CANPLAY){
                              
                            for(int b=0;b<squadNum;b++){
                                Boolean IN=false;
                                if(!canOut){
                                    int FOUNDCOLOR=(int) Math.floor(DECKINT[b]/10);
                                    int pawnNum=DECKINT[b]-FOUNDCOLOR*10; 
                                     
                                if(DECKINT[b]>0&&!canGo){
                                   
                                  try{
                                    
                                       
                                       if(FOUNDCOLOR==TURN){
                                           CANPLAY=RI.CANPLAY=true;
                                           if(RI.sticky[b]>0){
                                              CANPLAY=false;
                                              RI.CANPLAY=false;
                                               
                                           }
                                           canGo=CANPLAY;
                                           IN=true;
                                           if(CANPLAY)  break;
                                            
                                       }
                                   
                                   if(CANPLAY&&IN)  break;
                                   }catch(Exception err){}
                                }
                                  
                            }
                                
                            }
                         }
                       
                                    for(int b=0;b<squadNum;b++){

                                            if(RI.sticky[b]>0&&NUMTOGO==0){

                                              try{
                                                 
                                                int FOUNDCOLOR=(int) Math.floor(DECKINT[b]/10);
                                                int pawnNum=DECKINT[b]-FOUNDCOLOR*10;
                                                   if(FOUNDCOLOR==TURN){

                                                    RI.sticky[b]--;

                                                  if(RI.sticky[b]<1){
                                                        ISFSObject endStick = new SFSObject();
                                                         endStick.putInt("p", b);
                                                         endStick.putInt("c", TURN);
                                                          send("eSt", endStick, gameRoom.getUserList());
                                                          RI.numSTICKY--;

                                                    }
                                                   }
                                               
                                               }catch(Exception err){}
                                            }
                                  }
                       
                        }  ///////////////////END HAVE STICKY///////////////
                      }///////END CAN PLAY AND CANNOT OUT//////////
                      
                      /////////////////////////////////////////
                      
                      if(NUMTOGO==0){
                          
                          if(diceNum!=6){
                          
                                    if(RI.tweens[TURN-1]>0){
                                        RI.tweens[TURN-1]--;
                                        if(RI.tweens[TURN-1]==0){
                                            RI.tweenNum[TURN-1]=1;
                                            ISFSObject endGemini = new SFSObject();
                                                       endGemini.putInt("c", TURN);
                                             send("eGm", endGemini, gameRoom.getUserList());  
                                        }
                                    }


                                    /////////////CHECK GHOST///////////

                                    for(int g=0;g<3;g++){
                                          if(RI.ghosts[TURN-1][g]>1000){
                                              RI.ghosts[TURN-1][g]-=1000;
                                              
                                              if(RI.ghosts[TURN-1][g]<1000){
                                                  ISFSObject endGhost = new SFSObject();
                                                       endGhost.putInt("p", RI.ghosts[TURN-1][g]);
                                                 send("eGst", endGhost, gameRoom.getUserList()); 
                                                 RI.ghosts[TURN-1][g]=-1;
                                              }
                                          }
                                      }


                                  //////////////////////////////////////                    
                          }
                          
                          
                          
                          NUMTOGO=1;
                          RI.mustPlay=true;
                          
                          if(!CANPLAY){
                             RI.mustPlay=false;
                             int N=0;
                             do{
                              if(TURN<NUMPLRS)TURN+=1;
                              else            {TURN =1;RI.Round++; }
                                N++;
                            }while("".equals(RI.players[TURN-1])&&N<6);
                            
                             if(RI.pawnsOUT[TURN-1]+pawnsFinished.getInt(TURN-1)==NUMPAWNS){
                                 NUMTOGO=3;
                             }
                          }
                      } 
                      
                      RoomVariable dN=new SFSRoomVariable("dN",diceNum,true,true,true);   // diceNum
                      RoomVariable Trn=new SFSRoomVariable("Trn",TURN,true,true,true);    // Whose Turn
                      RoomVariable nG=new SFSRoomVariable("nG",NUMTOGO,true,true,true);
                      RoomVariable sS=new SFSRoomVariable("sS",SH,true,true,true);
                      gameRoom.setVariables(Arrays.asList(Trn,dN,Trn,nG,sS));
                      
                     // saveToDiceTable(player_id,diceNum);
                      ISFSObject respObj = new SFSObject();
		      respObj.putInt("dN", diceNum);
                      respObj.putBool("cP", CANPLAY);
                      respObj.putBool("B",isBust);
                      respObj.putInt("t", TURN);
                      respObj.putInt("tm", RI.gameTime);
                      send("RD", respObj, gameRoom.getUserList());
                      RI.PROCESSING=false;
                      RI.ERRORNUM=0;
                      RI.setTimerTurn(TURN);
                  }else{
                      trace("NOT TURN OR NOT PLAY"+fbId+"  --------  "+TURN);
                      RI.PROCESSING=false;
                      RI.setTimerTurn(TURN);
                       RI.ERRORNUM++;
                       ISFSObject respObj2 = new SFSObject();
		       respObj2.putUtfString("D", Arrays.toString(RI.allPawns));
                       respObj2.putUtfString("I", Arrays.toString(RI.pawnsIn[0])+"|"+Arrays.toString(RI.pawnsIn[1])+"|"+Arrays.toString(RI.pawnsIn[2])+"|"+Arrays.toString(RI.pawnsIn[3]));
                       respObj2.putUtfString("TD", Arrays.toString(RI.teleportDirection));  
                       respObj2.putInt("DC", 2);
                        respObj2.putInt("T", TURN);
                         respObj2.putUtfString("GM", Arrays.toString(RI.tweenNum));
                        String shArr=Arrays.toString(RI.shieldPlace[0])+"|"+Arrays.toString(RI.shieldPlace[1])+"|"+Arrays.toString(RI.shieldPlace[2])+"|"+Arrays.toString(RI.shieldPlace[3]);
                            shArr=shArr.replace(" ","");
                     respObj2.putUtfString("SH", shArr);
                     String gsArr=Arrays.toString(RI.ghosts[0])+"|"+Arrays.toString(RI.ghosts[1])+"|"+Arrays.toString(RI.ghosts[2])+"|"+Arrays.toString(RI.ghosts[3]);
                            gsArr=gsArr.replace(" ","");
                     respObj2.putUtfString("GH", gsArr);
                         //int[]  pawnsOut2 =convertSFSArray(gameRoom.getVariable("nOut").getSFSArrayValue());
                       respObj2.putUtfString("H", RI.pawnsOUT[0]+"|"+RI.pawnsOUT[1]+"|"+RI.pawnsOUT[2]+"|"+RI.pawnsOUT[3]);
                       send("ERR", respObj2, gameRoom.getUserList());
                      
                  }
                  
           }
         }
        }
    
    
     
    
    
    
    
    
    
    public void MovePawn(int PLID,int fromRoom,int CP, int D, User user){
                  
                  Room gameRoom=getParentZone().getRoomById(fromRoom);
                  int MOVEFROM=CP;//(int)params.getInt("cP");
                  int MOVEFROM2=MOVEFROM;
                  int TURN=gameRoom.getVariable("Trn").getIntValue();
                  int DICESIDES=gameRoom.getVariable("dS").getIntValue();
                  int NUMTOGO=gameRoom.getVariable("nG").getIntValue();
                  int DICENUM=gameRoom.getVariable("dN").getIntValue();
                  int DICENUM2=DICENUM;
                  int ISTWEEN=1;
                  
                  //int[]  pawnsOut =convertSFSArray(gameRoom.getVariable("nOut").getSFSArrayValue());
                  int[]  pawnsFinished =convertSFSArray(gameRoom.getVariable("nFin").getSFSArrayValue());
                  String SH=gameRoom.getVariable("sS").getStringValue();
                 // String LOGS=gameRoom.getVariable("LOG").getStringValue();
                  Boolean ERRORMOVE=false;
                  Boolean HAVESHIELD=false;
                  int SHIELDNUM=0;
                   
                  roomInfo ROOM= gRooms.get(fromRoom);
                  ROOM. missedPlayCount=0;
                  String TD="";
                  //int[] DECKINT=ROOM.allPawns;
                  int[][]INARRRINT=ROOM.pawnsIn;
                  if(ROOM.ERRORNUM>5)return;
                 if(!ROOM.gameFinished){
                  
                  
                  int NUMPLRS=ROOM.numPlayers;
                  int NUMPAWNS=ROOM.numPawns;
                  int moveCount=0;
                  int PAWNTOMOVE=0;
                  int EG=0;
                  
                  if(TURN==PLID&&!ROOM.PROCESSING){//&&D==DICENUM
                      //ROOM.PROCESSING=true;
                      NUMTOGO--;
                      Boolean canContinue=false;
                      ROOM.missedCount[TURN-1]=0;
                      int PawnNum=getPawnNum(ROOM,MOVEFROM,TURN);
                      
                      
                      if(DICENUM==DICESIDES){
                         NUMTOGO=1;
                      } 
                      
                      int ENDPLACE=0;
                      Boolean PAWNSAFED=false;
                      Boolean GOIN=false;
                      Boolean PawnFinished=false;
                      int STARTPOINT=7*(TURN-1);
                      int NPP=0;
                       
                      if(MOVEFROM==-1){
                          
                         // pawnsOut[TURN-1]-=1;
                          if( ROOM.pawnsOUT[TURN-1]>0&&DICENUM==DICESIDES){
                            ROOM.pawnsOUT[TURN-1]-=1;
                            ENDPLACE=STARTPOINT+1;
                            moveCount+=addPointsOut;
                            PAWNTOMOVE=ROOM.pawnsOUT[TURN-1];
                            ROOM.mCount[TURN-1][ROOM.pawnsOUT[TURN-1]]+=1;
                            moveCount+=1;
                            ISTWEEN=1;
                          }else{
                               ERRORMOVE=true; 
                          }
                      }else {
                           
                           for(int p=ROOM.mCount[TURN-1].length-1;p>-1;p--){
                                   
                                   if(ROOM.mCount[TURN-1][p]==MOVEFROM){
                                      PAWNTOMOVE=p;
                                      break;
                                   }
                           }
                          
                          
                           if(MOVEFROM>=100){
                               int newPlace=MOVEFROM-101;
                               NPP=newPlace;
                                                          
                             
                               int OUTPLACE=INARRRINT[TURN-1][newPlace];//Integer.parseInt(myInArr[newPlace].trim());
                                
                               if(OUTPLACE>0){
                                   ISTWEEN=1;
                                   if(ROOM.tweens[TURN-1]>0)ISTWEEN=OUTPLACE;
                                    OUTPLACE-=ISTWEEN;
                                    INARRRINT[TURN-1][newPlace]=OUTPLACE;
                                    
                               }
                              
                               DICENUM+=NPP+1;
                               MOVEFROM=STARTPOINT;
                               GOIN=true;
                               moveCount=-(NPP+1);
                                if(ROOM.tweens[TURN-1]>0)ISTWEEN=ROOM.tweenNum[TURN-1];
                           }else{
                               
                                if(ROOM.allPawns[MOVEFROM]>10){// NOT EMPTY
                                  Boolean FOUND=false;
                                 
                                 int FOUNDPAWN=(int) Math.floor(ROOM.allPawns[MOVEFROM]/10);
                                 int pawnNum=ROOM.allPawns[MOVEFROM]-FOUNDPAWN*10;
                                 if(FOUNDPAWN==TURN){
                                     FOUND=true;
                                     
                                     if(ROOM.HAVESHIELD>0){
                                        
                                         for (int s=0;s<3;s++) {
                                            if(ROOM.shieldPlace[TURN-1][s]==MOVEFROM2){
                                                if(ROOM.moveShield[MOVEFROM2]==pawnNum){
                                                    HAVESHIELD=true;
                                                    SHIELDNUM=s;
                                                    break;
                                                }
                                            }
                                            
                                         }
                                     }
                                     if(pawnNum>1&&ROOM.tweens[TURN-1]>0)ISTWEEN=ROOM.tweenNum[TURN-1];
                                     ROOM.allPawns[MOVEFROM]-=ISTWEEN;
                                     if(ROOM.allPawns[MOVEFROM]<=TURN*10)ROOM.allPawns[MOVEFROM]=-1;
                                     
                                 }else{
                                     ERRORMOVE=true; 
                                 }
                                 
                                 
                                }else{
                                    ERRORMOVE=true;
                                }
                           }
                          
                          ENDPLACE=MOVEFROM+DICENUM;
                           if(!ERRORMOVE) {
                          if(ENDPLACE>squadNum-1&&CP<100){ //PASSING THE START
                                ENDPLACE-=squadNum;
                            }
                         
                           ///////////CHECK GO IN
                     
                            if((MOVEFROM<STARTPOINT+1||(TURN==1&&MOVEFROM+DICENUM>squadNum))){

                                  if(ENDPLACE>STARTPOINT){  //GO IN SAFE
                                      
                                      if(ENDPLACE>STARTPOINT+5){//RETURN OUT
                                            
                                          ENDPLACE=STARTPOINT-(ENDPLACE-STARTPOINT-6);
                                           
                                            moveCount=(ENDPLACE-STARTPOINT-NPP-1);
                                            if(!GOIN&&NPP==0)moveCount=0;
                                           
                                            if(ENDPLACE<0)ENDPLACE+=squadNum;
                                             GOIN=true;
                                             PAWNSAFED=false;
                                            
                                      }else{                    //SAFE PAWN
                                          
                                          
                                          
                                          if(HAVESHIELD){ ////REMOVE SHIELD IF HAVE
                                              
                                             for (int s_2=0;s_2<3;s_2++) {
                                                  
                                                if(ROOM.shieldPlace[TURN-1][s_2]==MOVEFROM){
                                                           ROOM.shields[TURN-1][s_2]=-1;
                                                           ROOM.shieldPlace[TURN-1][s_2]=-1;
                                                        if(ROOM.HAVESHIELD>0)ROOM.HAVESHIELD--;
                                                        
                                                         ISFSObject endShield = new SFSObject();
                                                                    endShield.putInt("p", MOVEFROM);
                                                                    endShield.putInt("c", TURN);
                                                                    send("eS", endShield, gameRoom.getUserList());
                                                        
                                                        
                                                        break;
                                                }
                                              }
                                              
                                          }
                                          
                                          
                                          
                                            PAWNSAFED=true;
                                          if(ENDPLACE==STARTPOINT+3){ ///////////FINISH PAWN
                                              
                                               pawnsFinished[TURN-1]+=ISTWEEN;
                                               
                                               if(pawnsFinished[TURN-1]==numPawns)EG=TURN;
                                               moveCount+=addPointsIn;
                                               moveCount+=DICENUM;
                                               GOIN=false;
                                               PawnFinished=true;
                                          }else{
                                             GOIN=true;
                                              
                                              int newPlace;
                                              if(ENDPLACE>STARTPOINT+3){
                                                                                                    
                                                  newPlace=2-(ENDPLACE-STARTPOINT-3);
                                                  int mCount=STARTPOINT-MOVEFROM;
                                                  if(mCount<0)mCount+=squadNum;
                                                  moveCount+=mCount;
                                                 
                                                  
                                                 if(newPlace==0){moveCount+=1;}
                                                 else           {moveCount+=2;}
                                                  INARRRINT[TURN-1][newPlace]+=ISTWEEN;
                                              }else{
                                                  moveCount+=DICENUM;
                                                  newPlace= ENDPLACE-STARTPOINT-1;
                                                 
                                                  INARRRINT[TURN-1][newPlace]+=ISTWEEN;
                                              }
                                              
                                          }
                                          
                                      }
                                     
                                  }else{
                                      moveCount+=DICENUM;
                                  }

                            }else{
                                moveCount+=DICENUM;
                            }
                      
                      
                      
                        ///////////END CHECK GO IN
                           }//NO ERRORMOVE
                      } 
                      
                      if(!ERRORMOVE){
                      int TELEPORT=0;
                      int hitPlace=ENDPLACE;
                      int reduceCount=0;
                       if(!PAWNSAFED){  //PAWN STILL NOT SAFED OR FINISHED
                            int ENDPLACE2=ENDPLACE;
                            
                            ENDPLACE=checkTeleport(ENDPLACE,TURN,ROOM);
                                int DIR=ENDPLACE>0?1:-1;
                                ENDPLACE=Math.abs(ENDPLACE);
                               if(ENDPLACE2!=ENDPLACE){// CHECK HIT BEFORE TELEPORT
                                   if(checkShield(ENDPLACE, ROOM, TURN)>0){
                                      ENDPLACE=ENDPLACE2; 
                                   }else{
                                       TELEPORT=1;
                                        String dirArr=Arrays.toString(ROOM.teleportDirection);
                                        dirArr=dirArr.replace(" ","");
                                        TD=dirArr;
                                   int nd=ENDPLACE-ENDPLACE2;
                                   
                                   if(nd<0&&DIR>0)nd+=squadNum;
                                   else if(nd>0&&DIR<0)nd-=squadNum;
                                   moveCount+=nd;
                                   
                                  if(ROOM.allPawns[ENDPLACE2]!=-1) {
                                   
                                      int FOUNDCOLOR2=(int) Math.floor(ROOM.allPawns[ENDPLACE2]/10);
                                      int pawnNum2=ROOM.allPawns[ENDPLACE2]-FOUNDCOLOR2*10;
                                        
                                       if (FOUNDCOLOR2 != TURN ) { ///////HIT
                                             for (int F=0;F<pawnNum2;F++) {
                                                
                                                moveCount+=addPointsHit;
                                                hitPlace=ENDPLACE2;
                                                reduceCount=ROOM.removeMoveCount(FOUNDCOLOR2, ENDPLACE2);
                                                ROOM.pawnsOUT[FOUNDCOLOR2-1]+=1;
                                             } 
                                              ROOM.allPawns[ENDPLACE2]=-1;
                                        }  
                                       
                                       
                                    }
                                   }
                                } 

                                ////////////////////////////////////////////  CHECK SHIELDS
                                  
                                       
                                      if(ROOM.HAVESHIELD>0){
                                        
                                          if(HAVESHIELD){
                                                   ROOM.shieldPlace[TURN-1][SHIELDNUM]=ENDPLACE;
                                                   int FOUNDCOLOR3=(int) Math.floor(ROOM.allPawns[ENDPLACE]/10);
                                                   int pawnNum3=ROOM.allPawns[ENDPLACE]-FOUNDCOLOR3*10;
                                                    ROOM.moveShield[MOVEFROM2]=0;
                                                   if(FOUNDCOLOR3==TURN){
                                                       ROOM.moveShield[ENDPLACE]=pawnNum3+1;
                                                   }else{
                                                       ROOM.moveShield[ENDPLACE]=1;
                                                   }
                                                  
                                                   
                                            }
                                      }
                                  /////////////////////////////////////////////
                            
                            
                          switch (ROOM.allPawns[ENDPLACE]) {
                              case -1:
                                  
                                  ROOM.allPawns[ENDPLACE]=TURN*10+ISTWEEN;
                                  ROOM.mCount[TURN-1][PAWNTOMOVE]=ENDPLACE;
                                  break;
                              case -2:
                                  ////HAVE BOMB
                                   
                                  if(!HAVESHIELD){
                                     reduceCount=ROOM.removeMoveCount(TURN, ENDPLACE);
                                     ROOM.mCount[TURN-1][PAWNTOMOVE]=-1;
                                     ROOM.pawnsOUT[TURN-1]+=ISTWEEN;
                                      
                                      if(ROOM.tweenNum[TURN-1]>1&&ISTWEEN>1){
                                                    ROOM.tweens[TURN-1] = 0;
                                                    ROOM.tweenNum[TURN-1]= 1;
                                                }
                                     ROOM.allPawns[ENDPLACE]=-1;
                                    if(ROOM.bombs[ENDPLACE]>0){
                                        if(ROOM.bombs[ENDPLACE]!=TURN)ROOM.addMoveCount(ROOM.bombs[ENDPLACE], 10,1);
                                        ROOM.bombs[ENDPLACE]=0;
                                    }
                                    
                                   
                                    
                                    int mMoves=ENDPLACE-STARTPOINT;
                                    if(mMoves<0)mMoves+=squadNum;
                                    moveCount-=mMoves;
                                         /////////////////remove GHOST if is/////////////////
                                             
                                       removeGhost(ROOM,TURN,ENDPLACE);
                                    
                                  }else{
                                     ROOM.allPawns[ENDPLACE]=TURN*10+ISTWEEN;
                                     ROOM.shields[TURN-1][SHIELDNUM]=-1;
                                     ROOM.shieldPlace[TURN-1][SHIELDNUM]=-1;
                                     if(ROOM.HAVESHIELD>0)ROOM.HAVESHIELD--;
                                  }
                                  break;
                                  
                               case -3:
                                   ROOM.allPawns[ENDPLACE]=TURN*10+ISTWEEN;
                                  if(!HAVESHIELD){  
                                    ROOM.sticky[ENDPLACE]=numSticky;
                                    ROOM.numSTICKY++;
                                  }else{
                                     ROOM.shields[TURN-1][SHIELDNUM]=-1;
                                     ROOM.shieldPlace[TURN-1][SHIELDNUM]=-1;
                                     if(ROOM.HAVESHIELD>0)ROOM.HAVESHIELD--;  
                                  }
                                  ROOM.mCount[TURN-1][PAWNTOMOVE]=ENDPLACE;
                                  break;
                              default:
                                  //  ENDPLACE NOT EMPTY
                                  
                                  int FOUNDCOLOR3=(int) Math.floor(ROOM.allPawns[ENDPLACE]/10);
                                  int pawnNum3=ROOM.allPawns[ENDPLACE]-FOUNDCOLOR3*10;
                                        
                                           if (FOUNDCOLOR3 != TURN ) { ///////HIT
                                               if(ROOM.sticky[ENDPLACE]>0){
                                                ROOM.sticky[ENDPLACE]=0;
                                               }
                                             for (int F3=0;F3<pawnNum3;F3++) {
                                             
                                                ROOM.pawnsOUT[FOUNDCOLOR3-1]+=1;
                                                moveCount+=addPointsHit;
                                                hitPlace=ENDPLACE;
                                                reduceCount=ROOM.removeMoveCount(FOUNDCOLOR3, ENDPLACE);
                                                
                                             } 
                                             ROOM.allPawns[ENDPLACE]=TURN*10+ISTWEEN;
                                             
                                             removeGhost(ROOM,FOUNDCOLOR3,ENDPLACE);
                                             
                                           }else{
                                             ROOM.allPawns[ENDPLACE]+=ISTWEEN;
                                           }  
                                       
                                  ROOM.mCount[TURN-1][PAWNTOMOVE]=ENDPLACE;
                                  break;
                          }
                        
                       }else{
                           int NP=ENDPLACE-STARTPOINT+100;
                           if(NP==104)     NP=102;
                           else if(NP==105)NP=101;
                           if(PawnFinished)NP=150;
                           ROOM.mCount[TURN-1][PAWNTOMOVE]=NP;
                           
                       }
                        
                       
                       
                      moveCount=moveCount*ISTWEEN;
                       
                      ROOM.addMoveCount(TURN, moveCount,PAWNTOMOVE); 
                     
                     if(!GOIN&&checkExtraThrow(ENDPLACE))NUMTOGO=1;
                        
                     ////////////// CHECK MOVE GHOST///////////////
                     
                      for(int g=0;g<3;g++){
                        if(ROOM.ghosts[TURN-1][g]>-1){
                          int EGST=ROOM.ghosts[TURN-1][g]%1000;
                          int rest=ROOM.ghosts[TURN-1][g]%1000;
                            
                            if(rest==MOVEFROM2){
                               ROOM.ghosts[TURN-1][g]=ROOM.ghosts[TURN-1][g]-MOVEFROM2+ENDPLACE;
                               break;
                            }else if(rest-100==MOVEFROM2){
                                ROOM.ghosts[TURN-1][g]-=100;
                            }else if(rest==ENDPLACE){
                                ROOM.ghosts[TURN-1][g]+=100;
                            }
                          }
                        }
                      
                      
                     //////////////////////////////////////////////////
                        
                        
                      if(NUMTOGO==0){
                          
                          NUMTOGO=1;
                          int N=0;
                          ROOM.NOTSIXES[TURN-1]++;
                          do{
                              if(TURN<NUMPLRS) TURN+=1;
                              else            {TURN =1;ROOM.Round++;}
                              N++;
                          }while("".equals(ROOM.players[TURN-1])&&N<6);
                           
                             if(ROOM.pawnsOUT[TURN-1]+pawnsFinished[TURN-1]==NUMPAWNS){
                                 NUMTOGO=3;
                             }
                          
                      } 
                       
                    
                      
                      
                      RoomVariable dN=new SFSRoomVariable("dN",DICENUM2,true,true,true);   // diceNum
                      RoomVariable Trn=new SFSRoomVariable("Trn",TURN,true,true,true);     // Whose Turn
                      RoomVariable nG=new SFSRoomVariable("nG",NUMTOGO,true,true,true);
                      
                      ISFSArray finishPawns = convertToSFSARRAY(pawnsFinished);
                      RoomVariable nFin=new SFSRoomVariable("nFin",finishPawns);
                  
                      gameRoom.setVariables(Arrays.asList(Trn,dN,Trn,nG,nFin));
                       
                      
                      
                      ISFSObject respObj = new SFSObject();
		      respObj.putInt("cP", MOVEFROM2);
                      respObj.putInt("pl", PLID);
                      respObj.putInt("d", DICENUM2);
                      respObj.putInt("t", TURN);
                      respObj.putInt("tw", ISTWEEN);
                      respObj.putInt("r", reduceCount);
                      respObj.putInt("ep", hitPlace);
                      respObj.putInt("tM", ROOM.gameTime);
                      respObj.putUtfString("mc", ROOM.getMoveCountAll());
                      respObj.putUtfString("td", TD);
                      send("MP", respObj, gameRoom.getUserList());
                       
                       ROOM.mustPlay=false;
                       ROOM.CANPLAY=false;
                       ROOM.PROCESSING=false;
                       ROOM.ERRORNUM=0;
                       ROOM.GetDeck=0;
                       ROOM.setTimerTurn(TURN);
                        if(EG!=0){
                           finishGame(gameRoom,ROOM,EG,0);
                         }
                      }else{//ERROR MOVE
                            try{
                                    ROOM.PROCESSING=false;
                                    ROOM.ERRORNUM++;
                                   ISFSObject respObj2 = new SFSObject();
                                respObj2.putUtfString("D", Arrays.toString(ROOM.allPawns));
                                respObj2.putUtfString("I", Arrays.toString(ROOM.pawnsIn[0])+"|"+Arrays.toString(ROOM.pawnsIn[1])+"|"+Arrays.toString(ROOM.pawnsIn[2])+"|"+Arrays.toString(ROOM.pawnsIn[3]));
                                respObj2.putInt("DC", DICENUM);
                                 respObj2.putInt("T", TURN);
                                 String shArr=Arrays.toString(ROOM.shieldPlace[0])+"|"+Arrays.toString(ROOM.shieldPlace[1])+"|"+Arrays.toString(ROOM.shieldPlace[2])+"|"+Arrays.toString(ROOM.shieldPlace[3]);
                                     shArr=shArr.replace(" ","");
                              respObj2.putUtfString("SH", shArr);

                              String gsArr=Arrays.toString(ROOM.ghosts[0])+"|"+Arrays.toString(ROOM.ghosts[1])+"|"+Arrays.toString(ROOM.ghosts[2])+"|"+Arrays.toString(ROOM.ghosts[3]);
                                     gsArr=gsArr.replace(" ","");
                              respObj2.putUtfString("GH", gsArr);
                               respObj2.putUtfString("GM", Arrays.toString(ROOM.tweenNum));
                                respObj2.putUtfString("H", ROOM.pawnsOUT[0]+"|"+ROOM.pawnsOUT[1]+"|"+ROOM.pawnsOUT[2]+"|"+ROOM.pawnsOUT[3]);
                                respObj2.putUtfString("TD", Arrays.toString(ROOM.teleportDirection));   
                                send("ERR", respObj2, user);
                          }catch(Exception e){}
                          
                      }
                  }else{
                      try{
                         trace("NOT YOUR TURN"+user.getIpAddress());
                               ROOM.PROCESSING=false;
                               ROOM.ERRORNUM++;
                                 ISFSObject respObj2 = new SFSObject();
                                 respObj2.putUtfString("D", Arrays.toString(ROOM.allPawns));
                                 respObj2.putUtfString("I", Arrays.toString(ROOM.pawnsIn[0])+"|"+Arrays.toString(ROOM.pawnsIn[1])+"|"+Arrays.toString(ROOM.pawnsIn[2])+"|"+Arrays.toString(ROOM.pawnsIn[3]));
                                 respObj2.putInt("DC", DICENUM);
                                  respObj2.putInt("T", TURN);
                                  String shArr=Arrays.toString(ROOM.shieldPlace[0])+"|"+Arrays.toString(ROOM.shieldPlace[1])+"|"+Arrays.toString(ROOM.shieldPlace[2])+"|"+Arrays.toString(ROOM.shieldPlace[3]);
                                      shArr=shArr.replace(" ","");
                               respObj2.putUtfString("SH", shArr);
                               String gsArr=Arrays.toString(ROOM.ghosts[0])+"|"+Arrays.toString(ROOM.ghosts[1])+"|"+Arrays.toString(ROOM.ghosts[2])+"|"+Arrays.toString(ROOM.ghosts[3]);
                                      gsArr=gsArr.replace(" ","");
                               respObj2.putUtfString("GH", gsArr);
                                respObj2.putUtfString("GM", Arrays.toString(ROOM.tweenNum));
                                 respObj2.putUtfString("H", ROOM.pawnsOUT[0]+"|"+ROOM.pawnsOUT[1]+"|"+ROOM.pawnsOUT[2]+"|"+ROOM.pawnsOUT[3]);
                                 respObj2.putUtfString("TD", Arrays.toString(ROOM.teleportDirection));   
                                 send("ERR", respObj2, user);
                        }catch(Exception e){}
                  }
        
           }
    }
    
    
    
    
    
    
    
    void removeGhost(roomInfo ROOM, int TURN, int ENDPLACE){
        for(int g=0;g<3;g++){
            if(ROOM.ghosts[TURN-1][g]>-1){
                int EGST=ROOM.ghosts[TURN-1][g]%1000;
                 if(EGST==ENDPLACE){
                       ROOM.ghosts[TURN-1][g]=-1;
                  }
             }
         }
    }
    
    
    
    
    
    
    
    int getPawnNum(roomInfo ROOM,int place, int TURN){
            
            int pawnNum=0;
                for(int R=0;R<ROOM.numPawns;R++){
                     if(ROOM.pPlace[TURN-1][R]==place){
                             pawnNum=R;
                             break;
                      }
                 }
            return pawnNum;
        }
    
    
    
     
    
    
    
    void setPrize(User user){
        
        String queryQW;
         int player_id=user.getVariable("pId").getIntValue();
         int myMoney=user.getVariable("money").getIntValue();
         String fbId=user.getVariable("uId").getStringValue();
         int ludoType=user.getVariable("LT").getIntValue();
         int fromRoom = user.getVariable("playIn").getIntValue();
         int B;
        Double R=Math.random()*100;
        int typeWon=0;
        if(R<40){
             queryQW="UPDATE `game_state` SET `coins`=`coins`+50 WHERE `player_id`=?";// WIN 50
             typeWon=0;
              myMoney+=50;
              UserVariable uMoney = new SFSUserVariable("money",myMoney,true);
              getApi().setUserVariables(user, Arrays.asList(uMoney));
              
                    try {
                       roomInfo ROOM= gRooms.get(fromRoom);
                       sendStatisticWinCoin(fbId,"WF_MONEY",50,ROOM.GID,ludoType);
                     } catch (IOException ex) {
                      Logger.getLogger(LudoExtension.class.getName()).log(Level.SEVERE, null, ex);
                    }    
              
              
        }else if(R<55){
             queryQW="UPDATE `game_state` SET `coins`=`coins`+100 WHERE `player_id`=?";// WIN 100
             typeWon=1;
              myMoney+=100;
              UserVariable uMoney = new SFSUserVariable("money",myMoney,true);
              getApi().setUserVariables(user, Arrays.asList(uMoney));
              
                   try {
                       roomInfo ROOM= gRooms.get(fromRoom);
                       sendStatisticWinCoin(fbId,"WF_MONEY",100,ROOM.GID,ludoType);
                     } catch (IOException ex) {
                      Logger.getLogger(LudoExtension.class.getName()).log(Level.SEVERE, null, ex);
                    } 
        }else if(R<67){
             queryQW="UPDATE `game_state` SET `coins`=`coins`+300 WHERE `player_id`=?";// WIN 300
             typeWon=2;
             myMoney+=300;
              UserVariable uMoney = new SFSUserVariable("money",myMoney,true);
              getApi().setUserVariables(user, Arrays.asList(uMoney));
              
                    try {
                       roomInfo ROOM= gRooms.get(fromRoom);
                       sendStatisticWinCoin(fbId,"WF_MONEY",300,ROOM.GID,ludoType);
                     } catch (IOException ex) {
                      Logger.getLogger(LudoExtension.class.getName()).log(Level.SEVERE, null, ex);
                    } 
              
        }else if(R<69){
             queryQW="UPDATE `game_state` SET `coins`=`coins`+10000 WHERE `player_id`=?";// WIN 10000
             typeWon=3;
             myMoney+=10000;
              UserVariable uMoney = new SFSUserVariable("money",myMoney,true);
              getApi().setUserVariables(user, Arrays.asList(uMoney));
              
                    try {
                       roomInfo ROOM= gRooms.get(fromRoom);
                       sendStatisticWinCoin(fbId,"WF_MONEY",10000,ROOM.GID,ludoType);
                     } catch (IOException ex) {
                      Logger.getLogger(LudoExtension.class.getName()).log(Level.SEVERE, null, ex);
                    } 
              
        }else if(R<72){
             queryQW="UPDATE `booster` SET `booster_count`=`booster_count` + 5 WHERE `player_id`=? AND `booster_type`=3";// WIN 5 Booster shoot
             typeWon=4;
              B=user.getVariable("B_3").getIntValue();
              UserVariable B_3 = new SFSUserVariable("B_3",B+5,true);
              getApi().setUserVariables(user, Arrays.asList(B_3));
              
                     try {
                       roomInfo ROOM= gRooms.get(fromRoom);
                       sendStatisticWinCoin(fbId,"WF_BUSTER",6000,ROOM.GID,ludoType);
                     } catch (IOException ex) {
                      Logger.getLogger(LudoExtension.class.getName()).log(Level.SEVERE, null, ex);
                    } 
        }else if(R<79){
             queryQW="UPDATE `booster` SET `booster_count`=`booster_count` + 1 WHERE `player_id`=? AND `booster_type`=1"; //Booster bomb
             typeWon=5;
             B=user.getVariable("B_1").getIntValue();
              UserVariable B_1 = new SFSUserVariable("B_1",B+1,true);
              getApi().setUserVariables(user, Arrays.asList(B_1));
              
                     try {
                       roomInfo ROOM= gRooms.get(fromRoom);
                       sendStatisticWinCoin(fbId,"WF_BUSTER",500,ROOM.GID,ludoType);
                     } catch (IOException ex) {
                      Logger.getLogger(LudoExtension.class.getName()).log(Level.SEVERE, null, ex);
                     } 
        }else if(R<86){
             queryQW="UPDATE `booster` SET `booster_count`=`booster_count` + 1 WHERE `player_id`=? AND `booster_type`=2"; //Booster shield
             typeWon=6;
             B=user.getVariable("B_2").getIntValue();
              UserVariable B_2 = new SFSUserVariable("B_2",B+1,true);
              getApi().setUserVariables(user, Arrays.asList(B_2));
              
                    try {
                       roomInfo ROOM= gRooms.get(fromRoom);
                       sendStatisticWinCoin(fbId,"WF_BUSTER",500,ROOM.GID,ludoType);
                     } catch (IOException ex) {
                      Logger.getLogger(LudoExtension.class.getName()).log(Level.SEVERE, null, ex);
                    } 
              
        }else if(R<93){
             queryQW="UPDATE `booster` SET `booster_count`=`booster_count` + 1 WHERE `player_id`=? AND `booster_type`=3"; //Booster shoot
             typeWon=7;
             B=user.getVariable("B_3").getIntValue();
              UserVariable B_3 = new SFSUserVariable("B_3",B+1,true);
              getApi().setUserVariables(user, Arrays.asList(B_3));
              
                  try {
                       roomInfo ROOM= gRooms.get(fromRoom);
                       sendStatisticWinCoin(fbId,"WF_BUSTER",1200,ROOM.GID,ludoType);
                     } catch (IOException ex) {
                      Logger.getLogger(LudoExtension.class.getName()).log(Level.SEVERE, null, ex);
                    } 
        }else if(R<99){
             queryQW="UPDATE `booster` SET `booster_count`=`booster_count` + 1 WHERE `player_id`=? AND `booster_type`=4"; //Booster dice
             typeWon=8;
             B=user.getVariable("B_4").getIntValue();
              UserVariable B_4 = new SFSUserVariable("B_4",B+1,true);
              getApi().setUserVariables(user, Arrays.asList(B_4));
                
                 try {
                       roomInfo ROOM= gRooms.get(fromRoom);
                       sendStatisticWinCoin(fbId,"WF_BUSTER",1000,ROOM.GID,ludoType);
                     } catch (IOException ex) {
                      Logger.getLogger(LudoExtension.class.getName()).log(Level.SEVERE, null, ex);
                    } 
              
        }else{
             queryQW="UPDATE `booster` SET `booster_count`=`booster_count` + 1 WHERE `player_id`=? AND `booster_type`=5"; //Booster invisible
             typeWon=9;
             B=user.getVariable("B_5").getIntValue();
              UserVariable B_5 = new SFSUserVariable("B_5",B+1,true);
              getApi().setUserVariables(user, Arrays.asList(B_5));
              
              
                  try {
                       roomInfo ROOM= gRooms.get(fromRoom);
                       sendStatisticWinCoin(fbId,"WF_BUSTER",500,ROOM.GID,ludoType);
                     } catch (IOException ex) {
                      Logger.getLogger(LudoExtension.class.getName()).log(Level.SEVERE, null, ex);
                    } 
        }
   
                                    Object[] array = new Object[1];
                                    array[0] = player_id;
                                    ISFSArray resQW = DBWRAPPER(queryQW, array, 3);
                                       
                                    
         ISFSObject respObj2 = new SFSObject();
		    respObj2.putInt("t", typeWon);
          send("WF", respObj2,user);
    }
    
    
    
    
    
    
    
    
    void sendStatistic(String fbId, String type, UUID GID, int ludoType)throws IOException {
     
          
        Date date = new Date();
        StringBuilder sb = new StringBuilder("[{");
        if(ludoType!=0)sb.append("\"game\":\"Ludo\",");
        else           sb.append("\"game\":\"PaperLudo\",");
        sb.append("\"deviceId\":\"F: ").append(fbId).append("\",");
        if(ludoType==3||ludoType==4){
             sb.append("\"nameSuffix\":\"").append("Iphone").append("\",");
        }else if(ludoType==31||ludoType==41){
            sb.append("\"nameSuffix\":\"").append("Android").append("\",");
        }
        sb.append("\"sessionId\":0,");
        sb.append("\"type\":\"").append(type).append("\",");
        sb.append("\"version\":\"1.0\",");
        sb.append("\"connectionType\":\"FB\",");
        sb.append("\"latency\":\"0\",");
        sb.append("\"data\":{\"gameId\":\"").append(GID).append("\"},");
        sb.append("\"date\":\"").append(Math.round(date.getTime()/1000)).append("\"");
        sb.append("}]");
         
     
        String urlParameters = "json="+sb.toString();
        sendStatistics(urlParameters);
            
        
     
    }
     
    
    
    void sendStatisticF(String fbId, String type, UUID GID, int points,int ludoType)throws IOException {
     
        
        Date date = new Date();
        StringBuilder sb = new StringBuilder("[{");
        if(ludoType!=1)sb.append("\"game\":\"Ludo\",");
        else           sb.append("\"game\":\"PaperLudo\",");
        sb.append("\"deviceId\":\"F: ").append(fbId).append("\",");
        if(ludoType==3||ludoType==4){
             sb.append("\"nameSuffix\":\"").append("Iphone").append("\",");
        }else if(ludoType==31||ludoType==41){
            sb.append("\"nameSuffix\":\"").append("Android").append("\",");
        }
        sb.append("\"sessionId\":0,");
        sb.append("\"type\":\"").append(type).append("\",");
        sb.append("\"version\":\"1.0\",");
        sb.append("\"connectionType\":\"FB\",");
        sb.append("\"latency\":\"0\",");
      
        sb.append("\"data\":{\"pointsToFirst\":\"").append(points).append("\",\"gameId\":\"").append(GID).append("\"},");
        sb.append("\"date\":\"").append(Math.round(date.getTime()/1000)).append("\"");
        sb.append("}]");
         
        String urlParameters = "json="+sb.toString();
        sendStatistics(urlParameters);
            
        
     
    }
    
    
    
    
    
    
    void sendStatisticEG(String fbId, String type, UUID GID, int BG,int ludoType)throws IOException {
      
        
        Date date = new Date();
        StringBuilder sb = new StringBuilder("[{");
        if(ludoType!=1)sb.append("\"game\":\"Ludo\",");
        else           sb.append("\"game\":\"PaperLudo\",");
        sb.append("\"deviceId\":\"F: ").append(fbId).append("\",");
        if(ludoType==3||ludoType==4){
             sb.append("\"nameSuffix\":\"").append("Iphone").append("\",");
        }else if(ludoType==31||ludoType==41){
            sb.append("\"nameSuffix\":\"").append("Android").append("\",");
        }
        sb.append("\"sessionId\":0,");
        sb.append("\"type\":\"").append(type).append("\",");
        sb.append("\"version\":\"1.0\",");
        sb.append("\"connectionType\":\"FB\",");
        sb.append("\"latency\":\"0\",");
      
        sb.append("\"data\":{\"buddyGame\":\"").append(BG).append("\",\"gameId\":\"").append(GID).append("\"},");
        sb.append("\"date\":\"").append(Math.round(date.getTime()/1000)).append("\"");
        sb.append("}]");
         
        String urlParameters = "json="+sb.toString();
        sendStatistics(urlParameters);
            
        
     
    }
    
    void sendStatisticSG(String fbId, String type, UUID GID, int BG,int players,int ludoType)throws IOException {
     
      
        Date date = new Date();
        StringBuilder sb = new StringBuilder("[{");
        if(ludoType!=1)sb.append("\"game\":\"Ludo\",");
        else           sb.append("\"game\":\"PaperLudo\",");
        sb.append("\"deviceId\":\"F: ").append(fbId).append("\",");
        if(ludoType==3||ludoType==4){
             sb.append("\"nameSuffix\":\"").append("Iphone").append("\",");
        }else if(ludoType==31||ludoType==41){
            sb.append("\"nameSuffix\":\"").append("Android").append("\",");
        }
        sb.append("\"sessionId\":0,");
        sb.append("\"type\":\"").append(type).append("\",");
        sb.append("\"version\":\"1.0\",");
        sb.append("\"connectionType\":\"FB\",");
        sb.append("\"latency\":\"0\",");
      
        sb.append("\"data\":{\"buddyGame\":\""+BG+"\",\"gameId\":\""+GID+"\",\"players\":\""+players+"\"},");
        sb.append("\"date\":\"").append(Math.round(date.getTime()/1000)).append("\"");
        sb.append("}]");
         
        String urlParameters = "json="+sb.toString();
        sendStatistics(urlParameters);
            
        
     
    }
    
    
    
     void sendStatisticB(String fbId,int player_id, String type, int bT, UUID GID,int ludoType)throws IOException {
     
         
         
        IDBManager dbManager = getParentZone().getDBManager();
         
        String queryQW="UPDATE `booster` SET `booster_count`=`booster_count` - 1 WHERE `player_id`=? AND `booster_type`=?"; //Booster invisible
        Object[] array = new Object[2];
                 array[0] = player_id;
                 array[1] = bT;
            ISFSArray resQW = DBWRAPPER(queryQW, array, 3);
         
        
      
        Date date = new Date();
        StringBuilder sb = new StringBuilder("[{");
        if(ludoType!=1)sb.append("\"game\":\"Ludo\",");
        else           sb.append("\"game\":\"PaperLudo\",");
        sb.append("\"deviceId\":\"F: ").append(fbId).append("\",");
        if(ludoType==3||ludoType==4){
             sb.append("\"nameSuffix\":\"").append("Iphone").append("\",");
        }else if(ludoType==31||ludoType==41){
            sb.append("\"nameSuffix\":\"").append("Android").append("\",");
        }
        sb.append("\"sessionId\":0,");
        sb.append("\"type\":\"boosterUsed\",");
        sb.append("\"version\":\"1.0\",");
        sb.append("\"connectionType\":\"FB\",");
        sb.append("\"latency\":\"0\",");
        sb.append("\"data\":{\"boosterName\":\"").append(type).append("\",\"gameId\":\"").append(GID).append("\"},");
        sb.append("\"date\":\"").append(Math.round(date.getTime()/1000)).append("\"");
        sb.append("}]");
        String urlParameters = "json="+sb.toString();
        sendStatistics(urlParameters);
            
          
            
    
     
    }
     
   
     
     
     
     void sendStatisticWinCoin(String fbId, String type, int win, UUID GID,int ludoType)throws IOException {
     
        
        Date date = new Date();
        StringBuilder sb = new StringBuilder("[{");
        if(ludoType!=1)sb.append("\"game\":\"Ludo\",");
        else if(ludoType==1)sb.append("\"game\":\"PaperLudo\",");
        sb.append("\"deviceId\":\"F: ").append(fbId).append("\",");
        if(ludoType==3||ludoType==4){
             sb.append("\"nameSuffix\":\"").append("Iphone").append("\",");
        }else if(ludoType==31||ludoType==41){
            sb.append("\"nameSuffix\":\"").append("Android").append("\",");
        }
        sb.append("\"sessionId\":0,");
        sb.append("\"type\":\"winCoins\",");
        sb.append("\"version\":\"1.0\",");
        sb.append("\"connectionType\":\"FB\",");
        sb.append("\"latency\":\"0\",");
        sb.append("\"data\":{\"winCoins\":\"").append(win).append("\",\"winType\":\"").append(type).append("\",\"gameId\":\"").append(GID).append("\"},");
        sb.append("\"date\":\"").append(Math.round(date.getTime()/1000)).append("\"");
        sb.append("}]");
       
        String urlParameters = "json="+sb.toString();
        sendStatistics(urlParameters);
         
    }
     
     
     void sendStatisticLogMobile(String DeviceId,int ludoType)throws IOException {
     
        
        Date date = new Date();
        StringBuilder sb = new StringBuilder("[{");
        String GT="Android";
        if(ludoType==3||ludoType==4)GT="Iphone";
        sb.append("\"game\":\"Ludo\",");
        sb.append("\"nameSuffix\":\"").append(GT).append("\",");
        sb.append("\"deviceId\":\"").append(DeviceId).append("\",");
        sb.append("\"sessionId\":0,");
        sb.append("\"type\":\"login\",");
        sb.append("\"version\":\"1.0\",");
        sb.append("\"connectionType\":\"-\",");
        sb.append("\"latency\":\"0\",");
        sb.append("\"data\":{},");
        sb.append("\"date\":\"").append(Math.round(date.getTime()/1000)).append("\"");
        sb.append("}]");
       
        String urlParameters = "json="+sb.toString();
        sendStatistics(urlParameters);
         
    }
     
      
     
     void sendStatisticLogMobilePayments(String fbId,int ludoType,int status, String transaction_id,int player_id, double price,int games_played,int wins, String country)throws IOException {
     
        
        Date date = new Date();
        StringBuilder sb = new StringBuilder("[{");
        String GT="Android";
        if(ludoType==3||ludoType==4)GT="Iphone";
        sb.append("\"game\":\"Ludo\",");
        sb.append("\"nameSuffix\":\"").append(GT).append("\",");
        sb.append("\"deviceId\":\"").append(fbId).append("\",");
        sb.append("\"sessionId\":0,");
        sb.append("\"type\":\"packageBuy\",");
        sb.append("\"version\":\"1.0\",");
        sb.append("\"connectionType\":\"-\",");
        sb.append("\"latency\":\"0\",");
        sb.append("\"data\":{\"status\":\"").append(status).append("\",\"transaction_id\":\"").append(transaction_id).append("\",\"player_id\":\"").append(player_id).append("\",\"price\":\"").append(price).append("\",\"games_played\":\"").append(games_played).append("\",\"wins\":\"").append(wins).append("\",\"country\":\"").append(country).append("\"},");
        sb.append("\"date\":\"").append(Math.round(date.getTime()/1000)).append("\"");
        sb.append("}]");
       
        String urlParameters = "json="+sb.toString();
        sendStatistics(urlParameters);
         
    }
     
     
     
     
     void sendStatisticLogIN(String fbId, String type, int b1, int b2, int b3,int b4, int b5, int b6, int b7, int b8, int ludoType)throws IOException {
     
        
        Date date = new Date();
        StringBuilder sb = new StringBuilder("[{");
        if(ludoType!=1)sb.append("\"game\":\"Ludo\",");
        else           sb.append("\"game\":\"PaperLudo\",");
        sb.append("\"deviceId\":\"F: ").append(fbId).append("\",");
        if(ludoType==3||ludoType==4){
             sb.append("\"nameSuffix\":\"").append("Iphone").append("\",");
        }else if(ludoType==31||ludoType==41){
            sb.append("\"nameSuffix\":\"").append("Android").append("\",");
        }
        sb.append("\"sessionId\":0,");
        sb.append("\"type\":\"").append(type).append("\",");
        sb.append("\"version\":\"1.0\",");
        sb.append("\"connectionType\":\"FB\",");
        sb.append("\"latency\":\"0\",");
        sb.append("\"data\":{\"Bomb\":\"").append(b1).append("\",\"Shield\":\"").append(b2).append("\",\"Shoot\":\"").append(b3).append("\",\"Dice\":\"").append(b4).append("\",\"Invisible\":\"").append(b5).append("\",\"One Minute\":\"").append(b6).append("\",\"Sticky\":\"").append(b7).append("\",\"Gemini\":\"").append(b8).append("\"},");
        sb.append("\"date\":\"").append(Math.round(date.getTime()/1000)).append("\"");
        sb.append("}]");
        
        String urlParameters = "json="+sb.toString();
        sendStatistics(urlParameters);
     
    }
     
     
     void addUserListBuddies(User user, ISFSArray Buddies) throws SFSBuddyListException{
      
             
              
            try {
                
                      
               SmartFoxServer sfs = SmartFoxServer.getInstance();
               ISFSBuddyApi buddyApi = sfs.getAPIManager().getBuddyApi();
               ISFSArray onlineBuddies=new SFSArray();
                        
                     
                    for(int i=0;i<Buddies.size();i++){
                         
                      Long FriendFbId=Buddies.getLong(i);
                      String uName=Long.toString(FriendFbId);
                           
                          
                             try{
                               buddyApi.addBuddy(user, uName, false, true, true);
                             }catch(Exception ex){
                                trace("ERROR WRITING BUDDY");
                             }
                             
                             
                     }
                     
                  
            } catch (Exception ex) {
                Logger.getLogger(LudoExtension.class.getName()).log(Level.SEVERE, null, ex);
            } 
     }
     
     
     
     
     void sendLog(Room room){
         
          String LOGS=room.getVariable("LOG").getStringValue();
          roomInfo ROOM=gRooms.get(room.getId());
             if(ROOM==null){
                 LOGS+="PLAYERS:"+ROOM.players[0]+"|"+ROOM.players[1]+"|"+ROOM.players[2]+"|"+ROOM.players[3];
             }
          IDBManager dbManager = getParentZone().getDBManager();
         
        String queryQW="INSERT INTO `logs` (log,date) values(?,NOW())"; //Booster invisible
        Object[] array = new Object[1];
                 array[0] = LOGS;
                 
         ISFSArray resQW = DBWRAPPER(queryQW, array, 2);
            
     }
     
     
     
     
     void sendStatisticUserCount()throws IOException {
      
        Date date = new Date();
        StringBuilder sb = new StringBuilder("[{");
        sb.append("\"game\":\"Ludo\",");
        sb.append("\"deviceId\":0,");
        sb.append("\"sessionId\":0,");
        sb.append("\"type\":\"usersOnline\",");
        sb.append("\"version\":\"1.0\",");
        sb.append("\"connectionType\":\"FB\",");
        sb.append("\"latency\":\"0\",");
        sb.append("\"data\":{\"usersOnline\":\""+getParentZone().getUserCount()+"\"},");
        sb.append("\"date\":\""+Math.round(date.getTime()/1000)+"\"");
        sb.append("}]");
        
        String urlParameters = "json="+sb.toString();
        sendStatistics(urlParameters);
            
           
    }
     
     
     void saveToDiceTable(int player_id,int diceNum){
            
        /*  IDBManager dbManager = getParentZone().getDBManager();
         
                    if(player_id>0){
         
                           String queryQW="INSERT INTO `dice_log` (`PLAYER_ID`,`DICE`) values(?,?);";
                                    Object[] array = new Object[2];
                                    array[0] = player_id;
                                    array[1] = diceNum;
                                    
                                      
                                     try{
                                       dbManager.executeUpdate(queryQW,array);
                                     }catch(SQLException err){
                                         trace(err);
                                     }
                    }
        */
     }
        
    
     
     void sendStatistics(String urlParameters) throws IOException{
         
         
         URL url = new URL(urlS);
         HttpURLConnection conn = (HttpURLConnection) url.openConnection();
         conn.setDoOutput(true);
         conn.setConnectTimeout(1000);
         conn.setReadTimeout(1000);
         conn.setRequestMethod("POST");
         
         
         
         try (DataOutputStream wr = new DataOutputStream(conn.getOutputStream())) {
                wr.writeBytes(urlParameters);
                wr.flush();
                wr.close();
                int responseCode = conn.getResponseCode();
                conn.disconnect();
                
         }catch(Exception err){
             trace("ERROR:"+err);
         }finally{
             conn.disconnect();
             conn=null;
         }
         
         
     }
     
     
     
    public ISFSArray DBWRAPPER(String querry,  Object[] ARR, int TYPE){ // 1 SELECT, 2 INSERT , 3 UPDATE
        
        ISFSArray res=null;
         
        IDBManager dbManager = getParentZone().getDBManager();
            try {
                if(TYPE==1)      res=dbManager.executeQuery(querry);
                else if(TYPE==2) dbManager.executeInsert(querry,ARR);
                else if(TYPE==3) dbManager.executeUpdate(querry,ARR);
            } catch (SQLException ex) {
                trace("DB EXC "+ex);
                Logger.getLogger(LudoExtension.class.getName()).log(Level.SEVERE, null, "DB EXC "+ex);
            }
        
                        
                
         return (res);
        
    }
    
        
}
