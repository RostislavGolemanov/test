package sfs2x.extensions.games.ludo;


import com.google.gson.JsonArray;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import com.smartfoxserver.v2.entities.User;
import com.smartfoxserver.v2.entities.data.ISFSObject;
import com.smartfoxserver.v2.entities.data.SFSObject;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.net.HttpURLConnection;
import java.net.URL;




 class IOS_Purchase extends PurchaseClass
{
	 
	
    
	public IOS_Purchase(User user, String receipt)
	{
		// Check params
               
		final String PRODUCTION_URL = "https://buy.itunes.apple.com/verifyReceipt";
                final String SANDBOX_URL = "https://sandbox.itunes.apple.com/verifyReceipt";
                   LudoExtension gameExt = (LudoExtension) user.getZone().getExtension();
                  gameExt.trace("TRY TO BUY");
                  ISFSObject resObj = new SFSObject();
                  
                 
                   NewPayment PAYMENT=null;
                   String PID="";
                   final String jsonData = "{\"receipt-data\" : \"" + receipt + "\"}";
                  
                  
                 try        
                    {
                        
                        HttpURLConnection connection = openConnection(PRODUCTION_URL);
                        connection = postRequestData(jsonData, connection);
                        
                        try{
                           
                            PAYMENT=createPayment(connection,user, gameExt);
                            PID=(String)PAYMENT.getProductId();
                        
                        }catch(ApplePaymentException e){
                          
                          int code = e.getCode();
                          
                          if (code == 21007) {
                                         
					HttpURLConnection sandboxConnection = openConnection(SANDBOX_URL);
					sandboxConnection = postRequestData(jsonData,sandboxConnection);
                                        PAYMENT = createPayment(sandboxConnection,user,gameExt);
                                        PID=(String)PAYMENT.getProductId();
                             }
                        }
                    }
                    catch (Exception e)
                    {
                       PAYMENT = handleException(e);
                    }
                	
                 if(PAYMENT.getVerifeid()){
                   trackPayment(PAYMENT, user,gameExt);
                   giveReward(PID, user,gameExt);
                 }
	}
        
        private NewPayment handleException(Exception e) {

		

		int code = -1;

		if (e instanceof ApplePaymentException) {

			code = ((ApplePaymentException) e).getCode();

		}



		NewPayment payment = new NewPayment();

		payment.setIsVerified(false);

		return payment;

	}
        
       
        
        private HttpURLConnection openConnection(String urlAddress) throws IOException {
 
		URL url = new URL(urlAddress);
 
		HttpURLConnection connection = (HttpURLConnection) url.openConnection();
 
		connection.setDoOutput(true);
 
		connection.setRequestMethod("POST");
 
		return connection;
 
	 }
        
        
        
        private HttpURLConnection postRequestData(String requestData, HttpURLConnection connection) throws IOException {

		OutputStreamWriter writer = new OutputStreamWriter(

		connection.getOutputStream());

		writer.write(requestData);

		writer.close();

		return connection;

	}
	
       private NewPayment createPayment(HttpURLConnection connection,User user,LudoExtension gameExt) throws IOException,ApplePaymentException {

		NewPayment payment = null;
                
		if (checkHttpConnectionStatus(connection)) {

			JsonObject jsonResponse = readJsonResponse(connection,gameExt);
                    
                    
                        if (checkJsonResponse(jsonResponse)) {
                            
                            payment = processJsonResponse(jsonResponse,user,gameExt);
                            
                        }
                    

		}
              
		return payment;

	}
       
       private boolean checkJsonResponse(JsonObject jsonResponse)

			throws ApplePaymentException {
          
               int status = jsonResponse.get("status").getAsInt();


                if (status == 0) {
                    // gameExt.trace("PAY SUCCESS");
                    return true;

                } else {

                    throw new ApplePaymentException(status);
                     
                }
           

	}
       
       private JsonObject readJsonResponse(HttpURLConnection connection,LudoExtension gameExt)

			throws IOException {

		String response = readResponse(connection,gameExt);
               
                JsonObject json = new JsonParser().parse(response).getAsJsonObject();
		//JSONObject json = parseResponse(response);

		return json;

	}
       
       private boolean checkHttpConnectionStatus(HttpURLConnection connection)
 
			throws IOException {
 
		int statusCode = connection.getResponseCode();
 
		if (statusCode == HttpURLConnection.HTTP_OK) {
 
			return true;
 
		} else {
                        return false;
			//throw new HttpException(statusCode);
 
		}
 
	}
       
       private String readResponse(HttpURLConnection connection,LudoExtension gameExt)

			throws IOException {

		BufferedReader reader = new BufferedReader(new InputStreamReader(

				connection.getInputStream()));

		StringBuilder sb = new StringBuilder();

		String line;

		while ((line = reader.readLine()) != null) {

			sb.append(line);

		}

		reader.close();
                  
                
		return sb.toString();

	}
       
       private JsonObject parseResponse(String response) {
  
                JsonParser parser = new JsonParser();
                JsonObject json = parser.parse(response).getAsJsonObject();
                return json;
 
	}
       
       private NewPayment processJsonResponse(JsonObject jsonResponse,User user,LudoExtension gameExt) {
            
          
                
             JsonObject receipt = jsonResponse.getAsJsonObject("receipt");
             JsonArray inAppReceipts = receipt.getAsJsonArray("in_app");
             JsonObject inAppReceipt = inAppReceipts.get(0).getAsJsonObject();
             NewPayment payment = processReceipt(inAppReceipt, user,gameExt);
                      
             payment.setItemId(receipt.get("app_item_id").getAsInt());
             payment.setType(receipt.get("receipt_type").getAsString());
             payment.setBundle(receipt.get("bundle_id").getAsString());
             payment.setRDate(receipt.get("request_date").getAsString());
             payment.setReceipt(receipt);
             
             return payment;
          

	}
       
      
       private NewPayment processReceipt(JsonObject receipt, User user,LudoExtension gameExt) {
        try {
            String packageId = receipt.get("product_id").getAsString();
            
            String transactionId = receipt.get("transaction_id").getAsString();
            boolean isSuccess = false;
            
           
            
            if (checkTransactionId(transactionId,gameExt)) {
                                
                isSuccess = true;
                
            }
            
            NewPayment payment = createPaymentEntity(receipt, isSuccess);
            
             
            return payment;
        } catch (Exception ex) {
             return null;
          
        }

	}
       
       
       private NewPayment createPaymentEntity(JsonObject receipt,boolean isVerified) {
        try {
            NewPayment payment = new NewPayment();
                       
            String packageId = receipt.get("product_id").getAsString();
            
            payment.setPackageId(packageId);
            
            String transactionId = receipt.get("transaction_id").getAsString();
            
            payment.setTransactionId(transactionId);
                        
            Double packagePrice = IOS_PaymentUtil.getPackagePrice(packageId);
            
            payment.setPackagePrice(packagePrice);
            
            payment.setMarket("IOS");
            
            payment.setIsVerified(isVerified);
            
            payment.setPDate(receipt.get("purchase_date").getAsString());
            
            payment.setQuantity(receipt.get("quantity").getAsInt());
            
            
            return payment;
        } catch (Exception ex) {
            return null;
           // Logger.getLogger(IOS_Purchase.class.getName()).log(Level.SEVERE, null, ex);
        }

	}



	private AppleTransactionInfo createTransactionInfo(boolean isVerified) {

		AppleTransactionInfo transactionInfo = new AppleTransactionInfo();

		int code = getCode(isVerified);

		transactionInfo.setCode(code);

		String description = getDescription(isVerified);

		transactionInfo.setDescription(description);

		return transactionInfo;

	}
       
       
       
       
       
       
       
       
       private void setPaymentData(NewPayment payment, User user){
           
           
       //    AppleTransactionInfo transactionInfo = payment.getTransactionInfo();
         
           
            
       }
       
       
       private int getCode(boolean isVerified) {

		return isVerified ? 0 : -1;

	}



	private String getDescription(boolean isVerified) {

		return isVerified ? "" : "Duplicate";

	}
        
}
