package sfs2x.extensions.games.ludo;


import com.smartfoxserver.v2.annotations.Instantiation;
import com.smartfoxserver.v2.annotations.Instantiation.InstantiationMode;
import com.smartfoxserver.v2.entities.Room;
import com.smartfoxserver.v2.entities.User;
import com.smartfoxserver.v2.entities.data.ISFSObject;
import com.smartfoxserver.v2.entities.data.SFSObject;
import com.smartfoxserver.v2.exceptions.SFSRuntimeException;
import com.smartfoxserver.v2.extensions.BaseClientRequestHandler;


@Instantiation(InstantiationMode.SINGLE_INSTANCE)
public class joinGame extends BaseClientRequestHandler
{
	 
	
	@Override
	public void handleClientRequest(User user, ISFSObject params)
	{
		// Check params
               
		
                  LudoExtension gameExt = (LudoExtension) getParentExtension();
                  ISFSObject resObj = new SFSObject();
                  int rId=params.getInt("r");
                  int currRoom=user.getVariable("playIn").getIntValue();
                  if(currRoom!=gameExt.Lobby_Id&&currRoom!=0){
                      Room room=gameExt.getParentZone().getRoomById(currRoom);
                      gameExt.getApi().leaveRoom(user, room);
                  }
                  gameExt.joinPRoom(user,rId,100);
                //gameExt.addUserToGame(user);
                			 
	}
	

}
