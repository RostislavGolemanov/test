package sfs2x.extensions.games.ludo;

import com.smartfoxserver.v2.entities.Room;
import com.smartfoxserver.v2.annotations.Instantiation;
import com.smartfoxserver.v2.annotations.Instantiation.InstantiationMode;
import com.smartfoxserver.v2.db.IDBManager;
import com.smartfoxserver.v2.entities.User;
import com.smartfoxserver.v2.entities.data.ISFSObject;
import com.smartfoxserver.v2.entities.data.SFSObject;
import com.smartfoxserver.v2.entities.variables.SFSUserVariable;
import com.smartfoxserver.v2.entities.variables.UserVariable;
 
import com.smartfoxserver.v2.extensions.BaseClientRequestHandler;
import java.io.IOException;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.Arrays;
import java.util.logging.Level;
import java.util.logging.Logger;

 

@Instantiation(InstantiationMode.SINGLE_INSTANCE)
public class buySlot extends BaseClientRequestHandler
{
	 
	
	@Override
	public void handleClientRequest(User user, ISFSObject params)
	{
		// Check params
                 
		
                   LudoExtension gameExt = (LudoExtension) getParentExtension();
		   IDBManager dbManager = gameExt.getParentZone().getDBManager();
                    
               
                 
		   int player_id=user.getVariable("pId").getIntValue();
                   int money=user.getVariable("money").getIntValue();
                   int real_money=user.getVariable("rM").getIntValue();
                
                try{
                      
                    
                     
                            
                               
                               if(money+real_money>=gameExt.slotPrice){
                                   
                                 Room toRoom=user.getLastJoinedRoom();
                                 roomInfo ROOM=gameExt.gRooms.get(toRoom.getId());
                                 for(int p=0;p<4;p++){
                 
                                   if(ROOM.pIds[p]==user.getId()){
                                    ROOM.players[user.getPlayerId()-1]=user.getVariable("uId").getValue()+"|"+user.getVariable("uName").getStringValue()+"|"+user.getVariable("uThumb").getValue()+"|"+user.getPlayerId()+"|1";
                                    break;
                                   }
                                 }
                                  
                                  int virtualPrice;
                                 int realPrice;
                                 if(money<gameExt.slotPrice){
                                     virtualPrice=money;
                                     realPrice=gameExt.slotPrice-virtualPrice;
                                 }else{
                                     virtualPrice=gameExt.slotPrice;
                                     realPrice=0;
                                 }
                                  int newMoney=money-virtualPrice;
                                  int newRMoney=real_money-realPrice;
                                  
                                    
                                 String query4="UPDATE `game_state`  SET `coins`=`coins`-?, `real_coins`=`real_coins`-? WHERE `player_id`=?";
                                     Object[] array4 = new Object[3];
                                    array4[0] = virtualPrice;
                                    array4[1] = realPrice;
                                    array4[2] = player_id;
                                    dbManager.executeUpdate(query4,array4);
                                  insertPaymentLog Insert=new insertPaymentLog(user,gameExt.slotPrice,"extra_slot");
                                 ISFSObject respObj = new SFSObject();
                                 
                                 
                                
                                 UserVariable uMoney = new SFSUserVariable("money",newMoney,true);
                                 UserVariable rMoney = new SFSUserVariable("rM",newRMoney,true);
                                 UserVariable uVIP = new SFSUserVariable("VIP",1,true);
                                 getApi().setUserVariables(user, Arrays.asList(uMoney,rMoney,uVIP));
                                  gameExt.send("vip", respObj,user);
                                  
                                                   try {
                                                      int ludoType=user.getVariable("LT").getIntValue();
                                                      String fbId=user.getVariable("uId").getStringValue();
                                                      gameExt.sendStatisticWinCoin(fbId,"BUY_SLOT",-5000,ROOM.GID,ludoType);
                                                    } catch (IOException ex) {
                                                     Logger.getLogger(LudoExtension.class.getName()).log(Level.SEVERE, null, ex);
                                                    }  
                                 
                               }
                                 
                                                    
                
                }catch(SQLException sqle){
                     //trace("ERROR "+sqle);
                	sqle.printStackTrace();
                        
                                
                }finally{
                      
                        
                 }
                
	}
	

}
