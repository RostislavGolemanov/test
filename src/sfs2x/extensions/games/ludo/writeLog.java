package sfs2x.extensions.games.ludo;

import com.smartfoxserver.v2.entities.Room;
import com.smartfoxserver.v2.annotations.Instantiation;
import com.smartfoxserver.v2.annotations.Instantiation.InstantiationMode;
import com.smartfoxserver.v2.entities.User;
import com.smartfoxserver.v2.entities.data.ISFSObject;
 
import com.smartfoxserver.v2.extensions.BaseClientRequestHandler;

 

@Instantiation(InstantiationMode.SINGLE_INSTANCE)
public class writeLog extends BaseClientRequestHandler
{
	 
	
	@Override
	public void handleClientRequest(User user, ISFSObject params)
	{
		// Check params
                 
		
                LudoExtension gameExt = (LudoExtension) getParentExtension();
		Room toRoom=user.getLastJoinedRoom();
                 gameExt.sendLog(toRoom);
                
                  
             
					 
	}
	
}
