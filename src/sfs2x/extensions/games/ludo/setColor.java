package sfs2x.extensions.games.ludo;


import com.smartfoxserver.v2.annotations.Instantiation;
import com.smartfoxserver.v2.annotations.Instantiation.InstantiationMode;
import com.smartfoxserver.v2.db.IDBManager;
import com.smartfoxserver.v2.entities.Room;
import com.smartfoxserver.v2.entities.User;
import com.smartfoxserver.v2.entities.data.ISFSArray;
import com.smartfoxserver.v2.entities.data.ISFSObject;
import com.smartfoxserver.v2.entities.data.SFSArray;
import com.smartfoxserver.v2.entities.data.SFSObject;
import com.smartfoxserver.v2.entities.variables.RoomVariable;
import com.smartfoxserver.v2.entities.variables.SFSRoomVariable;
import com.smartfoxserver.v2.entities.variables.SFSUserVariable;
import com.smartfoxserver.v2.entities.variables.UserVariable;
import com.smartfoxserver.v2.exceptions.SFSRuntimeException;
import com.smartfoxserver.v2.exceptions.SFSVariableException;
import com.smartfoxserver.v2.extensions.BaseClientRequestHandler;
import java.io.IOException;
import java.sql.Connection;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.Iterator;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;


@Instantiation(InstantiationMode.SINGLE_INSTANCE)
public class setColor extends BaseClientRequestHandler
{
	 
	
	@Override
	public void handleClientRequest(User user, ISFSObject params)
	{
		// Check params
               
		
                  LudoExtension gameExt = (LudoExtension) getParentExtension();
                  ISFSObject resObj = new SFSObject();
                 
                   
                  String fbId=user.getVariable("uId").getStringValue();
                  
                   
                   int COLOR=(int)params.getInt("C");
                   int plId=user.getVariable("pId").getIntValue();
                  
                  IDBManager dbManager = gameExt.getParentZone().getDBManager();
                   Statement stmt;
                  
                   Connection connection=null;
               
                   
                   
                  
                      
                      try {
                           UserVariable C = new SFSUserVariable("C",COLOR,true);
                           getApi().setUserVariables(user, Arrays.asList(C));
                          String query="UPDATE `game_settings` SET `color`=? WHERE `player_id` = ?";
                           Object[] array = new Object[2];
                                     array[0] = COLOR;
                                     array[1] = plId;
                                      
                                     dbManager.executeUpdate(query,array);
                                      
                      } catch (SQLException ex) {
                 
                      }
        
       
        }
}
