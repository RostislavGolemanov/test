package sfs2x.extensions.games.ludo;

import com.smartfoxserver.v2.entities.Room;
import com.smartfoxserver.v2.annotations.Instantiation;
import com.smartfoxserver.v2.annotations.Instantiation.InstantiationMode;
import com.smartfoxserver.v2.db.IDBManager;
import com.smartfoxserver.v2.entities.User;
import com.smartfoxserver.v2.entities.data.ISFSArray;
import com.smartfoxserver.v2.entities.data.ISFSObject;
import com.smartfoxserver.v2.entities.data.SFSObject;
import com.smartfoxserver.v2.entities.variables.SFSUserVariable;
import com.smartfoxserver.v2.entities.variables.UserVariable;
 
import com.smartfoxserver.v2.extensions.BaseClientRequestHandler;
import java.io.IOException;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.Arrays;
import java.util.logging.Level;
import java.util.logging.Logger;

 

@Instantiation(InstantiationMode.SINGLE_INSTANCE)
public class buyAnim extends BaseClientRequestHandler
{
	 
	
	@Override
	public void handleClientRequest(User user, ISFSObject params)
	{
		// Check params
                 
		
                   LudoExtension gameExt = (LudoExtension) getParentExtension();
		   
                   
                   int type = (int)params.getInt("t");
                    
                   int kind = (int)params.getInt("k");
                     
		   int player_id=user.getVariable("pId").getIntValue();
                   
                   int money=user.getVariable("money").getIntValue();
                   int real_money=user.getVariable("rM").getIntValue();
                   String query="SELECT `price` FROM `animation_pack` WHERE `animation_type` = "+type+" AND `animation_kind`= "+kind;
                  
                 
                   // ResultSet res = stmt.executeQuery(query);
                    ISFSArray res = gameExt.DBWRAPPER(query, null, 1);
                            if (res.size()>0) {
                                ISFSObject RESSET=res.getSFSObject(0);
                                 int animPrice=RESSET.getInt("price");
                                   
                               if(money+real_money>=animPrice){
                                   
                                 
                                     String query2="INSERT INTO `animation`(`player_id`, `type`, `type_kind`, `active`) VALUES (?, ?, ?, 0)";
                                     Object[] array = new Object[3];
                                     array[0] = player_id;
                                     array[1] = type;
                                     array[2] = kind;
                                    
                                  ISFSArray resQW = gameExt.DBWRAPPER(query2, array, 2);
                                 
                                 int virtualPrice;
                                 int realPrice;
                                 if(money<animPrice){
                                     virtualPrice=money;
                                     realPrice=animPrice-virtualPrice;
                                 }else{
                                     virtualPrice=animPrice;
                                     realPrice=0;
                                 }
                                  int newMoney=money-virtualPrice;
                                  int newRMoney=real_money-realPrice;
                                  String TYPE="anim_"+type;
                                    
                                 String query4="UPDATE `game_state`  SET `coins`=`coins`-?, `real_coins`=`real_coins`-? WHERE `player_id`=?";
                                     Object[] array4 = new Object[3];
                                    array4[0] = virtualPrice;
                                    array4[1] = realPrice;
                                    array4[2] = player_id;
                                     ISFSArray res4 = gameExt.DBWRAPPER(query4, array4, 3);
                                    
                                  insertPaymentLog Insert=new insertPaymentLog(user,animPrice,TYPE);
                                  ISFSObject respObj = new SFSObject();
                                 
                              
                                 UserVariable uMoney = new SFSUserVariable("money",newMoney,true);
                                 UserVariable rMoney = new SFSUserVariable("rM",newRMoney,true);
                                 getApi().setUserVariables(user, Arrays.asList(uMoney,rMoney));
                                 
                                 respObj.putInt("OK", 1);
                                 respObj.putInt("m", newMoney);
                                 respObj.putInt("t", type);
                                 respObj.putInt("k", kind);
		                 gameExt.send("bAnim", respObj,user);
                                 
                                       try {
                                              int ludoType=user.getVariable("LT").getIntValue();
                                              String fbId=user.getVariable("uId").getStringValue();
                                              gameExt.sendStatisticWinCoin(fbId,"BUY_Animation_"+type+"_"+kind,-1*animPrice,null,ludoType);
                                         } catch (IOException ex) {
                                              Logger.getLogger(LudoExtension.class.getName()).log(Level.SEVERE, null, ex);
                                         }
                                 
                               }
                                 
                            }
                
                
                
	}
	

}
