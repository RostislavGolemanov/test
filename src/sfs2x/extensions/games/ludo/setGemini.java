package sfs2x.extensions.games.ludo;


import com.smartfoxserver.v2.annotations.Instantiation;
import com.smartfoxserver.v2.annotations.Instantiation.InstantiationMode;
import com.smartfoxserver.v2.entities.Room;
import com.smartfoxserver.v2.entities.User;
import com.smartfoxserver.v2.entities.data.ISFSArray;
import com.smartfoxserver.v2.entities.data.ISFSObject;
import com.smartfoxserver.v2.entities.data.SFSArray;
import com.smartfoxserver.v2.entities.data.SFSObject;
import com.smartfoxserver.v2.entities.variables.RoomVariable;
import com.smartfoxserver.v2.entities.variables.SFSRoomVariable;
import com.smartfoxserver.v2.entities.variables.SFSUserVariable;
import com.smartfoxserver.v2.entities.variables.UserVariable;
import com.smartfoxserver.v2.exceptions.SFSRuntimeException;
import com.smartfoxserver.v2.exceptions.SFSVariableException;
import com.smartfoxserver.v2.extensions.BaseClientRequestHandler;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.Iterator;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;


@Instantiation(InstantiationMode.SINGLE_INSTANCE)
public class setGemini extends BaseClientRequestHandler
{
	 
	
	@Override
	public void handleClientRequest(User user, ISFSObject params)
	{
		// Check params
               
		
                  LudoExtension gameExt = (LudoExtension) getParentExtension();
                  ISFSObject resObj = new SFSObject();
                 
                  int fromRoom = user.getVariable("playIn").getIntValue();
                  String fbId=user.getVariable("uId").getStringValue();
                  Room gameRoom=gameExt.getParentZone().getRoomById(fromRoom);
                   
                  int TURN=gameRoom.getVariable("Trn").getIntValue();
                  int BOMBPLACE=(int)params.getInt("p");
                  String[] DECK = gameRoom.getVariable("DCK").getStringValue().split(","); 
                   roomInfo ROOM= gameExt.gRooms.get(fromRoom);
                  
                  
                  if(TURN==user.getPlayerId()&&checkGemini(BOMBPLACE)){
                       
                        int ISTWEEN=0;
                       
                       
                       if(ROOM.allPawns[BOMBPLACE]!=-1){// NOT EMPTY
                                 
                                 Boolean FOUND=false;
                                    int FOUNDCOLOR2=(int) Math.floor(ROOM.allPawns[BOMBPLACE]/10);
                                    int pawnNum2=ROOM.allPawns[BOMBPLACE]-FOUNDCOLOR2*10;
                                        if (FOUNDCOLOR2 == TURN) {
                                            FOUND=true;    // HAVE SAME
                                            ISTWEEN=pawnNum2;
                                        }  
                                     
                                    
                           }
                       
                       if(ISTWEEN>1){
                        
                                ROOM.tweens[TURN-1]=gameExt.geminiInt;
                                ROOM.tweenNum[TURN-1]=ISTWEEN;
                                ISFSObject respObj = new SFSObject();
                                 respObj.putInt("p", BOMBPLACE);
                                 respObj.putInt("c", TURN);
                                 respObj.putInt("t", ISTWEEN);
                                send("sGm", respObj, gameRoom.getUserList());
                               try {
                                   int plId=user.getVariable("pId").getIntValue();
                                   int B8=user.getVariable("B_8").getIntValue();
                                              if(B8>0){
                                                  B8-=1;
                                                  UserVariable B_8 = new SFSUserVariable("B_8",B8,true);
                                                  getApi().setUserVariables(user, Arrays.asList(B_8));
                                                   roomInfo room=gameExt.gRooms.get(fromRoom);

                                                  gameExt.sendStatisticB(fbId,plId,"gemini",8,room.GID,user.getVariable("LT").getIntValue());
                                              }
                               } catch (IOException ex) {
                                Logger.getLogger(LudoExtension.class.getName()).log(Level.SEVERE, null, ex);
                               }
                       }
                  }else{
                      trace("NOT YOUR TURN");
                  }
                  
                  
                			 
	}
        
        Boolean checkGemini(int BOMBPLACE){
            Boolean legitime=true;
             
            return legitime;
        }
	

}
