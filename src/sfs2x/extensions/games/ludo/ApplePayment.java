/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package sfs2x.extensions.games.ludo;

import com.google.gson.JsonObject;

/**
 *
 * @author IO_PC_24
 */
class ApplePayment {
   
    AppleTransactionInfo TI;
    private Boolean isVerifeid=true;
    private String package_id="";
    private String transaction_id="";
    private int package_price=0;
    private int app_item_id=0;
    private String receipt_type="";
    private String bundle="";
    private String request_date="";
    private String purchase_date="";
    private int quantity=0;
    private String receipt="";
    
    
    
    void setPackageId(String packageId) {
        package_id=packageId;
    }

    void setTransactionId(String transactionId) {
        transaction_id=transactionId;
    }

    void setPackagePrice(int packagePrice) {
        package_price=packagePrice;
    }

     
    void setIsVerified(boolean verified) {
        isVerifeid=verified;
    }
    

    void setItemId(int asInt) {
        app_item_id=asInt;
    }

    void setType(String asString) {
       receipt_type=asString;
    }

    void setBundle(String asString) {
        bundle=asString;
    }

    void setRDate(String asString) {
        request_date=asString;
    }

    void setPDate(String asString) {
       purchase_date=asString;
    }

    void setQuantity(int asInt) {
       quantity=asInt;
    }

    Object getTransactionId() {
        return transaction_id;
    }

    Object getApp_item_id() {
       return app_item_id;
    }

    Object getReceiptType() {
       return receipt_type;
    }

    Object getRequestDate() {
       return request_date;
    }

    Object getPerchaseDate() {
        return purchase_date;
    }

    Object getBundleId() {
        return bundle;
    }

    Object getQuantity() {
        return quantity;
    }

    Object getProductId() {
        return package_id;
    }

    Object getReceipt() {
        return receipt;
    }

    boolean getVerifeid() {
        return isVerifeid;
    }

    void setReceipt(JsonObject rec) {
        receipt=rec.toString();
    }
    
}
