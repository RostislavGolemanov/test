package sfs2x.extensions.games.ludo;


import com.smartfoxserver.v2.core.ISFSEvent;
import com.smartfoxserver.v2.core.SFSEventParam;
import com.smartfoxserver.v2.entities.Room;
import com.smartfoxserver.v2.entities.User;
import com.smartfoxserver.v2.entities.data.ISFSArray;
import com.smartfoxserver.v2.entities.data.ISFSObject;
import com.smartfoxserver.v2.entities.data.SFSArray;
import com.smartfoxserver.v2.entities.data.SFSObject;
import com.smartfoxserver.v2.entities.variables.SFSUserVariable;
import com.smartfoxserver.v2.entities.variables.UserVariable;
import com.smartfoxserver.v2.exceptions.SFSException;
import com.smartfoxserver.v2.extensions.BaseServerEventHandler;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Date;

public class OnUserJoin extends BaseServerEventHandler
{
    
         
	 
	private final Date DATE_WHEN_NO_CRON = new Date(0); //default date
	 
    
    
	@SuppressWarnings("unchecked")
        @Override 
        public void handleServerEvent(ISFSEvent event) throws SFSException
	{
		LudoExtension gameExt = (LudoExtension) getParentExtension();
		
		
		// Get event params
                
		User user = (User) event.getParameter(SFSEventParam.USER);
                Room room = (Room) event.getParameter(SFSEventParam.ROOM);
                
                
                if(room.getId()==gameExt.Lobby_Id){
                        
                        ISFSObject resObj = new SFSObject();
                        resObj.putUtfString("uN", user.getVariable("uName").getStringValue());
                        resObj.putUtfString("AN", user.getVariable("uId").getStringValue());
                        trace("real MOney: "+user.getVariable("rM").getIntValue());
                        int allMoney=user.getVariable("money").getIntValue()+user.getVariable("rM").getIntValue();
                       
                        resObj.putInt("bw",gameExt.winBet);
                        resObj.putInt("w", user.getVariable("wins").getIntValue());
                        resObj.putInt("b1", user.getVariable("B_1").getIntValue());
                        resObj.putInt("b2", user.getVariable("B_2").getIntValue());
                        resObj.putInt("b3", user.getVariable("B_3").getIntValue());
                        resObj.putInt("b4", user.getVariable("B_4").getIntValue());
                        resObj.putInt("b5", user.getVariable("B_5").getIntValue());
                        resObj.putInt("b6", user.getVariable("B_6").getIntValue());
                        resObj.putInt("b7", user.getVariable("B_7").getIntValue());
                        resObj.putInt("b8", user.getVariable("B_8").getIntValue());
                        resObj.putInt("b9", user.getVariable("B_9").getIntValue());
                        resObj.putInt("b10", user.getVariable("B_10").getIntValue());
                       
                        resObj.putInt("wb", user.getVariable("WB").getIntValue());
                       
                        resObj.putInt("sv", user.getVariable("SV").getIntValue());
                       
                        resObj.putInt("h", user.getVariable("H").getIntValue());
                       
                        resObj.putInt("cl", user.getVariable("C").getIntValue());
                        
                        resObj.putInt("sp", gameExt.slotPrice);//bustersPrices
                        resObj.putUtfString("bp", gameExt.bustpriceSend);//bustersPrices
                        long CD=(long)user.getSession().getProperty("CD");
                        
                           resObj.putLong("cd", CD); 
                            Date date3;
                             Calendar calendar = Calendar.getInstance();
                              
                            
                             long NOW=calendar.getTimeInMillis();
                                  resObj.putLong("SVT", NOW);       
                            
                              Integer Z=0;
                                user.getSession().setProperty("CD", Z.longValue());
                       
                         UserVariable playIn = new SFSUserVariable("playIn",gameExt.Lobby_Id,true);
                         UserVariable WB = new SFSUserVariable("WB",0,true);
                         UserVariable H = new SFSUserVariable("H",0,true);
                          UserVariable uVIP = new SFSUserVariable("VIP",0,true);
                         getApi().setUserVariables(user, Arrays.asList(playIn,WB,H,uVIP));
                         
                         if (user.getVariable("pId").getIntValue() > 0&&allMoney<51) {
                             
				ISFSArray coinsToAdd = isUserPlayedTooMuch(user);
				if (coinsToAdd.getInt(0) > 0) {
					 
					int money = user.getVariable("money").getIntValue();
					UserVariable uMoney = new SFSUserVariable("money", coinsToAdd.getInt(0) + money, true);
					user.setVariable(uMoney);
					 
					allMoney = user.getVariable("money").getIntValue() + user.getVariable("rM").getIntValue();
					resObj.putInt("gM", coinsToAdd.getInt(0));//gift money
				}
                                 resObj.putLong("tLM", coinsToAdd.getLong(1));// TIME LEFT MONEY
			}
                          resObj.putInt("m", allMoney);
                          send("logOK", resObj, user);
                         
                         
                }
                
		 
               		 
	}
        
        
        long setCronDate(User user, Date cronDate, int games_played) {
		LudoExtension gameExt = (LudoExtension) getParentExtension();
		int player_id = user.getVariable("pId").getIntValue();
		int myMoney = user.getVariable("money").getIntValue();
		int realMoney = user.getVariable("rM").getIntValue();
		 long cDate=0l;
		 
		if (myMoney + realMoney <= gameExt.MIN_MONEY_FOR_PAY) {
			 
				if (games_played < gameExt.MAX_GAMES_PLAYED_PER_DAY && cronDate.equals(DATE_WHEN_NO_CRON)) {

					String query4 = "UPDATE `game_state` SET `cron_date`=? WHERE `player_id`=?";
					Calendar cal = Calendar.getInstance(); // creates calendar

					cal.setTime(new Date());// sets
											// calendar
											// time/date

					cal.add(Calendar.MINUTE, gameExt.TIME_TO_PAY); // adds 90 mins
					Date now = cal.getTime();
                                        cDate=cal.getTimeInMillis();
					trace("Cron Date: " + now);
					Object[] array4 = new Object[2];
					array4[0] = now;
					array4[1] = player_id;
					gameExt.DBWRAPPER(query4, array4, 3);

					 
                                        
				} else{
                                     cDate=cronDate.getTime();
                                }

			 

		} 
                return cDate;
		
	}

	
        
        
        private int giveMoneyIfEmpty(User user) {
		int player_id = user.getVariable("pId").getIntValue();
		LudoExtension gameExt = (LudoExtension) getParentExtension();
		 
		int coinsToAdd = gameExt.FREE_PAY_VALUE;
		 
			String query4 = "UPDATE `game_state` SET `coins`=`coins`+?, `is_paid`=? WHERE `player_id`=?";
			Object[] array4 = new Object[3];
			array4[0] = coinsToAdd;
			array4[1] = 1;
			array4[2] = player_id;
			gameExt.DBWRAPPER(query4, array4, 3);

			return coinsToAdd;
		

	}

	
        
        
        ISFSArray isUserPlayedTooMuch(User user) {
		boolean isUserPlayedTooMuch = false;
		int player_id = user.getVariable("pId").getIntValue();
		LudoExtension gameExt = (LudoExtension) getParentExtension();
		 ISFSArray response=new SFSArray();
		 int coinsToAdd = 0;
		 Long timeLeft=0l;
		 

			 
			String query4 = "SELECT `cron_date`, `games_played`, `is_paid`  FROM `game_state` WHERE `player_id`= "
					+ player_id;
			ISFSArray setArr = gameExt.DBWRAPPER(query4, null, 1);
			ISFSObject set = null;
			if (setArr.size() > 0) {
				set = setArr.getSFSObject(0);
			}

			if (set != null) {
				Date cronDate = new Date(set.getLong("cron_date"));
				int games_played = set.getInt("games_played");
				int is_paid = set.getInt("is_paid");

				Date now = new Date();
				Calendar calendarNow = Calendar.getInstance();
				calendarNow.setTime(now);
                                long timeNow=calendarNow.getTimeInMillis();
				Calendar calendarCron = Calendar.getInstance();
				calendarCron.setTime(cronDate);
                                 
                               
                                
				// paying money
				if (!(cronDate.equals(new Date(0))) && is_paid == 0 && !now.before(cronDate)) {
					coinsToAdd = giveMoneyIfEmpty(user);
					is_paid = 1;
				}
 
				boolean differentDay = calendarNow.get(Calendar.YEAR) != calendarCron.get(Calendar.YEAR)
						|| calendarNow.get(Calendar.DAY_OF_YEAR) != calendarCron.get(Calendar.DAY_OF_YEAR)
						|| calendarNow.get(Calendar.MONTH) != calendarCron.get(Calendar.MONTH);

				// changing the day
				if (differentDay && (is_paid != 0 || cronDate .equals( DATE_WHEN_NO_CRON))) {
					trace("New Day: games_played, cron_date and is_paid has changed to default");
					String query = "UPDATE `game_state` SET  `games_played`=?, `cron_date`=?, `is_paid`=?  WHERE `player_id`=?";
					Object[] array4 = new Object[4];
					array4[0] = 0;
					array4[1] = DATE_WHEN_NO_CRON;
					array4[2] = 0;
					array4[3] = player_id;
					gameExt.DBWRAPPER(query, array4, 3);
					games_played = 0;
					cronDate = DATE_WHEN_NO_CRON;
				}

				if (games_played > gameExt.MAX_GAMES_PLAYED_PER_DAY)
					isUserPlayedTooMuch = true;

				// setting date
				if (!isUserPlayedTooMuch) {
                                       
					long timeCron=setCronDate(user, cronDate, games_played);
                                        if(timeCron>timeNow&&timeCron>0)timeLeft=timeCron-timeNow;
				}
			}
		 
		 response.addInt(coinsToAdd);
                 response.addLong(timeLeft);
		return response;
	}
}
