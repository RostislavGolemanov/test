package sfs2x.extensions.games.ludo;


import com.smartfoxserver.v2.db.IDBManager;
import com.smartfoxserver.v2.entities.User;
import com.smartfoxserver.v2.entities.data.ISFSObject;
import com.smartfoxserver.v2.entities.data.SFSObject;
import java.sql.Connection;
import java.sql.ResultSet;


import java.sql.SQLException;
import java.sql.Statement;
import java.util.Calendar;
import java.util.Date;
import java.util.logging.Level;
import java.util.logging.Logger;





 
public class PurchaseClass 
{
	 
	
     public void trackPayment(NewPayment payment,User user,LudoExtension gameExt){
           
              IDBManager dbManager = gameExt.getParentZone().getDBManager();
                try {
                    
                    String insertSql = "INSERT INTO `payments_log` (`transaction_id`,`status`,`MARKET`,`app_item_id`,`receipt_type`,`request_date`,`purchase_date`,`bundle_id`,`quantity`,`product_id`,`player_id`,`coins_balance`,`country`,`games_played`,`wins`,`receipt`,`date`,`price`) VALUES (?, ?, ?, ?, ? ,? ,? ,? ,? ,? ,? ,? ,? ,? ,? ,? ,?,?)";
                      Object[] array12 = new Object[18];
                                     array12[0] = payment.getTransactionId();
                                     array12[1] = 1;
                                     array12[2] = payment.getMarket();
                                     array12[3] = payment.getApp_item_id();
                                     array12[4] = payment.getReceiptType();
                                     array12[5] = payment.getRequestDate();
                                     array12[6] = payment.getPerchaseDate();
                                     array12[7] = payment.getBundleId();
                                     array12[8] = payment.getQuantity();
                                     array12[9] = payment.getProductId();
                                     array12[10] = user.getVariable("pId").getIntValue();
                                     array12[11] = user.getVariable("money").getIntValue()+user.getVariable("rM").getIntValue();
                                     array12[12] = "";
                                     array12[13] = user.getVariable("plays").getIntValue();
                                     array12[14] = user.getVariable("wins").getIntValue();
                                     array12[15] = payment.getReceipt();
                                     Date now=Calendar.getInstance().getTime();
                                     array12[16] = now;
                                     array12[17] = payment.getPrice();
                                     dbManager.executeInsert(insertSql,array12); 
               
                } catch (SQLException ex) {
                     gameExt.trace(ex);
                } finally{
                    
                }
           
       }
       public void giveReward(String packageId, User user,LudoExtension gameExt) {

           gameExt.trace("INSERT GOODS");
           ISFSObject Goods=getGoods(packageId);
           insertGoods Insert=new insertGoods(user,Goods);
           
       }

       public boolean checkTransactionId(String transactionId,LudoExtension gameExt) {

            /*IDBManager dbManager = gameExt.getParentZone().getDBManager();
           
            String checkSql = "SELECT COUNT(*) FROM `payments_log` WHERE `transaction_id`="+transactionId;
            Connection connection=null;
        
         try {
             connection=dbManager.getConnection();
             Statement pstm = connection.createStatement();
             ResultSet res = pstm.executeQuery(checkSql);
             if (res.next()) {
                return false;
             }
         } catch (SQLException ex) {
             Logger.getLogger(PurchaseClass.class.getName()).log(Level.SEVERE, null, ex);
         }finally{
             try{
                  if(connection!=null) connection.close();
              }catch(SQLException sqle) {
                 
              }
         }
           
           String checkSql = "SELECT COUNT(*) FROM `payments_log` WHERE `transaction_id`="+transactionId;
           ISFSArray res= gameExt.DBWRAPPER(checkSql, null, 1);
           
                  */     
                    
		return true; 	
      }
       
       
       public ISFSObject getGoods(String packageId){
           
           ISFSObject respObj = new SFSObject();
            switch (packageId) {
                case "coins_pack_10k":
                    respObj.putInt("coins", 10000);
                    break;
                case "coins_pack_20k":
                    respObj.putInt("coins", 20000);
                    break;
                case "coins_pack_40k":
                    respObj.putInt("coins", 44000);
                    break;
                case "coins_pack_80k":
                    respObj.putInt("coins", 92000);
                    break;
                case "coins_pack_160k":
                    respObj.putInt("coins", 192000);
                    break;
                case "coins_pack_160k_s":
                    respObj.putInt("coins", 192000);
                    respObj.putInt("b1", 5);
                    respObj.putInt("b2", 5);
                    respObj.putInt("b3", 5);
                    respObj.putInt("b4", 5);
                    respObj.putInt("b5", 5);
                    respObj.putInt("b6", 5);
                    respObj.putInt("b7", 5);
                    respObj.putInt("b8", 5);
                    break;
            }
           return respObj;
           
       }
     int getPaymentType(String receipt){
         int PaymentType=1;
         
         return PaymentType;
     }   
}
