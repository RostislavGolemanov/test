package sfs2x.extensions.games.ludo;

import com.smartfoxserver.v2.entities.Room;
import com.smartfoxserver.v2.annotations.Instantiation;
import com.smartfoxserver.v2.annotations.Instantiation.InstantiationMode;
import com.smartfoxserver.v2.entities.User;
import com.smartfoxserver.v2.entities.data.ISFSObject;
import com.smartfoxserver.v2.entities.data.SFSObject;
import com.smartfoxserver.v2.exceptions.SFSRuntimeException;
import com.smartfoxserver.v2.extensions.BaseClientRequestHandler;


@Instantiation(InstantiationMode.SINGLE_INSTANCE)
public class publicMessageHandler extends BaseClientRequestHandler
{
	 
	
	@Override
	public void handleClientRequest(User user, ISFSObject params)
	{
		// Check params
                if (!params.containsKey("m"))
			throw new SFSRuntimeException("Invalid request, one mandatory param is missing. Required 'x' and 'y'");
		
                LudoExtension gameExt = (LudoExtension) getParentExtension();
		int fromRoom = (int)user.getVariable("playIn").getIntValue();
                
                String Mess = (String)params.getUtfString("m");
		ISFSObject respObj = new SFSObject();
                int u=(int)user.getId();
                
                 
                 if(fromRoom!=gameExt.Lobby_Id){
                     
                     Room toRoom=(Room)gameExt.getParentZone().getRoomById(fromRoom);
                     if(toRoom!=null){
                       respObj.putUtfString("uN",user.getName());
		       respObj.putUtfString("msg",Mess);
                       send("pMsg", respObj, toRoom.getUserList());
                     }
                 }else{
                       respObj.putInt("u", u);
		       respObj.putUtfString("msg",Mess);
		      // send("pMsg", respObj, gameExt.lobbyUsers);
                 }
                
               
		 
                
					 
	}
	

}
