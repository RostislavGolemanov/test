package sfs2x.extensions.games.ludo;

import com.smartfoxserver.v2.entities.Room;
import com.smartfoxserver.v2.annotations.Instantiation;
import com.smartfoxserver.v2.annotations.Instantiation.InstantiationMode;
import com.smartfoxserver.v2.db.IDBManager;
import com.smartfoxserver.v2.entities.User;
import com.smartfoxserver.v2.entities.data.ISFSArray;
import com.smartfoxserver.v2.entities.data.ISFSObject;
import com.smartfoxserver.v2.entities.data.SFSArray;
import com.smartfoxserver.v2.entities.data.SFSObject;
import com.smartfoxserver.v2.entities.variables.SFSUserVariable;
import com.smartfoxserver.v2.entities.variables.UserVariable;
 
import com.smartfoxserver.v2.extensions.BaseClientRequestHandler;
import java.io.IOException;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Date;
import java.util.UUID;
import java.util.logging.Level;
import java.util.logging.Logger;

 

@Instantiation(InstantiationMode.SINGLE_INSTANCE)
public class insertJS_Error extends BaseClientRequestHandler
{
	 
	
	@Override
	public void handleClientRequest(User user, ISFSObject params)
	{
		// Check params
                 
		
                   LudoExtension gameExt = (LudoExtension) getParentExtension();
		     
                   int fromRoom = (int) user.getVariable("playIn").getIntValue();
                       roomInfo ROOM= gameExt.gRooms.get(fromRoom);
                       if(ROOM!=null){
                                UUID GID=ROOM.GID;
                               String insertSql = "INSERT INTO `js_error_log` (`player_id`,`error`,`game_id`) VALUES (?, ?, ?)";
                                  Object[] array12 = new Object[3];
                                           array12[0] = user.getVariable("pId").getIntValue();
                                           array12[1] = params.getUtfString("err");
                                           array12[2] = GID.toString();
                                ISFSArray res = gameExt.DBWRAPPER(insertSql, array12, 2);
                       }
	}
}
