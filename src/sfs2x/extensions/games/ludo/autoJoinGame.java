package sfs2x.extensions.games.ludo;


import com.smartfoxserver.v2.annotations.Instantiation;
import com.smartfoxserver.v2.annotations.Instantiation.InstantiationMode;
import com.smartfoxserver.v2.entities.User;
import com.smartfoxserver.v2.entities.data.ISFSObject;
import com.smartfoxserver.v2.entities.data.SFSObject;
import com.smartfoxserver.v2.exceptions.SFSRuntimeException;
import com.smartfoxserver.v2.extensions.BaseClientRequestHandler;


@Instantiation(InstantiationMode.SINGLE_INSTANCE)
public class autoJoinGame extends BaseClientRequestHandler
{
	 
	
	@Override
	public void handleClientRequest(User user, ISFSObject params)
	{
		// Check params
               
		
                  LudoExtension gameExt = (LudoExtension) getParentExtension();
                
                   int time=0;//params.getInt("t");
                   int bet=params.getInt("b");
                   int b1=0;
                   int b2=0;
                   int b3=0;
                   
                   int a1=user.getVariable("A_1").getIntValue();
                   int a2=user.getVariable("A_2").getIntValue();
                   int a3=user.getVariable("A_3").getIntValue();
                   int a4=user.getVariable("A_4").getIntValue();
                   int a5=user.getVariable("A_5").getIntValue();
                   
                  gameExt.autoJoin(user,time,bet,b1,b2,b3,a1,a2,a3,a4,a5);
                
                			 
	}
	

}
