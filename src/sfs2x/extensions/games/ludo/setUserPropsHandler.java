package sfs2x.extensions.games.ludo;

import java.util.Arrays;
import com.smartfoxserver.v2.core.ISFSEvent;
import com.smartfoxserver.v2.core.SFSEventParam;
import com.smartfoxserver.v2.entities.Room;
import com.smartfoxserver.v2.entities.User;
import com.smartfoxserver.v2.entities.data.ISFSObject;
import com.smartfoxserver.v2.entities.data.SFSObject;
import com.smartfoxserver.v2.entities.variables.SFSUserVariable;
import com.smartfoxserver.v2.entities.variables.UserVariable;
import com.smartfoxserver.v2.exceptions.SFSException;
import com.smartfoxserver.v2.exceptions.SFSJoinRoomException;
import com.smartfoxserver.v2.extensions.BaseServerEventHandler;
import java.io.IOException;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;

public class setUserPropsHandler extends BaseServerEventHandler
{
	@SuppressWarnings("unchecked")
        @Override 
        public void handleServerEvent(ISFSEvent paramISFSEvent) throws SFSException
	{
		LudoExtension gameExt = (LudoExtension) getParentExtension();
		User user = (User) paramISFSEvent.getParameter(SFSEventParam.USER);
                //Room lobby = gameExt.getLobbyRoom();
                
		              String UNAME=(String)user.getSession().getProperty("uName");
                              String FBNAME=(String)user.getSession().getProperty("uId");
                
                              UserVariable uName = new SFSUserVariable("uName",UNAME,true);
                              UserVariable uId = new SFSUserVariable("uId",FBNAME,false);
                              UserVariable pId = new SFSUserVariable("pId",(int)user.getSession().getProperty("pId"),true);
                              
                              UserVariable rM = new SFSUserVariable("rM",(Integer)user.getSession().getProperty("rM"),true);//real money
                              UserVariable uThumb = new SFSUserVariable("uThumb",(String)user.getSession().getProperty("uThumb"),true);
                              UserVariable playIn = new SFSUserVariable("playIn",0,true);
                              UserVariable uMoney = new SFSUserVariable("money",(Integer)user.getSession().getProperty("money"),true);
                              UserVariable uPlays = new SFSUserVariable("plays",(Integer)user.getSession().getProperty("plays"),true);
                              UserVariable uWins = new SFSUserVariable("wins",(Integer)user.getSession().getProperty("wins"),true);
                              UserVariable uPoints = new SFSUserVariable("points",(Integer)user.getSession().getProperty("points"),true);
                              UserVariable B_1 = new SFSUserVariable("B_1",(Integer)user.getSession().getProperty("b_1"),true);
                              UserVariable B_2 = new SFSUserVariable("B_2",(Integer)user.getSession().getProperty("b_2"),true);
                              UserVariable B_3 = new SFSUserVariable("B_3",(Integer)user.getSession().getProperty("b_3"),true);
                              UserVariable B_4 = new SFSUserVariable("B_4",(Integer)user.getSession().getProperty("b_4"),true);
                              UserVariable B_5 = new SFSUserVariable("B_5",(Integer)user.getSession().getProperty("b_5"),true);
                              UserVariable B_6 = new SFSUserVariable("B_6",(Integer)user.getSession().getProperty("b_6"),true);
                              UserVariable B_7 = new SFSUserVariable("B_7",(Integer)user.getSession().getProperty("b_7"),true);
                              UserVariable B_8 = new SFSUserVariable("B_8",(Integer)user.getSession().getProperty("b_8"),true);
                              UserVariable B_9 = new SFSUserVariable("B_9",(Integer)user.getSession().getProperty("b_9"),true);
                              UserVariable B_10 = new SFSUserVariable("B_10",(Integer)user.getSession().getProperty("b_10"),true);
                              UserVariable A_1 = new SFSUserVariable("A_1",(Integer)user.getSession().getProperty("a_1"),true);
                              UserVariable A_2 = new SFSUserVariable("A_2",(Integer)user.getSession().getProperty("a_2"),true);
                              UserVariable A_3 = new SFSUserVariable("A_3",(Integer)user.getSession().getProperty("a_3"),true);
                              UserVariable A_4 = new SFSUserVariable("A_4",(Integer)user.getSession().getProperty("a_4"),true);
                              UserVariable A_5 = new SFSUserVariable("A_5",(Integer)user.getSession().getProperty("a_5"),true);
                              UserVariable WB = new SFSUserVariable("WB",(Integer)user.getSession().getProperty("wb"),true);
                              UserVariable SV = new SFSUserVariable("SV",(Integer)user.getSession().getProperty("sound"),true);
                               UserVariable LT = new SFSUserVariable("LT",(Integer)user.getSession().getProperty("lT"),true);
                              UserVariable H = new SFSUserVariable("H",(Integer)user.getSession().getProperty("hints"),true);
                              UserVariable C = new SFSUserVariable("C",(Integer)user.getSession().getProperty("color"),true);
                               
                            
            try {
                  gameExt.sendStatisticLogIN((String)user.getSession().getProperty("uId"), "gameState", (Integer)user.getSession().getProperty("b_1"), (Integer)user.getSession().getProperty("b_2"), (Integer)user.getSession().getProperty("b_3"), (Integer)user.getSession().getProperty("b_4"), (Integer)user.getSession().getProperty("b_5"), (Integer)user.getSession().getProperty("b_6"), (Integer)user.getSession().getProperty("b_7"), (Integer)user.getSession().getProperty("b_8"),(Integer)user.getSession().getProperty("lT"));
                  
            
            } catch (IOException ex) {
                Logger.getLogger(setUserPropsHandler.class.getName()).log(Level.SEVERE, null, ex);
            }
                 
            getApi().setUserVariables(user, Arrays.asList(uName,uId, rM,uThumb,playIn,uMoney,uPlays,uWins,uPoints,pId,B_1,B_2,B_3,B_4,B_5,B_6,B_7,B_8,B_9,B_10,A_1,A_2,A_3,A_4,A_5,WB,SV,LT,H,C),false,true);
	    
                 
                    try {
                        gameExt.getApi().joinRoom(user, gameExt.Lobby, "",false,null,false,true);
                        
                    } catch (SFSJoinRoomException ex) {
                         Logger.getLogger(leaveRoom.class.getName()).log(Level.SEVERE, null, ex);
                    }
	}
}
