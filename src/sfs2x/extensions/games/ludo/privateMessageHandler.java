package sfs2x.extensions.games.ludo;


import com.smartfoxserver.v2.annotations.Instantiation;
import com.smartfoxserver.v2.annotations.Instantiation.InstantiationMode;
import com.smartfoxserver.v2.entities.User;
import com.smartfoxserver.v2.entities.data.ISFSObject;
import com.smartfoxserver.v2.entities.data.SFSObject;
import com.smartfoxserver.v2.exceptions.SFSRuntimeException;
import com.smartfoxserver.v2.extensions.BaseClientRequestHandler;


@Instantiation(InstantiationMode.SINGLE_INSTANCE)
public class privateMessageHandler extends BaseClientRequestHandler
{
	 
	
	@Override
	public void handleClientRequest(User user, ISFSObject params)
	{
		// Check params
                if (!params.containsKey("m"))
			throw new SFSRuntimeException("Invalid request, one mandatory param is missing. Required 'x' and 'y'");
		
               LudoExtension gameExt = (LudoExtension) getParentExtension();
                String Mess = (String)params.getUtfString("m");
                int uId = (int)params.getInt("u");
		User toUser=(User)gameExt.getApi().getUserById(uId);
                if(toUser!=null){
                   ISFSObject respObj = new SFSObject();
                   respObj.putUtfString("msg",Mess);
                   send("prMsg", respObj, toUser);
                }
                			 
	}
	

}
