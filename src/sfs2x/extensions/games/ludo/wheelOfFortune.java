package sfs2x.extensions.games.ludo;


import com.smartfoxserver.v2.annotations.Instantiation;
import com.smartfoxserver.v2.annotations.Instantiation.InstantiationMode;
import com.smartfoxserver.v2.entities.Room;
import com.smartfoxserver.v2.entities.User;
import com.smartfoxserver.v2.entities.data.ISFSObject;
import com.smartfoxserver.v2.entities.data.SFSObject;
import com.smartfoxserver.v2.exceptions.SFSRuntimeException;
import com.smartfoxserver.v2.extensions.BaseClientRequestHandler;


@Instantiation(InstantiationMode.SINGLE_INSTANCE)
public class wheelOfFortune extends BaseClientRequestHandler
{
	 
	
	@Override
	public void handleClientRequest(User user, ISFSObject params)
	{
		// Check params
               
	            LudoExtension gameExt = (LudoExtension) getParentExtension();
                  ISFSObject resObj = new SFSObject();
                  Room room=user.getLastJoinedRoom();
                  if(room.isActive()){
                        roomInfo ROOM=gameExt.gRooms.get(room.getId());
                        if(ROOM.gameFinished&&ROOM.haveprize[user.getPlayerId()-1]==0){

                            ROOM.haveprize[user.getPlayerId()-1]=1;
                            gameExt.setPrize(user);

                        }
                  
                  }
                			 
	}
	

}
