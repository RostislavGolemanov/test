package sfs2x.extensions.games.ludo;

import com.smartfoxserver.v2.entities.Room;
import com.smartfoxserver.v2.annotations.Instantiation;
import com.smartfoxserver.v2.annotations.Instantiation.InstantiationMode;
import com.smartfoxserver.v2.db.IDBManager;
import com.smartfoxserver.v2.entities.User;
import com.smartfoxserver.v2.entities.data.ISFSObject;
import com.smartfoxserver.v2.entities.data.SFSObject;
import com.smartfoxserver.v2.entities.variables.SFSUserVariable;
import com.smartfoxserver.v2.entities.variables.UserVariable;
 
import com.smartfoxserver.v2.extensions.BaseClientRequestHandler;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.Arrays;

 

@Instantiation(InstantiationMode.SINGLE_INSTANCE)
public class setAnim extends BaseClientRequestHandler
{
	 
	
	@Override
	public void handleClientRequest(User user, ISFSObject params)
	{
		// Check params
                 
		
                LudoExtension gameExt = (LudoExtension) getParentExtension();
		   IDBManager dbManager = gameExt.getParentZone().getDBManager();
                   Statement stmt;
                  
                   Connection connection=null;
               
                   int type = (int)params.getInt("t");
                   int kind = (int)params.getInt("k");
		   int player_id=user.getVariable("pId").getIntValue();
                   int money=user.getVariable("money").getIntValue();
                   String query="UPDATE `animation` SET `active`=false WHERE `player_id`=? AND `type`=?";
                   Object[] array = new Object[2];
                                    array[0] = player_id;
                                    array[1] = type;
                   
                try{
                      
                    connection=dbManager.getConnection();
                    stmt = connection.createStatement();
                    dbManager.executeUpdate(query,array);
                            
                           
                                  
                                   
                                   
                                  
                                     String query2="UPDATE `animation` SET `active`=true WHERE `player_id`=? AND `type`=? AND `type_kind`=?;";
                                     Object[] array2 = new Object[3];
                                     array2[0] = player_id;
                                     array2[1] = type;
                                     array2[2] = kind;
                                     dbManager.executeUpdate(query2,array2);
                                  
                                 
                                
                                  
                                 if(type==5){
                                      UserVariable A_1 = new SFSUserVariable("A_1",kind,true);
                                       getApi().setUserVariables(user, Arrays.asList(A_1));
                                 }else if(type==2){
                                     UserVariable A_2 = new SFSUserVariable("A_2",kind,true);
                                       getApi().setUserVariables(user, Arrays.asList(A_2));
                                 }else if(type==8){
                                     UserVariable A_3 = new SFSUserVariable("A_3",kind,true);
                                       getApi().setUserVariables(user, Arrays.asList(A_3));
                                 }else if(type==3){
                                     UserVariable A_4 = new SFSUserVariable("A_4",kind,true);
                                       getApi().setUserVariables(user, Arrays.asList(A_4));
                                 }else if(type==6){
                                     UserVariable A_5 = new SFSUserVariable("A_5",kind,true);
                                       getApi().setUserVariables(user, Arrays.asList(A_5));
                                 }
                              
                             
                
                }catch(SQLException sqle){
                     trace("ERROR "+sqle);
                     sqle.printStackTrace();
                        
                                
                }finally{
                      trace("FINALLY");
                        try{
                           if(connection!=null) connection.close();
                        }catch(SQLException sqle)
                        {
                             trace("It shouldnt happen connection not closed");
                        }
                 }
                
	}
	

}
