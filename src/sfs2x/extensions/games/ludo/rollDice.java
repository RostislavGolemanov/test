package sfs2x.extensions.games.ludo;


import com.smartfoxserver.v2.annotations.Instantiation;
import com.smartfoxserver.v2.annotations.Instantiation.InstantiationMode;
import com.smartfoxserver.v2.entities.Room;
import com.smartfoxserver.v2.entities.User;
import com.smartfoxserver.v2.entities.data.ISFSArray;
import com.smartfoxserver.v2.entities.data.ISFSObject;
import com.smartfoxserver.v2.entities.data.SFSObject;
import com.smartfoxserver.v2.entities.variables.RoomVariable;
import com.smartfoxserver.v2.entities.variables.SFSRoomVariable;
import com.smartfoxserver.v2.exceptions.SFSRuntimeException;
import com.smartfoxserver.v2.exceptions.SFSVariableException;
import com.smartfoxserver.v2.extensions.BaseClientRequestHandler;
import java.io.IOException;
import static java.lang.System.err;
import java.util.Arrays;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;


@Instantiation(InstantiationMode.SINGLE_INSTANCE)
public class rollDice extends BaseClientRequestHandler
{
	 
	
	@Override
	public void handleClientRequest(User user, ISFSObject params)
	{
		// Check params
               
		    
                  String fbId="0";
                  int plId=0;
                  int fromRoom=-1;
                  int player_id=0;
                   
                    try{
                        fromRoom = user.getVariable("playIn").getIntValue();
                        fbId=user.getVariable("uId").getStringValue();
                        plId=user.getPlayerId();
                        player_id=user.getVariable("pId").getIntValue();
                    }catch(Exception e){
                        
                    }
                     if(fromRoom>0){
                         int DICEIS=params.getInt("d");
                         LudoExtension gameExt = (LudoExtension) getParentExtension();
                         gameExt.ROLL(fromRoom,fbId,plId,DICEIS,player_id); 
                     }
                	
                    
	}
        
        
        
        
        
        
        
        
	

}
