package sfs2x.extensions.games.ludo;

import com.smartfoxserver.v2.db.IDBManager;
import com.smartfoxserver.v2.entities.User;
import com.smartfoxserver.v2.entities.data.ISFSObject;
import com.smartfoxserver.v2.exceptions.SFSRuntimeException;
import com.smartfoxserver.v2.extensions.BaseClientRequestHandler;
import java.sql.SQLException;

public class saveSettsHandler extends BaseClientRequestHandler
{
	@Override
	public void handleClientRequest(User user, ISFSObject params)
	{
            
            
                 if (!params.containsKey("S") )
			throw new SFSRuntimeException("Invalid request, one mandatory param is missing. Required 'x' and 'y'");
		
                 LudoExtension gameExt = (LudoExtension) getParentExtension();
		 
                 int fromRoom = (int)user.getVariable("playIn").getIntValue();
                 int setts = (int)params.getInt("S");
                 int uId= (int)user.getVariable("uId").getIntValue();
                 IDBManager dbManager = gameExt.getParentZone().getDBManager();
                 if(uId>0){
                    try{
                            
                           String sql="UPDATE game_ranking SET settings= ? WHERE game_id=? AND user_id=?;";

                           Object[] array = new Object[3];
                           array[0] = setts;
                           array[1] = gameExt.gID;
                           array[2] = uId;

                           dbManager.executeUpdate(sql, array);

                       }catch(SQLException e){

                           trace("Error writing winner");
                       }
                 }
	}
}
