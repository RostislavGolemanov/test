package sfs2x.extensions.games.ludo;

import java.util.Timer;
import com.smartfoxserver.v2.entities.Room;
import com.smartfoxserver.v2.entities.SFSUser;
import com.smartfoxserver.v2.entities.User;
import com.smartfoxserver.v2.entities.data.ISFSObject;
import com.smartfoxserver.v2.entities.data.SFSObject;
import com.smartfoxserver.v2.exceptions.SFSJoinRoomException;
import java.util.Arrays;
import java.util.TimerTask;
import java.util.UUID;
import java.util.logging.Level;
import java.util.logging.Logger;
public class roomInfo  
{
        
         Room ROOM;
         int gameTime=0;
         Timer egTimer;
         Timer turnTimer;
         
         int[] MovesCount={0,0,0,0};
         int[] pawnsOUT={3,3,3,3};
         int[][] mCount={{-1,-1,-1,-1},{-1,-1,-1,-1},{-1,-1,-1,-1},{-1,-1,-1,-1}};
         int[][] mPoints={{0,0,0,0},{0,0,0,0},{0,0,0,0},{0,0,0,0}};
         int[][] pPlace={{0,0,0,0},{0,0,0,0},{0,0,0,0},{0,0,0,0}};
         int[][] busters={{0,0,0,0,0,0,0,0,0,0},{0,0,0,0,0,0,0,0,0,0},{0,0,0,0,0,0,0,0,0,0},{0,0,0,0,0,0,0,0,0,0}};
         int[][] animations={{1,1,1,1,1},{1,1,1,1,1},{1,1,1,1,1},{1,1,1,1,1}};
         int[][] shields={{-1,-1,-1},{-1,-1,-1},{-1,-1,-1},{-1,-1,-1}};
         int[][] pawnsIn={{0,0},{0,0},{0,0},{0,0}};
         int[][] shieldPlace={{-1,-1,-1},{-1,-1,-1},{-1,-1,-1},{-1,-1,-1}};
         int[][] ghosts={{-1,-1,-1},{-1,-1,-1},{-1,-1,-1},{-1,-1,-1}};
        
         int[] moveShield={0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0};
         int[] sticky={0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0};
         int[] tweens={0,0,0,0};
         int[] tweenNum={1,1,1,1};
         int[] vips={0,0,0,0};
         int[] bombs={0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0};
         int[] allPawns={-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1};
         int[] teleportDirection={1,1,1,1,1,1,1,1};
         int HAVESHIELD=0;
         int numSTICKY=0;
         int RID=0;
         int numPawns=3;
         int numPlayers=0;
         int currPlayers=0;
         int realPlayers=0;
         int roomPlayers=0;
         int[] bet={0,0,0,0};
         int[] pIds={0,0,0,0};
         int[] plIds={0,0,0,0};
         int[] SIXES={0,0,0,0};
         int[] NOTSIXES={0,0,0,0};
         int[] haveprize={0,0,0,0};
         String[] fbIds={"","","",""};
         int[] missedCount={0,0,0,0};
         String[] players={"","","",""};
         int diceSide=6;
         UUID GID;
         Boolean mustPlay=false;
         Boolean gameStarted=false;
         Boolean gameFinished=false;
         Boolean CANPLAY=false;
         int TURN=0;
    	 int Round=0;
         Boolean isPrivate=false;
         Boolean PROCESSING=false;
         int BACKUPTIMER=0;
         int missedPlayCount=0;
         int ERRORNUM=0;
         int GetDeck=0;
         public roomInfo(Room room, Boolean isPrivate)
	{
             ROOM=room;
              
             egTimer = new Timer();
             if(!isPrivate){
                endTimeHandler ET=new endTimeHandler(this);
                egTimer.schedule(new TimerTask() {
                 @Override
                    public void run() {

                       egTimer.cancel();
                       egTimer.purge();
                        setStartTime();
                      /* if(currPlayers>2){

                           numPlayers=currPlayers;
                           //LudoExtension gameExt =(LudoExtension) ROOM.getZone().getExtension();
                           setStartTime();
                       }else{

                           numPlayers=3;
                       }*/

                    }
                 }, 20000); //20000
             }
	}
        
        
        public void setStartTime(){
            LudoExtension gameExt =(LudoExtension) ROOM.getZone().getExtension();
            
            for(int pp=0;pp<4;pp++){
                if(bet[pp]==0&&pIds[pp]>0){
                  
                    try {
                        gameExt.getApi().joinRoom(ROOM.getUserByPlayerId(pp+1), gameExt.Lobby, "",false,ROOM,false,true);
                        currPlayers--;
                    } catch (SFSJoinRoomException ex) {
                        Logger.getLogger(roomInfo.class.getName()).log(Level.SEVERE, null, ex);
                    }
                }
            }
            realPlayers=currPlayers;
            if(currPlayers<3){
                String[] bNames={"Shiny","Lucky","Friendly","Bossy","Dandy","Jolly","Risky","Gravy","Fizzy","Crunky","Spiffy","Skippy","Hoopy"};
                for(int p=currPlayers;p<3;p++){
                    int nameN=(int)(Math.random()*bNames.length);
                    players[p]="0|"+bNames[nameN]+"|a|p|0";
                    pIds[p]=-1;
                    currPlayers++;
                }
            }
             
             gameExt.lockRoom(RID);
            
             ISFSObject respObj = new SFSObject();
                        respObj.putUtfString("ul", players[0]+","+players[1]+","+players[2]+","+players[3]);
                        respObj.putUtfString("bet",bet[0]+","+bet[1]+","+bet[2]+","+bet[3]);
                        respObj.putInt("t", gameExt.waitTimeBoosters);
                         
		 gameExt.send("stT", respObj, ROOM.getUserList());
                
            
            if(egTimer!=null){
              egTimer.cancel();
              egTimer.purge(); 
            }
            
           egTimer = new Timer();
            endTimeHandler ET=new endTimeHandler(this);
            egTimer.schedule(new TimerTask() {
             @Override
                public void run() {
                   
                   egTimer.cancel();
                   egTimer.purge();
                    
                   
                      LudoExtension gameExt =(LudoExtension) ROOM.getZone().getExtension();
                      
                       gameExt.startGame(RID);
                    
                    
                }
             }, gameExt.waitTimeBoosters*1000); //15000
            
        }
        
        void setTimer(){
            
            if(egTimer!=null){
              egTimer.cancel();
              egTimer.purge(); 
            }
            
            egTimer = new Timer();
            endTimeHandler ET=new endTimeHandler(this);
            egTimer.schedule(ET, 1000,1000);
        }
        
       
        int getTime(){
            return gameTime;
        }
        
        
        public void timeTask(){
             LudoExtension gameExt =(LudoExtension) ROOM.getZone().getExtension();
            gameTime--;
             BACKUPTIMER++;
             if(BACKUPTIMER>23){
                 gameExt.ChangeTurn(RID);
             }
            if(gameTime<1){
                remove();
                int WP=0;
                int winner=1;
                int ExtraWinner=0;
                 for(int p=0;p<4;p++){
                     if(WP<MovesCount[p]){
                         WP=MovesCount[p];
                         winner=p+1;
                         ExtraWinner=0;
                     }else if(WP==MovesCount[p]){
                         ExtraWinner=p+1;
                     }
                 }
                         
                 gameExt.finishGame(ROOM,this, winner,ExtraWinner);
            }
        }
        
        public int getMoveCount(int player){
            return MovesCount[player-1];
        }
        public String getMoveCountAll(){
             
            return Arrays.toString(MovesCount);
        }
        public void addMoveCount(int player,int move,int pawnNum){
            
             if(pawnNum<3&&player<5){
               MovesCount[player-1]+=move;
               if(MovesCount[player-1]<0)MovesCount[player-1]=0;
               mPoints[player-1][pawnNum]+=move;
             }
             
        }
        
        public void setTimerTurn(int T){
             TURN=T;
             BACKUPTIMER=0;
            
            if(turnTimer!=null){
               turnTimer.cancel();
               turnTimer.purge(); 
            }
            
           
            
            if(pIds[TURN-1]==-1){//////////////////////BOT/////////////////////////////
                LudoExtension gameExt =(LudoExtension) ROOM.getZone().getExtension();
                
                if(!gameFinished){
                if(CANPLAY==false){
                     
                    turnTimer = new Timer();
                    turnTimer.schedule(new TimerTask() {
                     @Override
                        public void run() {
                                
                                  turnTimer.cancel();
                                  turnTimer.purge();
                                 if(!PROCESSING){
                                  LudoExtension gameExt =(LudoExtension) ROOM.getZone().getExtension();
                                  gameExt.ROLL(RID, "0", TURN,0,0);
                                   
                                  
                                }
                            }
                       }, 1500); //2200
                    
                    
                }else{ ////////////////////MUST PLAY/////////////////////
                    
                    turnTimer = new Timer();
                    turnTimer.schedule(new TimerTask() {
                     @Override
                     public void run() {

                             turnTimer.cancel();
                             turnTimer.purge();
                            if(!PROCESSING){
                    int CP=0;
                    LudoExtension gameExt =(LudoExtension) ROOM.getZone().getExtension();
                   // int[]  pawnsOut =gameExt.convertSFSArray(ROOM.getVariable("nOut").getSFSArrayValue());
                    int[]  pawnsFinished =gameExt.convertSFSArray(ROOM.getVariable("nFin").getSFSArrayValue());
                    Boolean FOUND=false;
                        if(pawnsOUT[TURN-1]+pawnsFinished[TURN-1]==gameExt.numPawns){
                            CP=-1;
                            FOUND=true;
                        }else{
                            
                             
                            for(int ENDPLACE2=0;ENDPLACE2<gameExt.squadNum;ENDPLACE2++){///// CHECK PAWN ON DECK
                                
                               if(allPawns[ENDPLACE2]>0) {

                                         
                                            int FOUNDCOLOR2=(int) Math.floor(allPawns[ENDPLACE2]/10);
                                              
                                                if (FOUNDCOLOR2 == TURN ) {
                                                    
                                                    if(sticky[ENDPLACE2]<1){
                                                      CP=ENDPLACE2;
                                                      FOUND=true;
                                                      break;
                                                    }
                                                }
                                           
                                  }
                             }
                            if(!FOUND){////////////////CHECK PAWN IN
                                int[][] INARR = pawnsIn;//ROOM.getVariable("InArr").getStringValue().split(",");
                                int[] myInArr=INARR[TURN-1];
                                 
                                     if(myInArr[0]!=0) {
                                         CP=101;
                                         FOUND=true;
                                     }else if(myInArr[1]!=0){
                                         CP=102;
                                         FOUND=true;
                                     }
                                   
                              
                            }
                        }
                           if(!FOUND&&pawnsOUT[TURN-1]>0){
                               CP=-1;
                           } 

                         int DICENUM=ROOM.getVariable("dN").getIntValue();
                           gameExt.MovePawn(TURN, RID, CP,DICENUM,null);
                     }
                     }
                    }, 1200); //1100
                }
               }
            ////////////////////////////////////////////////////////////////////////////////////////////////
                
            }else{
            
              setTTURN();
            }
           
         }
        
        void setTTURN(){
            if(turnTimer!=null){
               turnTimer.cancel();
               turnTimer.purge(); 
            }
               turnTimer = new Timer();
               endTimeHandler ET=new endTimeHandler(this);
               turnTimer.schedule(new TimerTask() {
               @Override
                public void run() {
                   
                   turnTimer.cancel();
                   turnTimer.purge();
                    
                   if(!PROCESSING){
                       LudoExtension gameExt =(LudoExtension) ROOM.getZone().getExtension();
                       gameExt.ChangeTurn(RID);
                   }
                    
                }
             }, 15000); 
        }
        
        
         int removeMoveCount(int player,int place){
           LudoExtension gameExt =(LudoExtension) ROOM.getZone().getExtension();
            int RC=0;
           
            for(int p=0;p<4;p++){
               
                if(mCount[player-1][p]==place){
                    RC=(int)Math.round(mPoints[player-1][p]/2);
                     MovesCount[player-1]-=RC;
                       
                     if(MovesCount[player-1]<0)MovesCount[player-1]=0;
                     mPoints[player-1][p]=0;
                     mCount[player-1][p]=-1;
                     break;
                }
            }
            
            return(RC);
         }
        
         
         
         
        int removeUser(int plId){
            currPlayers--;
            realPlayers--;
             int UID=0;
            for(int p=0;p<4;p++){
                 
                if(pIds[p]==plId){
                    players[p]="";
                    //fbIds[p]="";
                    pIds[p]=0;
                    bet[p]=0;
                    UID=p+1;
                    break;
                }
            }
            return(UID);
            //players[plId-1]="";
        }
        
        
        void remove(){
            if(egTimer!=null){
              egTimer.cancel();
              egTimer.purge(); 
            }
            if(turnTimer!=null){
              turnTimer.cancel();
              turnTimer.purge(); 
            }
            
            /*
            MovesCount=null;
            pawnsOUT=null;
            mCount=null;
            mPoints=null;
            pPlace=null;
            busters=null;
            animations=null;
            shields=null;
            pawnsIn=null;
            shieldPlace=null;
            moveShield=null;
            sticky=null;
            tweens=null;
            tweenNum=null;
            vips=null;
            bombs=null;
            allPawns=null;
            bet=null;
            pIds=null;
            plIds=null;
            haveprize=null;
            fbIds=null;
            missedCount=null;
            players=null;
            */
            
            
        }

        
        public void stopTimerTurn(){
            if(turnTimer!=null){
              turnTimer.cancel();
              turnTimer.purge(); 
            }
        }
        
    private void trace(String string) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }
        
        
}
