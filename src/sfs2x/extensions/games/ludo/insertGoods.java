/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package sfs2x.extensions.games.ludo;

import com.smartfoxserver.v2.db.IDBManager;
import com.smartfoxserver.v2.entities.User;
import com.smartfoxserver.v2.entities.data.ISFSArray;
import com.smartfoxserver.v2.entities.data.ISFSObject;
import com.smartfoxserver.v2.entities.data.SFSObject;
import com.smartfoxserver.v2.entities.variables.SFSUserVariable;
import com.smartfoxserver.v2.entities.variables.UserVariable;
import com.smartfoxserver.v2.exceptions.SFSVariableException;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.Arrays;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author IO_PC_24
 */
class insertGoods {
    
    public insertGoods(User user, ISFSObject Goods){
      LudoExtension gameExt = (LudoExtension) user.getZone().getExtension();
      IDBManager dbManager = gameExt.getParentZone().getDBManager();
    
       int player_id=user.getVariable("pId").getIntValue();
       int B1=user.getVariable("B_1").getIntValue();
       int B2=user.getVariable("B_2").getIntValue();
       int B3=user.getVariable("B_3").getIntValue();
       int B4=user.getVariable("B_4").getIntValue();
       int B5=user.getVariable("B_5").getIntValue();
       int B6=user.getVariable("B_6").getIntValue();
       int B7=user.getVariable("B_7").getIntValue();
       int B8=user.getVariable("B_8").getIntValue();
       int B9=user.getVariable("B_9").getIntValue();
       int B10=user.getVariable("B_10").getIntValue();
       int coins=user.getVariable("rM").getIntValue();
        if(Goods.getInt("coins")!=null){
          
          
          
           String query4="UPDATE `game_state`  SET `real_coins`=`real_coins`+? WHERE `player_id`=?";
                 Object[] array4 = new Object[2];
                          array4[0] = Goods.getInt("coins");
                          array4[1] = player_id;
           ISFSArray res4= gameExt.DBWRAPPER(query4, array4, 3);
           
               coins+=Goods.getInt("coins");
               UserVariable uMoney = new SFSUserVariable("rM",coins,false);
              try {
                  user.setVariable(uMoney);
              } catch (SFSVariableException ex) {
                  Logger.getLogger(insertGoods.class.getName()).log(Level.SEVERE, null, ex);
              }
          
          
      
      }
      
      if(Goods.getInt("b1")!=null&&Goods.getInt("b2")!=null&&Goods.getInt("b3")!=null&&Goods.getInt("b4")!=null&&Goods.getInt("b5")!=null&&Goods.getInt("b6")!=null&&Goods.getInt("b7")!=null&&Goods.getInt("b8")!=null){
              
          boolean Success2=insertAllBoosters(player_id,Goods,dbManager);
          if(Success2){
               B1+=Goods.getInt("b1");
               B2+=Goods.getInt("b2");
               B3+=Goods.getInt("b3");
               B4+=Goods.getInt("b4");
               B5+=Goods.getInt("b5");
               B6+=Goods.getInt("b6");
               B7+=Goods.getInt("b7");
               B8+=Goods.getInt("b8");
          }
                      
      }
      
           UserVariable B_1 = new SFSUserVariable("B_1",B1,true);
           UserVariable B_2 = new SFSUserVariable("B_2",B2,true);
           UserVariable B_3 = new SFSUserVariable("B_3",B3,true);
           UserVariable B_4 = new SFSUserVariable("B_4",B4,true);
           UserVariable B_5 = new SFSUserVariable("B_5",B5,true);
           UserVariable B_6 = new SFSUserVariable("B_6",B6,true);
           UserVariable B_7 = new SFSUserVariable("B_7",B7,true);
           UserVariable B_8 = new SFSUserVariable("B_8",B8,true);
           UserVariable B_9 = new SFSUserVariable("B_9",B9,true);
           UserVariable B_10 = new SFSUserVariable("B_10",B10,true);
           UserVariable uMoney = new SFSUserVariable("rM",coins,true);
           gameExt.getApi().setUserVariables(user, Arrays.asList(B_1,B_2,B_3,B_4,B_5,B_6,B_7,B_8,B_9,B_10,uMoney));
      
                     ISFSObject respObj = new SFSObject();
                     respObj.putInt("b1", B1);
                     respObj.putInt("b2", B2);
                     respObj.putInt("b3", B3);
                     respObj.putInt("b4", B4);
                     respObj.putInt("b5", B5);
                     respObj.putInt("b6", B6);
                     respObj.putInt("b7", B7);
                     respObj.putInt("b8", B8);
                     respObj.putInt("b9", B9);
                     respObj.putInt("b10", B10);
                     int coins_game=user.getVariable("money").getIntValue();
                     coins_game+=coins;
                     respObj.putInt("m", coins_game);
		   gameExt.send("buySuccess", respObj,user);
      
    }
    private boolean insertAllBoosters(int player_id,ISFSObject Goods,IDBManager dbManager){
        Connection connection=null;
        try {
            
            connection=dbManager.getConnection();
          String boosterSql = "INSERT INTO `booster` (`player_id`,`booster_type`,`booster_count`) VALUES (?, ?, ?) ON DUPLICATE KEY UPDATE `booster_type`=?,`booster_count`=`booster_count`+?";
             //String boosterSql = "UPDATE `booster` SET `player_id`=?,`booster_type`=?,`booster_count`=? WHERE player_id=?";
            PreparedStatement pstmt = connection.prepareStatement(boosterSql);
                                         for(int B=1;B<9;B++){
                                             pstmt.setInt(1,player_id);
                                             pstmt.setInt(2,B);
                                             pstmt.setInt(3,5);
                                             pstmt.setInt(4,B);
                                             pstmt.setInt(5,5);
                                             pstmt.addBatch();
                                         }
                                         pstmt.executeBatch(); 
            return true;
        } catch (SQLException ex) {
            
           Logger.getLogger(insertGoods.class.getName()).log(Level.SEVERE, null, ex);
            return false;
             
        } finally{
            try{
                if(connection!=null) connection.close();
             }catch(SQLException sqle)
                 {
                //trace("It shouldnt happen connection not closed");
                 }
        }
    }
    private boolean insertCoinsDB(int player_id,int coins,IDBManager dbManager){
          
        String query4="UPDATE `game_state`  SET `real_coins`=`real_coins`+? WHERE `player_id`=?";
                 Object[] array4 = new Object[2];
                          array4[0] = coins;
                          array4[1] = player_id;
          try {
              dbManager.executeUpdate(query4,array4);
              return true;
          } catch (SQLException ex) {
              return false;
               
          }
    }
    
     
}

