package sfs2x.extensions.games.ludo;

import com.smartfoxserver.v2.entities.Room;
import com.smartfoxserver.v2.annotations.Instantiation;
import com.smartfoxserver.v2.annotations.Instantiation.InstantiationMode;
import com.smartfoxserver.v2.db.IDBManager;
import com.smartfoxserver.v2.entities.User;
import com.smartfoxserver.v2.entities.data.ISFSArray;
import com.smartfoxserver.v2.entities.data.ISFSObject;
import com.smartfoxserver.v2.entities.data.SFSArray;
import com.smartfoxserver.v2.entities.data.SFSObject;
import com.smartfoxserver.v2.entities.variables.SFSUserVariable;
import com.smartfoxserver.v2.entities.variables.UserVariable;
 
import com.smartfoxserver.v2.extensions.BaseClientRequestHandler;
import java.io.IOException;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.Arrays;
import java.util.logging.Level;
import java.util.logging.Logger;

 

@Instantiation(InstantiationMode.SINGLE_INSTANCE)
public class getAnim extends BaseClientRequestHandler
{
	 
	
	@Override
	public void handleClientRequest(User user, ISFSObject params)
	{
		// Check params
                 
		
                   LudoExtension gameExt = (LudoExtension) getParentExtension();
		      ISFSObject respObj = new SFSObject();
		            // SELECT animation_pack.*, (SELECT COUNT(animation.animation_id) FROM animation WHERE player_id = 21 AND animation_pack.animation_type = animation.type AND animation_pack.animation_kind = animation.type_kind) as havings FROM `animation_pack` WHERE animation_pack.animation_kind != 1     
                      String query8="SELECT animation.player_id, animation.type as animation_type, animation.type_kind as animation_kind, animation.active, (SELECT animation_pack.animation_name FROM animation_pack WHERE animation_pack.animation_type = animation.type AND animation_pack.animation_kind = animation.type_kind) as animation_name, (SELECT animation_pack.img FROM animation_pack WHERE animation_pack.animation_type = animation.type AND animation_pack.animation_kind = animation.type_kind) as img FROM `animation` WHERE animation.player_id ="+user.getVariable("pId").getIntValue();    
                           //user.getVariable("uId").getIntValue();
                      ISFSArray res8=gameExt.DBWRAPPER(query8, null, 1);
                      respObj.putSFSArray("aP", res8);
                      send("myAnim", respObj, user);
                
	}
	

}
