package sfs2x.extensions.games.ludo;

import com.smartfoxserver.v2.entities.Room;
import com.smartfoxserver.v2.annotations.Instantiation;
import com.smartfoxserver.v2.annotations.Instantiation.InstantiationMode;
import com.smartfoxserver.v2.db.IDBManager;
import com.smartfoxserver.v2.entities.User;
import com.smartfoxserver.v2.entities.data.ISFSObject;
import com.smartfoxserver.v2.entities.data.SFSObject;
import com.smartfoxserver.v2.entities.variables.SFSUserVariable;
import com.smartfoxserver.v2.entities.variables.UserVariable;
import com.smartfoxserver.v2.exceptions.SFSVariableException;
 
import com.smartfoxserver.v2.extensions.BaseClientRequestHandler;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.Arrays;
import java.util.logging.Level;
import java.util.logging.Logger;

 

@Instantiation(InstantiationMode.SINGLE_INSTANCE)
public class setSound extends BaseClientRequestHandler
{
	 
	
	@Override
	public void handleClientRequest(User user, ISFSObject params)
	{
		// Check params
                 
		
                   LudoExtension gameExt = (LudoExtension) getParentExtension();
		   IDBManager dbManager = gameExt.getParentZone().getDBManager();
                   Statement stmt;
                  
                   Connection connection=null;
               
                   int sound = (int)params.getInt("s");
		   int player_id=user.getVariable("pId").getIntValue();
                   int SVOLUME=(sound==1)?70:0;
                   String query="UPDATE `game_settings`  SET `sound_volume`=? WHERE `player_id`=?";
                
                try{
                      
                    connection=dbManager.getConnection();
                                     Object[] array = new Object[2];
                                     array[0] = SVOLUME;
                                     array[1] = player_id;
                    dbManager.executeUpdate(query,array);
                             
                                   
                                 UserVariable SV = new SFSUserVariable("SV",SVOLUME,true);
                                 user.setVariable(SV);
                                
                                 
                            
                
                }catch(SQLException sqle){
                     trace("ERROR "+sqle);
                     sqle.printStackTrace();
                        
                                
                } catch (SFSVariableException ex) {
                Logger.getLogger(setSound.class.getName()).log(Level.SEVERE, null, ex);
            }finally{
                      
                        try{
                           if(connection!=null) connection.close();
                        }catch(SQLException sqle)
                        {
                             trace("It shouldnt happen connection not closed");
                        }
                 }
                
	}
	

}
