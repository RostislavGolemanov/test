package sfs2x.extensions.games.ludo;


import com.smartfoxserver.v2.core.ISFSEvent;
import com.smartfoxserver.v2.core.SFSEventParam;
import com.smartfoxserver.v2.entities.Room;
import com.smartfoxserver.v2.entities.User;

import com.smartfoxserver.v2.exceptions.SFSException;
import com.smartfoxserver.v2.extensions.BaseServerEventHandler;


public class OnUserLeaveRoom extends BaseServerEventHandler
{
	@SuppressWarnings("unchecked")
        @Override 
        public void handleServerEvent(ISFSEvent event) throws SFSException
	{
		LudoExtension gameExt = (LudoExtension) getParentExtension();
		 
		// Get event params
		User user = (User) event.getParameter(SFSEventParam.USER);
                Room room = (Room) event.getParameter(SFSEventParam.ROOM);
                 
		gameExt.setUserGone(user,room.getId());
                
                
                  
	}
}
