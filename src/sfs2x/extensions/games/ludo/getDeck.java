package sfs2x.extensions.games.ludo;


import com.smartfoxserver.v2.annotations.Instantiation;
import com.smartfoxserver.v2.annotations.Instantiation.InstantiationMode;
import com.smartfoxserver.v2.entities.Room;
import com.smartfoxserver.v2.entities.User;
import com.smartfoxserver.v2.entities.data.ISFSArray;
import com.smartfoxserver.v2.entities.data.ISFSObject;
import com.smartfoxserver.v2.entities.data.SFSArray;
import com.smartfoxserver.v2.entities.data.SFSObject;
import com.smartfoxserver.v2.entities.variables.RoomVariable;
import com.smartfoxserver.v2.entities.variables.SFSRoomVariable;
import com.smartfoxserver.v2.entities.variables.SFSUserVariable;
import com.smartfoxserver.v2.entities.variables.UserVariable;
import com.smartfoxserver.v2.exceptions.SFSRuntimeException;
import com.smartfoxserver.v2.exceptions.SFSVariableException;
import com.smartfoxserver.v2.extensions.BaseClientRequestHandler;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.Iterator;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;


@Instantiation(InstantiationMode.SINGLE_INSTANCE)
public class getDeck extends BaseClientRequestHandler
{
	 
	
	@Override
	public void handleClientRequest(User user, ISFSObject params)
	{
		 
               try{
		 
                  LudoExtension gameExt = (LudoExtension) getParentExtension();
                  ISFSObject resObj = new SFSObject();
                 
                  int fromRoom = user.getVariable("playIn").getIntValue();
                  String fbId=user.getVariable("uId").getStringValue();
                  Room gameRoom=gameExt.getParentZone().getRoomById(fromRoom);
                  roomInfo RI=gameExt.gRooms.get(fromRoom);
                  if(RI.GetDeck>5)return;
                  RI.GetDeck++;
                 
                  int[]SENDDECK=RI.allPawns;
                  int TURN=gameRoom.getVariable("Trn").getIntValue();
                  
                     ISFSObject respObj2 = new SFSObject();
		     respObj2.putUtfString("D", Arrays.toString(SENDDECK));
                     respObj2.putUtfString("I", Arrays.toString(RI.pawnsIn[0])+"|"+Arrays.toString(RI.pawnsIn[1])+"|"+Arrays.toString(RI.pawnsIn[2])+"|"+Arrays.toString(RI.pawnsIn[3]));
                     respObj2.putUtfString("H", RI.pawnsOUT[0]+"|"+RI.pawnsOUT[1]+"|"+RI.pawnsOUT[2]+"|"+RI.pawnsOUT[3]);
                     respObj2.putUtfString("GM", Arrays.toString(RI.tweenNum));
                     String shArr=Arrays.toString(RI.shieldPlace[0])+"|"+Arrays.toString(RI.shieldPlace[1])+"|"+Arrays.toString(RI.shieldPlace[2])+"|"+Arrays.toString(RI.shieldPlace[3]);
                            shArr=shArr.replace(" ","");
                     respObj2.putUtfString("SH", shArr);
                     
                      String gsArr=Arrays.toString(RI.ghosts[0])+"|"+Arrays.toString(RI.ghosts[1])+"|"+Arrays.toString(RI.ghosts[2])+"|"+Arrays.toString(RI.ghosts[3]);
                            gsArr=gsArr.replace(" ","");
                     respObj2.putUtfString("GH", gsArr);
                     respObj2.putUtfString("TD", Arrays.toString(RI.teleportDirection));       
                     respObj2.putInt("t", TURN);
                      respObj2.putInt("ot",TURN);
                           respObj2.putInt("tm",RI.gameTime);
                           respObj2.putInt("np",RI.MovesCount[TURN-1]);
                     send("CT", respObj2, user); 
               }catch(Exception e){}
                			 
	}
	

}
