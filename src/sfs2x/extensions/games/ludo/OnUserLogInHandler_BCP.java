



package sfs2x.extensions.games.ludo;

import com.google.gson.JsonArray;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import java.sql.Connection;
import java.sql.Statement;
import java.sql.ResultSet;
import java.sql.SQLException;
import com.smartfoxserver.v2.db.IDBManager;
import com.smartfoxserver.v2.core.ISFSEvent;
import com.smartfoxserver.v2.core.SFSEventParam;
import com.smartfoxserver.v2.entities.Room;
import com.smartfoxserver.v2.entities.data.ISFSObject;
import com.smartfoxserver.v2.exceptions.SFSException;
import com.smartfoxserver.v2.extensions.BaseServerEventHandler;
import com.smartfoxserver.bitswarm.sessions.ISession;
import com.smartfoxserver.v2.core.SFSConstants;
import com.smartfoxserver.v2.entities.User;
import com.smartfoxserver.v2.entities.data.ISFSArray;
import com.smartfoxserver.v2.entities.data.SFSObject;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.URL;
import java.net.URLConnection;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.logging.Level;
import java.util.logging.Logger;
 

import java.io.DataOutputStream;
import java.io.OutputStreamWriter;
import java.net.HttpURLConnection;
import java.sql.PreparedStatement;
import org.json.JSONObject;



public class OnUserLogInHandler_BCP extends BaseServerEventHandler
{
	@SuppressWarnings("unchecked")
        @Override 
        public void handleServerEvent(ISFSEvent event) throws SFSException
	{
            
             
                final String FB_LOGIN_URL = "https://graph.facebook.com/me";
		LudoExtension gameExt = (LudoExtension) getParentExtension();
		Room lobby = gameExt.getLobbyRoom();
		 
		// Get event params
		 String userName = (String) event.getParameter(SFSEventParam.LOGIN_NAME);
                 String passWord = (String) event.getParameter(SFSEventParam.LOGIN_PASSWORD);
                 ISFSObject logObj = (ISFSObject) event.getParameter(SFSEventParam.LOGIN_IN_DATA);
		 ISession session = (ISession)event.getParameter(SFSEventParam.SESSION);
                 ISFSObject outData = (ISFSObject) event.getParameter(SFSEventParam.LOGIN_OUT_DATA);
                 
                 int uId=0;
                 int TYPE=logObj.getInt("T");
                 
                 String uThumb="";
		 SFSObject LOG_NEW=null;
                  
                 IDBManager dbManager = gameExt.getParentZone().getDBManager();
                 Statement stmt;
                 
                 String query;
                 boolean skipQuerry=false;
                 Connection connection=null;
                 
                
                      
                    
                    query="SELECT `facebook_id`,`player_id`,`first_name` FROM `facebook_players` WHERE md5(CONCAT(`facebook_id`, `token`)) = '"+userName+"';" ;
                    
                    
                    if(TYPE==3||TYPE==2) {// MOBILE OR JAVASCRIPT
                         try{
                             // trace("TOKEN "+logObj.getUtfString("token"));
                              HttpURLConnection fb_connection = openConnection(FB_LOGIN_URL+"?access_token=" + logObj.getUtfString("token"));
                              LOG_NEW=GetUser(fb_connection);
                              LOG_NEW.putUtfString("token", logObj.getUtfString("token"));
                              query="SELECT `facebook_id`,`player_id`,`first_name` FROM `facebook_players` WHERE `facebook_id` = "+LOG_NEW.getLong("FbId")+";" ;
                               //  trace(query) ;                 
                          }catch(Exception e){
                          
                              trace(e);
                          
                          } 
                      
                    }else if(TYPE==4){  /////ENTER DEVICE WITHOUT FB
                        
                         String DEVICEID=logObj.getUtfString("device_id");
                         int player_id=getPlayerFromDeviceID(DEVICEID);
                         
                         if(player_id>0){
                             query="SELECT `facebook_id`,`player_id`,`first_name` FROM `facebook_players` WHERE `player_id` = "+player_id+";" ;
                             LOG_NEW=SetNewUser();
                            
                             outData.putUtfString(SFSConstants.NEW_LOGIN_NAME, "u"+player_id);
                             
                         }else{
                              LOG_NEW=SetNewUser();
                              LOG_NEW.putUtfString("token", "");
                              skipQuerry=true;
                              player_id=REGISTER(LOG_NEW);
                              createDeviceUser(player_id,DEVICEID);
                              setDefaultProperties(LOG_NEW.getUtfString("first_name"),Long.toString(LOG_NEW.getLong("FbId")),player_id,session,TYPE);
                              
                         }
                    }                
                  
                    
                   if(!skipQuerry){
                    
                   ISFSArray res = gameExt.DBWRAPPER(query, null, 1);//             stmt.executeQuery(query);
                  
                     if (res.size()>0) {//user found
                         
                          ISFSObject RESSET=res.getSFSObject(0);
                            session.setProperty("uName", RESSET.getUtfString("first_name"));
                            session.setProperty("lT", TYPE);//ludo Type
                            String FBNAME=Long.toString(RESSET.getLong("facebook_id"));
                            session.setProperty("uId",FBNAME );
                            session.setProperty("uThumb", "a");
                           int player_id=(int)(long)RESSET.getLong("player_id"); 
                            
                          
                            String query2="SELECT * FROM `animation` WHERE `player_id` = '"+player_id+"' AND `active`=1;";
                            ISFSArray res2 = gameExt.DBWRAPPER(query2, null, 1);
                            for (int r=0; r<res2.size();r++) {
                                ISFSObject RESSET2=res2.getSFSObject(r);
                                if(RESSET2.getInt("type")==5)session.setProperty("a_1", RESSET2.getInt("type_kind"));
                                else if(RESSET2.getInt("type")==2)session.setProperty("a_2", RESSET2.getInt("type_kind"));
                                else if(RESSET2.getInt("type")==8)session.setProperty("a_3", RESSET2.getInt("type_kind"));
                                else if(RESSET2.getInt("type")==7)session.setProperty("a_4", RESSET2.getInt("type_kind"));
                                else if(RESSET2.getInt("type")==3)session.setProperty("a_5", RESSET2.getInt("type_kind"));
                                
                            }
                            
                            String query3="SELECT * FROM `booster` WHERE `player_id` = '"+player_id+"';";
                            ISFSArray res3 = gameExt.DBWRAPPER(query3, null, 1);
                            
                            int B3=0;
                            int B4=0;
                            int VIP=0;
                            int COINS=0;
                                session.setProperty("b_6", 0);
                                session.setProperty("b_7", 0);
                                session.setProperty("b_8", 0);
                                session.setProperty("b_9", 0);
                                session.setProperty("b_10",0);
                            
                            for (int r=0; r<res3.size();r++) {
                                ISFSObject RESSET3=res3.getSFSObject(r);
                                session.setProperty("b_"+RESSET3.getInt("booster_type"), RESSET3.getInt("booster_count"));
                                if(RESSET3.getInt("booster_type")==3)B3=RESSET3.getInt("booster_count");
                                else if(RESSET3.getInt("booster_type")==4)B4=RESSET3.getInt("booster_count");
                            }
                            
                             String query4="SELECT `player_id`, `coins`,`real_coins`, `plays`, `wins`, `score`, `vip_date`, `cron_date` FROM `game_state` WHERE `player_id` = '"+player_id+"'";
                             ISFSArray res4 = gameExt.DBWRAPPER(query4, null, 1);
                             
                             Calendar calendar = Calendar.getInstance();
                             Date date = new Date();
                             java.text.SimpleDateFormat sdf = new java.text.SimpleDateFormat("yyyy-MM-dd");
                             String WriteDate=sdf.format(date);
                            
                             if (res4.size()>0) {
                                ISFSObject RESSET4=res4.getSFSObject(0);
                                session.setProperty("plays", RESSET4.getInt("plays"));
                                session.setProperty("points", RESSET4.getInt("score"));
                                session.setProperty("wins", RESSET4.getInt("wins"));
                                session.setProperty("money", RESSET4.getInt("coins"));
                                session.setProperty("rM", RESSET4.getInt("real_coins"));
                                COINS=RESSET4.getInt("coins");
                                String vipDate = Long.toString(RESSET4.getLong("vip_date"));
                                    try {
                                        Date date7;
                                        java.text.SimpleDateFormat sdf2 = new java.text.SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
                                        date7 = sdf2.parse(Long.toString(RESSET4.getLong("cron_date")));
                                        calendar.setTime(date7);
                                        long NOW=calendar.getTimeInMillis();
                                        session.setProperty("CD", NOW);// CRON DATE
                                    } catch (ParseException ex) {
                                        Logger.getLogger(OnUserLogInHandler.class.getName()).log(Level.SEVERE, null, ex);
                                    }
                                
                                
                                
                              
                            }
                            
                            String query10="SELECT `sound_volume`, `music_volume`, `hints`, `color` FROM `game_settings` WHERE `player_id` = '"+player_id+"'";
                            ISFSArray res10 = gameExt.DBWRAPPER(query10, null, 1);
                            
                            int Hints=1;
                           ////////////////////////////////HINTS AND SOUND/////////////////////////
                            if (res10.size()>0) {
                                ISFSObject RESSET10=res10.getSFSObject(0);
                                session.setProperty("sound", RESSET10.getInt("sound_volume"));
                                session.setProperty("music", RESSET10.getInt("music_volume"));
                                session.setProperty("hints", RESSET10.getInt("hints"));
                                session.setProperty("color", RESSET10.getInt("color"));
                                Hints=RESSET10.getInt("hints");
                                 
                            }else{
                                session.setProperty("sound", 70);
                                session.setProperty("music", 70);
                                session.setProperty("hints", 0);
                                session.setProperty("color", -1);
                                String query12="INSERT INTO `game_settings` (`player_id`,`hints`,`music_volume`,`sound_volume`) values(?,?,?,?)";
                                
                                Object[] array12 = new Object[4];
                                     array12[0] = player_id;
                                     array12[1] = 0;
                                     array12[2] = 70;
                                     array12[3] = 70;
                                ISFSArray res12=gameExt.DBWRAPPER(query12, array12, 2);
                            }
                           
                            ////////////////////////////////HINTS AND SOUND END/////////////////////////
                            int GIFT=0;
                            
                            String query5="SELECT `last_play`, `last_gift` FROM `day_gift` WHERE `player_id` = '"+player_id+"'";
                            ISFSArray res5=gameExt.DBWRAPPER(query5, null, 1);
                            int prize=1;
                            Date date1;
                             
                               
                            if (res5.size()>0) {
                                ISFSObject RESSET5=res5.getSFSObject(0);
                               try {
                                   date1 = sdf.parse(WriteDate);
                                   calendar.setTime(date1);
                                   String dbSqlDate = RESSET5.getUtfString("last_play");
                                   int last_gift=RESSET5.getInt("last_gift");
                                   Date date2;
                                   String query7="UPDATE `day_gift` SET `last_play`=?,`last_gift`=? WHERE `player_id`=?";
                                   date2 = sdf.parse(dbSqlDate);
                                   int D1=calendar.get(Calendar.MONTH)*31+calendar.get(Calendar.DAY_OF_MONTH);
                                   calendar.setTime(date2);
                                   int D2=calendar.get(Calendar.MONTH)*31+calendar.get(Calendar.DAY_OF_MONTH);
                                   int NumDays=D1-D2;
                                  
                                   
                                   if(NumDays==1){
                                         Object[] array = new Object[3];
                                            array[0] = WriteDate;
                                            if(last_gift<5)last_gift+=1;
                                            else           last_gift=1;
                                            array[1] =  last_gift;
                                            array[2] = player_id;
                                            GIFT=last_gift;
                                            String query8;
                                            String query9;
                                            Object[] array8 = new Object[3];
                                            Object[] array9 = new Object[2];
                                           
                                            
                                            if(GIFT==1){
                                                
                                                query8="UPDATE `booster` SET `booster_count`=`booster_count` + ? WHERE `player_id`=? AND `booster_type`=?";
                                                
                                                    array8[0] = 1;
                                                    array8[1] = player_id;
                                                    array8[2] = 4;
                                                    session.setProperty("b_4", B4+1);
                                                 ISFSArray res8=gameExt.DBWRAPPER(query8, array8, 3);
                                                 
                                                
                                                 
                                                  try {
                                                     int ludoType= (Integer)session.getProperty("lT");//ludo Type
                                                     String fbId=(String)session.getProperty("uId");
                                                     gameExt.sendStatisticWinCoin(fbId,"WIN_DAILY BUSTER",1000,null,ludoType);
                                                  } catch (IOException ex) {
                                                     Logger.getLogger(LudoExtension.class.getName()).log(Level.SEVERE, null, ex);
                                                 }    
                                                
                                                
                                                
                                            }else if(GIFT==2){
                                               
                                                query8="UPDATE `booster` SET `booster_count`=`booster_count` + ? WHERE `player_id`=? AND `booster_type`=?";
                                                
                                                    array8[0] = 1;
                                                    array8[1] = player_id;
                                                    array8[2] = 3;
                                                    session.setProperty("b_3", B3+1);
                                                    ISFSArray res8=gameExt.DBWRAPPER(query8, array8, 3);
                                               
                                               query9="UPDATE `game_state` SET `coins`=`coins` + ? WHERE `player_id`=?";
                                                
                                                    array9[0] = 1000;
                                                    array9[1] = player_id;
                                                    session.setProperty("money", COINS+1000);
                                                    ISFSArray res9=gameExt.DBWRAPPER(query9, array9, 3);
                                                    
                                                  try {
                                                     int ludoType= (Integer)session.getProperty("lT");//ludo Type
                                                     String fbId=(String)session.getProperty("uId");
                                                     gameExt.sendStatisticWinCoin(fbId,"WIN_DAILY BUSTER",2200,null,ludoType);
                                                  } catch (IOException ex) {
                                                     Logger.getLogger(LudoExtension.class.getName()).log(Level.SEVERE, null, ex);
                                                  }  
                                            }else if(GIFT==3){
                                                query8="UPDATE `booster` SET `booster_count`=`booster_count` + ? WHERE `player_id`=? AND `booster_type`=?";
                                                
                                                    array8[0] = 5;
                                                    array8[1] = player_id;
                                                    array8[2] = 4;
                                                    session.setProperty("b_3", B3+5);
                                                   ISFSArray res8=gameExt.DBWRAPPER(query8, array8, 3);
                                                    
                                                    try {
                                                     int ludoType= (Integer)session.getProperty("lT");//ludo Type
                                                     String fbId=(String)session.getProperty("uId");
                                                     gameExt.sendStatisticWinCoin(fbId,"WIN_DAILY BUSTER",5000,null,ludoType);
                                                    } catch (IOException ex) {
                                                     Logger.getLogger(LudoExtension.class.getName()).log(Level.SEVERE, null, ex);
                                                   }  
                                            }else if(GIFT==4){
                                                query8="UPDATE `booster` SET `booster_count`=`booster_count` + ? WHERE `player_id`=? AND `booster_type`=?";
                                                
                                                    array8[0] = 5;
                                                    array8[1] = player_id;
                                                    array8[2] = 3;
                                                    session.setProperty("b_3", B3+5);
                                                    ISFSArray res8=gameExt.DBWRAPPER(query8, array8, 3);
                                                    try {
                                                     int ludoType= (Integer)session.getProperty("lT");//ludo Type
                                                     String fbId=(String)session.getProperty("uId");
                                                     gameExt.sendStatisticWinCoin(fbId,"WIN_DAILY BUSTER",6000,null,ludoType);
                                                    } catch (IOException ex) {
                                                     Logger.getLogger(LudoExtension.class.getName()).log(Level.SEVERE, null, ex);
                                                   }  
                                                 
                                            }else if(GIFT==5){
                                                    query9="UPDATE `game_state` SET `coins`=`coins` + ? WHERE `player_id`=?";
                                                
                                                    array9[0] = 10000;
                                                    array9[1] = player_id;
                                                    session.setProperty("money", COINS+10000);
                                                    ISFSArray res9=gameExt.DBWRAPPER(query9, array9, 3);
                                                    
                                                    try {
                                                     int ludoType= (Integer)session.getProperty("lT");//ludo Type
                                                     String fbId=(String)session.getProperty("uId");
                                                     gameExt.sendStatisticWinCoin(fbId,"WIN_DAILY MONEY",10000,null,ludoType);
                                                    } catch (IOException ex) {
                                                     Logger.getLogger(LudoExtension.class.getName()).log(Level.SEVERE, null, ex);
                                                   }  
                                            }
                                            
                                          session.setProperty("wb", last_gift);
                                          ISFSArray res7=gameExt.DBWRAPPER(query7, array, 3);
                                    
                                   }else if(NumDays>1){
                                        
                                        Object[] array = new Object[3];
                                            array[0] = WriteDate;
                                            array[1] =  1;
                                            array[2] = player_id;
                                            GIFT=1;
                                          ISFSArray res7=gameExt.DBWRAPPER(query7, array, 3);
                                       
                                          String query11="UPDATE `booster` SET `booster_count`=`booster_count` + ? WHERE `player_id`=? AND `booster_type`=?";
                                                Object[] array11 = new Object[3];
                                                    array11[0] = 1;
                                                    array11[1] = player_id;
                                                    array11[2] = 4;
                                                    session.setProperty("b_4", B4+1);
                                                
                                                    ISFSArray res11=gameExt.DBWRAPPER(query11, array11, 3);
                                                
                                    }
                                } catch (ParseException ex) {
                                    Logger.getLogger(OnUserLogInHandler.class.getName()).log(Level.SEVERE, null, ex);
                                }
                            }else{
                                 String query6="INSERT INTO `day_gift` (`player_id`,`last_play`,`last_gift`) VALUES (?,?,?)";
                                  Object[] array = new Object[3];
                                   array[0] = player_id;
                                   array[1] =  WriteDate;
                                   array[2] = 0;
                                   GIFT=0;
                                   
                                  ISFSArray res6=gameExt.DBWRAPPER(query6, array, 2);
                            }
                                
                           
                            session.setProperty("wb", GIFT);
                            session.setProperty("pId", player_id);
                            session.setProperty("VIP", VIP);
                            outData = (ISFSObject) event.getParameter(SFSEventParam.LOGIN_OUT_DATA);
                            if(!FBNAME.equals("0"))outData.putUtfString(SFSConstants.NEW_LOGIN_NAME, FBNAME);
   
                          
                     }  else{
                         
                         
                         
                           if(TYPE==3){
                                int player_id=0;
                             ///////////////////////////REGISTER//////////////////////////
                                player_id=REGISTER(LOG_NEW); 
                                setDefaultProperties(LOG_NEW.getUtfString("first_name"),Long.toString(LOG_NEW.getLong("FbId")),player_id,session,3);
                                   outData.putUtfString(SFSConstants.NEW_LOGIN_NAME, "u"+player_id);
                              //////////////////////////////////////////////////////////////
                              
                        
                           }else if(TYPE==2){
                              if(LOG_NEW.getLong("FbId")>0){
                                  
                                  ///////////////////////////REGISTER//////////////////////////
                                  int player_id=0;
                                  player_id=REGISTER(LOG_NEW); 
                                  setDefaultProperties(LOG_NEW.getUtfString("first_name"),Long.toString(LOG_NEW.getLong("FbId")),player_id,session,2);
                                  outData.putUtfString(SFSConstants.NEW_LOGIN_NAME, "u"+player_id);
                              //////////////////////////////////////////////////////////////
                              } 
                               
                               
                           }else{     
                                   
                                 
                                session.setProperty("uName", userName);
                                int uID=(int) Math.round(Math.random()*111500);
                                int pId=0;
                                setDefaultProperties(userName,Integer.toString(uID),pId,session, TYPE);
                                outData.putUtfString(SFSConstants.NEW_LOGIN_NAME, "u"+uID);
                           }
                       } 
                      
                   }
	       
                     
 		 
                  
           
	}
        
        
        
        private HttpURLConnection openConnection(String urlAddress) throws IOException {
 
		URL url = new URL(urlAddress);
 
		HttpURLConnection connection = (HttpURLConnection) url.openConnection();
 
		return connection;
 
	 }
        
        
        
        private HttpURLConnection postRequestData(HttpURLConnection connection) throws IOException {

		OutputStreamWriter writer = new OutputStreamWriter(

		connection.getOutputStream());

		
		writer.close();

		return connection;

	}
        
        private boolean checkHttpConnectionStatus(HttpURLConnection connection)
 
			throws IOException {
 
		int statusCode = connection.getResponseCode();
 
		if (statusCode == HttpURLConnection.HTTP_OK) {
 
			return true;
 
		} else {
                        return false;
			//throw new HttpException(statusCode);
 
		}
 
	}
        
        private SFSObject GetUser(HttpURLConnection connection)throws IOException,FBLoginException{
            SFSObject NEW_USER=null;
            if (checkHttpConnectionStatus(connection)) {
                     
			JsonObject jsonResponse = readJsonResponse(connection);
                              
                    
                        if (checkJsonResponse(jsonResponse)) {
                            
                            NEW_USER = processJsonResponse(jsonResponse);
                            
                        }
                    

		}
           return  NEW_USER;
        }
        
        
        private SFSObject processJsonResponse(JsonObject jsonResponse) {
            
            
                SFSObject NEW_USER=new SFSObject();
                NEW_USER.putLong("FbId", jsonResponse.get("id").getAsLong());
                NEW_USER.putUtfString("email", jsonResponse.get("email").getAsString());
                NEW_USER.putUtfString("first_name", jsonResponse.get("first_name").getAsString());
                NEW_USER.putUtfString("gender", jsonResponse.get("gender").getAsString());
                NEW_USER.putUtfString("last_name", jsonResponse.get("last_name").getAsString());
                NEW_USER.putUtfString("link", jsonResponse.get("link").getAsString());                        
                NEW_USER.putUtfString("locale", jsonResponse.get("locale").getAsString());
                NEW_USER.putUtfString("name", jsonResponse.get("name").getAsString());
                NEW_USER.putInt("timezone", jsonResponse.get("timezone").getAsInt());
                NEW_USER.putUtfString("time", jsonResponse.get("updated_time").getAsString());
                NEW_USER.putInt("b1", 10);
                NEW_USER.putInt("b2", 10);
                NEW_USER.putInt("b3", 10);
                NEW_USER.putInt("b4", 10);
                NEW_USER.putInt("b5", 10);
                
              
              return NEW_USER;
	}
        
        private SFSObject SetNewUser(){
            
                SFSObject NEW_USER=new SFSObject();
                NEW_USER.putLong("FbId", 0);
                NEW_USER.putUtfString("email", "");
                NEW_USER.putUtfString("first_name", "guest");
                NEW_USER.putUtfString("gender", "not_set");
                NEW_USER.putUtfString("last_name", "");
                NEW_USER.putUtfString("link", "");                        
                NEW_USER.putUtfString("locale", "");
                NEW_USER.putUtfString("name", "guest");
                NEW_USER.putInt("timezone", 0);
                NEW_USER.putUtfString("time", "not set");
                return NEW_USER;
            
            
        }
        
        private JsonObject readJsonResponse(HttpURLConnection connection)

			throws IOException {

		String response = readResponse(connection);
                JsonObject json = new JsonParser().parse(response).getAsJsonObject();
		//JSONObject json = parseResponse(response);

		return json;

	}
        
         private String readResponse(HttpURLConnection connection)

			throws IOException {

		BufferedReader reader = new BufferedReader(new InputStreamReader(

				connection.getInputStream()));

		StringBuilder sb = new StringBuilder();

		String line;

		while ((line = reader.readLine()) != null) {

			sb.append(line);

		}

		reader.close();
                 LudoExtension gameExt = (LudoExtension) getParentExtension();
                
		return sb.toString();

	}
         
         private boolean checkJsonResponse(JsonObject jsonResponse)

			throws FBLoginException {
              boolean verified=jsonResponse.get("verified").getAsBoolean();
              
               // if (verified) {
                    
                    return true;

               /* } else {
                    JsonObject ERROR=jsonResponse.get("error").getAsJsonObject();
                    int status=ERROR.get("code").getAsInt();
                    throw new FBLoginException(status);
                     
                }
           */

	}
         
         
         private int getPlayerFromDeviceID(String device_id){
             
             int ID=0;
             LudoExtension gameExt = (LudoExtension) getParentExtension();
              
             String query="SELECT * FROM `device_users` WHERE `device_id`='"+device_id+"'";
             ISFSArray res=gameExt.DBWRAPPER(query, null, 1);
               if (res.size()>0) {
                    ISFSObject RESSET=res.getSFSObject(0);
                    ID=RESSET.getInt("player_id");
                        
                }
                   
             return ID;
             
         }
         
         
         private int REGISTER(ISFSObject LOG_NEW){
                 
                  LudoExtension gameExt = (LudoExtension) getParentExtension();
                  IDBManager dbManager=gameExt.getParentZone().getDBManager();
                   Connection connection=null;
                   int player_id=0;  
                    
               try  {
                   
                   connection=dbManager.getConnection();
                   String queryQF="INSERT INTO `game_state` (`coins`, `score`, `wins`, `plays`, `cron_date`, `coins_balance`, `vip_date`) VALUES ("+gameExt.startCoins+",0,0,0,NOW(),0,NOW());";
                   Statement pstm;
                    ResultSet rs = null;
                    pstm = connection.createStatement();
                    pstm.executeUpdate(queryQF, Statement.RETURN_GENERATED_KEYS);//MYSQL

                       rs = pstm.getGeneratedKeys();
                        while (rs.next()){
                            String key = rs.getString(1);
                            player_id=Integer.parseInt(key);
                         }
                          
                                        String query13="INSERT INTO `facebook_players` (`player_id`,`facebook_id`,`token`, `name`, `first_name`, `last_name`,`player_name`, `email`, `link`, `gender`, `locale`, `date`) VALUES (?,?,?,?,?,?,?,?,?,?,?,?);";
                                        
                                        Object[] array13 = new Object[12];
                                         array13[0] = player_id;
                                         array13[1] = LOG_NEW.getLong("FbId");
                                         array13[2] = LOG_NEW.getUtfString("token");
                                         array13[3] = LOG_NEW.getUtfString("name"); 
                                         array13[4] = LOG_NEW.getUtfString("first_name"); 
                                         array13[5] = LOG_NEW.getUtfString("last_name"); 
                                         array13[6] = ""; 
                                         array13[7] = LOG_NEW.getUtfString("email"); 
                                         array13[8] = ""; 
                                         array13[9] = LOG_NEW.getUtfString("gender"); 
                                         array13[10] = LOG_NEW.getUtfString("locale");
                                          
                                          long time = System.currentTimeMillis();
                                         array13[11] =  new java.sql.Timestamp(time);; 
                                        dbManager.executeInsert(query13,array13); 
                                        String query12="INSERT INTO `game_settings` (`player_id`,`hints`,`music_volume`,`sound_volume`) values(?,?,?,?)";
                                        
                                         Object[] array12 = new Object[4];
                                         array12[0] = player_id;
                                         array12[1] = 0;
                                         array12[2] = 70;
                                         array12[3] = 70;
                                         dbManager.executeInsert(query12,array12); 
                                          
                                         String boosterSql = "INSERT INTO `booster` (`player_id`,`booster_type`,`booster_count`) VALUES (?, ?, ?)";
                                         PreparedStatement pstmt = connection.prepareStatement(boosterSql);
                                         for(int B=1;B<6;B++){
                                             pstmt.setInt(1,player_id);
                                             pstmt.setInt(2,B);
                                             pstmt.setInt(3,10);
                                             pstmt.addBatch();
                                         }
                                         pstmt.executeBatch(); 
                                         
                                         
                                         String animSql = "INSERT INTO `animation` (`player_id`,`type`,`type_kind`,`active`) VALUES (?, ?, ?, ?)";
                                         PreparedStatement pstmta = connection.prepareStatement(animSql);
                                         for(int B=1;B<9;B++){
                                             pstmta.setInt(1,player_id);
                                             pstmta.setInt(2,B);
                                             pstmta.setInt(3,1);
                                             pstmta.setInt(4,1);
                                             pstmta.addBatch();
                                         }
                                         pstmta.executeBatch(); 
                                                 
                                     }catch(SQLException sqle){
                                        trace("ERROR "+sqle);
                                        sqle.printStackTrace();


                                    }finally{
                                         trace("FINALLY");
                                           try{
                                              if(connection!=null) connection.close();
                                           }catch(SQLException sqle)
                                           {
                                                trace("It shouldnt happen connection not closed");
                                           }
                                    }
             
             
             return player_id;
         }
        
         private void setDefaultProperties(String uname,String FbId, int player_id, ISession session, int TYPE){
             
                           session.setProperty("uName", uname);
                           
                           session.setProperty("uId",FbId );
                           session.setProperty("uThumb", "a");
                           session.setProperty("lT", TYPE);//ludo Type
                           
                           session.setProperty("pId", player_id);
                           session.setProperty("plays", 0);
                                session.setProperty("points", 0);
                                session.setProperty("wins", 0);
                                session.setProperty("money", 1000);
                                session.setProperty("b_1", 10);
                                session.setProperty("b_2", 10);
                                session.setProperty("b_3", 10);
                                session.setProperty("b_4", 10);
                                session.setProperty("b_5", 10);
                                session.setProperty("b_6", 0);
                                session.setProperty("b_7", 0);
                                session.setProperty("b_8", 0);
                                session.setProperty("b_9", 0);
                                session.setProperty("b_10",0);
                                session.setProperty("a_1", 1);
                                session.setProperty("a_2", 1);
                                session.setProperty("a_3", 1);
                                session.setProperty("a_4", 1);
                                session.setProperty("a_5", 1);
                                session.setProperty("wb", 0);
                                session.setProperty("sound", 70);
                                session.setProperty("music", 70);
                                session.setProperty("hints", 0);
                                session.setProperty("color", -1);
                                session.setProperty("VIP", 0);
                                session.setProperty("rM", 0);
                                Integer Z=0;
                                session.setProperty("CD", Z.longValue());// CRON DATE
             
             
             
         }
         
         private void createDeviceUser(int player_id,String DEVICEID){
             
                  LudoExtension gameExt = (LudoExtension) getParentExtension();
                  
                   String queryQF="INSERT INTO `device_users` (`player_id`, `device_id`) VALUES (?,?);";
                   Object[] array = new Object[2];
                    array[0] = player_id;
                    array[1] = DEVICEID;
                   ISFSArray res = gameExt.DBWRAPPER(queryQF, array, 2);                             
             
         }
         
}
