package sfs2x.extensions.games.ludo;


import com.smartfoxserver.v2.annotations.Instantiation;
import com.smartfoxserver.v2.annotations.Instantiation.InstantiationMode;
import com.smartfoxserver.v2.entities.Room;
import com.smartfoxserver.v2.entities.User;
import com.smartfoxserver.v2.entities.data.ISFSArray;
import com.smartfoxserver.v2.entities.data.ISFSObject;
import com.smartfoxserver.v2.entities.data.SFSArray;
import com.smartfoxserver.v2.entities.data.SFSObject;
import com.smartfoxserver.v2.entities.variables.RoomVariable;
import com.smartfoxserver.v2.entities.variables.SFSRoomVariable;
import com.smartfoxserver.v2.entities.variables.SFSUserVariable;
import com.smartfoxserver.v2.entities.variables.UserVariable;
import com.smartfoxserver.v2.exceptions.SFSRuntimeException;
import com.smartfoxserver.v2.exceptions.SFSVariableException;
import com.smartfoxserver.v2.extensions.BaseClientRequestHandler;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.Iterator;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;


@Instantiation(InstantiationMode.SINGLE_INSTANCE)
public class setBomb extends BaseClientRequestHandler
{
	 
	
	@Override
	public void handleClientRequest(User user, ISFSObject params)
	{
		// Check params
               
		
                  LudoExtension gameExt = (LudoExtension) getParentExtension();
                  ISFSObject resObj = new SFSObject();
                 
                  int fromRoom = user.getVariable("playIn").getIntValue();
                  String fbId=user.getVariable("uId").getStringValue();
                  Room gameRoom=gameExt.getParentZone().getRoomById(fromRoom);
                   roomInfo ROOM= gameExt.gRooms.get(fromRoom);
                  int TURN=gameRoom.getVariable("Trn").getIntValue();
                  int BOMBPLACE=(int)params.getInt("p");
                  String[] DECK = gameRoom.getVariable("DCK").getStringValue().split(","); 
                  
                  
                  
                  if(TURN==user.getPlayerId()&&checkBombLegitimation(BOMBPLACE)){
                       
                      // DECK[BOMBPLACE]="-2";
                       ROOM.allPawns[BOMBPLACE]=-2;
                      // String putArr=Arrays.toString(DECK);
                      // putArr=putArr.replace(" ","");
                       //RoomVariable DCK=new SFSRoomVariable("DCK",putArr.substring(1,putArr.length()-1));
                      // gameRoom.setVariables(Arrays.asList(DCK));
                       
                       ISFSObject respObj = new SFSObject();
		        respObj.putInt("p", BOMBPLACE);
                        respObj.putInt("c", TURN);
                       send("sB", respObj, gameRoom.getUserList());
                      try {
                          int plId=user.getVariable("pId").getIntValue();
                          int B1=user.getVariable("B_1").getIntValue();
                                     if(B1>0){
                                         B1-=1;
                                         UserVariable B_1 = new SFSUserVariable("B_1",B1,true);
                                         getApi().setUserVariables(user, Arrays.asList(B_1));
                                          roomInfo room=gameExt.gRooms.get(fromRoom);
                                          room.bombs[BOMBPLACE]=TURN;
                                         gameExt.sendStatisticB(fbId,plId,"bomb",1,room.GID,user.getVariable("LT").getIntValue());
                                     }
                      } catch (IOException ex) {
                       Logger.getLogger(LudoExtension.class.getName()).log(Level.SEVERE, null, ex);
                      }
                      
                  }else{
                      trace("NOT YOUR TURN");
                  }
                  
                  
                			 
	}
        
        Boolean checkBombLegitimation(int BOMBPLACE){
            Boolean legitime=true;
              LudoExtension gameExt = (LudoExtension) getParentExtension();
            for(int t=0;t<gameExt.teleports.length;t++){
                if(BOMBPLACE==gameExt.teleports[t]){
                    legitime=false;
                    return legitime;
                    
                }
            }
            for(int e=0;e<gameExt.extraT.length;e++){
                if(BOMBPLACE==gameExt.extraT[e]){
                    legitime=false;
                    return legitime;
                    
                }
            }
            
            for(int s=0;s<4;s++){
                if(BOMBPLACE== 7*(s-1)+1){
                    legitime=false;
                    return legitime;
                    
                }
            }
            
            
            return legitime;
        }
	

}
