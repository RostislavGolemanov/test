package sfs2x.extensions.games.ludo;

import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import com.smartfoxserver.v2.annotations.Instantiation;
import com.smartfoxserver.v2.annotations.Instantiation.InstantiationMode;

import com.smartfoxserver.v2.entities.User;
import com.sun.org.apache.xerces.internal.impl.dv.util.Base64;

import java.security.InvalidKeyException;
import java.security.KeyFactory;
import java.security.NoSuchAlgorithmException;
import java.security.PublicKey;
import java.security.Signature;
import java.security.SignatureException;
import java.security.spec.InvalidKeySpecException;
import java.security.spec.X509EncodedKeySpec;

//import org.apache.commons.codec.binary.Base64;

@Instantiation(InstantiationMode.SINGLE_INSTANCE)
class ANDROID_Purchase1 extends PurchaseClass {

	private static final String KEY_FACTORY_ALGORITHM = "RSA";
	private static final String BASE_64_PUBLIC_KEY = "MIIBIjANBgkqhkiG9w0BAQEFAAOCAQ8AMIIBCgKCAQEAglhzHY3hQMNQQf/8hA04kgicBPduf9sbg2v/621RmjaJ1AU6rsK3BzRAF1VOXDDO5zzBuHypla0xy6ma0MMWM1atc0vqLyps+lb3r1oVCzDx7me95GMb2FFHjeKg14UeBBHuJbAWVRaasjzyvYXz5XXc7NDivZf/25IuCNszVb6XH5RClKJIM0EkkTc5vQvuxy0W2iI1HAfFaV2xoTogt77KfwFU6GxQeEnkqIMPatc6VXGaH1vj1WbWwgZt8UULBuhVkRuABjdmhe9v/gz+Csvj+isr7a65mE41tZ2e8hHquFBhgipk4+k04qwhXarsEv5Gm9QpfFcY18a75NCftwIDAQAB";
	private static final String SIGNATURE_ALGORITHM = "SHA1withRSA";

	public ANDROID_Purchase1(User user, String receipt) {

		LudoExtension gameExt = (LudoExtension) user.getZone().getExtension();
		NewPayment PAYMENT = null;

		PAYMENT = processJsonResponse(receipt, user, gameExt);
		String PID = (String) PAYMENT.getProductId();

		if (PAYMENT.getVerifeid()) {
			trackPayment(PAYMENT, user, gameExt);
			giveReward(PID, user, gameExt);
		}
	}

	private static JsonObject readJsonResponse(String receipt) {

		JsonObject json = new JsonParser().parse(receipt).getAsJsonObject();
		return json;

	}

	private NewPayment processJsonResponse(String receipt, User user, LudoExtension gameExt) {

		JsonObject JSON = readJsonResponse(receipt);
		JsonObject extra = readJsonResponse(receipt);
		String OriginalJson = extra.get("originalJson").getAsString();
		String signature = extra.get("signature").getAsString();

		NewPayment payment = processReceipt(extra, user, gameExt);

		payment.setItemId(0);
		payment.setType("--");

		payment.setRDate("--");
		payment.setReceipt(extra);
		Boolean VRF = verify(OriginalJson, signature);
		payment.setIsVerified(VRF);
		return payment;

	}

	private NewPayment processReceipt(JsonObject extra, User user, LudoExtension gameExt) {
		try {
			String OriginalJsonS = extra.get("originalJson").getAsString();
			JsonObject OriginalJson = readJsonResponse(OriginalJsonS);
			String packageId = OriginalJson.get("productId").getAsString();
			String transactionId = OriginalJson.get("orderId").getAsString();

			boolean isSuccess = false;

			if (checkTransactionId(transactionId, gameExt)) {

				isSuccess = true;

			}

			NewPayment payment = createPaymentEntity(OriginalJson, isSuccess, gameExt);

			return payment;
		} catch (Exception ex) {
			return null;
			// Logger.getLogger(IOS_Purchase.class.getName()).log(Level.SEVERE,
			// null, ex);
		}

	}

	private NewPayment createPaymentEntity(JsonObject receipt, boolean isVerified, LudoExtension gameExt) {
		try {
			NewPayment payment = new NewPayment();

			String packageId = receipt.get("productId").getAsString();

			payment.setPackageId(packageId);

			String transactionId = receipt.get("orderId").getAsString();

			payment.setTransactionId(transactionId);

			Double packagePrice = IOS_PaymentUtil.getPackagePrice(packageId);

			payment.setPackagePrice(packagePrice);

			String bundle = receipt.get("packageName").getAsString();

			payment.setBundle(bundle);

			payment.setMarket("ANDROID");

			payment.setIsVerified(isVerified);

			payment.setPDate("");

			payment.setQuantity(1);

			return payment;
		} catch (Exception ex) {
			return null;
			// Logger.getLogger(IOS_Purchase.class.getName()).log(Level.SEVERE,
			// null, ex);
		}

	}

	public static PublicKey generatePublicKey(String encodedPublicKey) {
		try {
			byte[] decodedKey = Base64.decode(encodedPublicKey);
			KeyFactory keyFactory = KeyFactory.getInstance(KEY_FACTORY_ALGORITHM);
			return keyFactory.generatePublic(new X509EncodedKeySpec(decodedKey));
		} catch (NoSuchAlgorithmException e) {
			throw new RuntimeException(e);
		} catch (InvalidKeySpecException e) {
			throw new IllegalArgumentException(e);
		}
	}

	public static boolean verify(String signedData, String signature) {
		Signature sig;
		PublicKey key = generatePublicKey(BASE_64_PUBLIC_KEY);

		try {
			sig = Signature.getInstance(SIGNATURE_ALGORITHM);
			sig.initVerify(key);
			sig.update(signedData.getBytes());
			byte[] decoded = Base64.decode(signature);
			if (!sig.verify(decoded)) {

				return false;
			}
			System.out.println(key);
			return true;
		} catch (NoSuchAlgorithmException e) {
			e.printStackTrace();
			System.out.println("1   " + e);
		} catch (InvalidKeyException e) {
			e.printStackTrace();
			System.out.println("2   " + e);
		} catch (SignatureException e) {
			e.printStackTrace();
			System.out.println("3   " + e);
		}

		return false;
	}

}
