package sfs2x.extensions.games.ludo;

import com.smartfoxserver.v2.entities.User;
import com.smartfoxserver.v2.entities.data.ISFSObject;
import com.smartfoxserver.v2.entities.data.SFSObject;
import com.smartfoxserver.v2.extensions.BaseClientRequestHandler;

public class getMoney extends BaseClientRequestHandler {

	@Override
	public void handleClientRequest(User user, ISFSObject params) {
		ISFSObject obj = new SFSObject();
		int allMoney = user.getVariable("money").getIntValue() + user.getVariable("rM").getIntValue();
		obj.putInt("allmoney", allMoney);
		send("money", obj, user);
	}
}
