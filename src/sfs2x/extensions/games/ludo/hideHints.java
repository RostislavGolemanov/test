package sfs2x.extensions.games.ludo;


import com.smartfoxserver.v2.annotations.Instantiation;
import com.smartfoxserver.v2.annotations.Instantiation.InstantiationMode;
import com.smartfoxserver.v2.db.IDBManager;
import com.smartfoxserver.v2.entities.Room;
import com.smartfoxserver.v2.entities.User;
import com.smartfoxserver.v2.entities.data.ISFSObject;
import com.smartfoxserver.v2.entities.data.SFSObject;
import com.smartfoxserver.v2.entities.variables.SFSUserVariable;
import com.smartfoxserver.v2.entities.variables.UserVariable;
import com.smartfoxserver.v2.exceptions.SFSRuntimeException;
import com.smartfoxserver.v2.extensions.BaseClientRequestHandler;
import java.sql.Connection;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.Arrays;
import java.util.logging.Level;
import java.util.logging.Logger;


@Instantiation(InstantiationMode.SINGLE_INSTANCE)
public class hideHints extends BaseClientRequestHandler
{
	 
	
	@Override
	public void handleClientRequest(User user, ISFSObject params)
	{
		// Check params
               
		
                  LudoExtension gameExt = (LudoExtension) getParentExtension();
                  IDBManager dbManager = gameExt.getParentZone().getDBManager();
                  
		   int player_id=user.getVariable("pId").getIntValue();
                   
                   String query="UPDATE `game_settings`  SET `hints`=0 WHERE `player_id`= ?";
                   Object[] array = new Object[1];
                   array[0] = player_id;
                   UserVariable H = new SFSUserVariable("H",0,true);
                   getApi().setUserVariables(user, Arrays.asList(H));
        
            try {
                dbManager.executeUpdate(query,array);
            } catch (SQLException ex) {
                Logger.getLogger(LudoExtension.class.getName()).log(Level.SEVERE, null, ex);
            }
                			 
	}
	

}
