package sfs2x.extensions.games.ludo;


import com.smartfoxserver.v2.annotations.Instantiation;
import com.smartfoxserver.v2.annotations.Instantiation.InstantiationMode;
import com.smartfoxserver.v2.entities.Room;
import com.smartfoxserver.v2.entities.User;
import com.smartfoxserver.v2.entities.data.ISFSArray;
import com.smartfoxserver.v2.entities.data.ISFSObject;
import com.smartfoxserver.v2.entities.data.SFSArray;
import com.smartfoxserver.v2.entities.data.SFSObject;
import com.smartfoxserver.v2.entities.variables.RoomVariable;
import com.smartfoxserver.v2.entities.variables.SFSRoomVariable;
import com.smartfoxserver.v2.entities.variables.SFSUserVariable;
import com.smartfoxserver.v2.entities.variables.UserVariable;
import com.smartfoxserver.v2.exceptions.SFSRuntimeException;
import com.smartfoxserver.v2.exceptions.SFSVariableException;
import com.smartfoxserver.v2.extensions.BaseClientRequestHandler;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.Iterator;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;


@Instantiation(InstantiationMode.SINGLE_INSTANCE)
public class setInvisible extends BaseClientRequestHandler
{
	 
	
	@Override
	public void handleClientRequest(User user, ISFSObject params)
	{
		// Check params
               
		if (!params.containsKey("p")){
                        throw new SFSRuntimeException("Invalid request SET INVISIBLE, one mandatory param is missing. Required 'p'");
                }
                  LudoExtension gameExt = (LudoExtension) getParentExtension();
                  ISFSObject resObj = new SFSObject();
                 
                  int fromRoom = user.getVariable("playIn").getIntValue();
                  String fbId=user.getVariable("uId").getStringValue();
                  Room gameRoom=gameExt.getParentZone().getRoomById(fromRoom);
                   
                  int TURN=gameRoom.getVariable("Trn").getIntValue();
                  int invPlace=(int)params.getInt("p");
                   
                  
                  
                  if(TURN==user.getPlayerId()){
                       
                       
                       
                       ISFSObject respObj = new SFSObject();
		        respObj.putInt("p", invPlace);
                        respObj.putInt("c", TURN);
                       send("inv", respObj, gameRoom.getUserList());
                      
                       
                       
                      try {
                          int plId=user.getVariable("pId").getIntValue();
                          int B5=user.getVariable("B_5").getIntValue();
                                     if(B5>0){
                                         B5-=1;
                                         UserVariable B_5 = new SFSUserVariable("B_5",B5,true);
                                         getApi().setUserVariables(user, Arrays.asList(B_5));
                                         roomInfo room=gameExt.gRooms.get(fromRoom);
                                         setGhostPlace(TURN,room,gameExt.ghostDuration,invPlace);
                                          gameExt.sendStatisticB(fbId,plId,"invisible",5,room.GID,user.getVariable("LT").getIntValue());
                                     }
                      } catch (IOException ex) {
                       Logger.getLogger(LudoExtension.class.getName()).log(Level.SEVERE, null, ex);
                      }
                  }else{
                      trace("NOT YOUR TURN");
                  }
                  
                  
                			 
	}
        
      private void setGhostPlace(int TURN,roomInfo ROOM,int ghostDuration,int invPlace){
          
          for(int g=0;g<3;g++){
              if(ROOM.ghosts[TURN-1][g]==-1){
                  ROOM.ghosts[TURN-1][g]=(ghostDuration+1)*1000+invPlace;
                  break;
              }
          }
          
      } 

}
