package sfs2x.extensions.games.ludo;


import com.smartfoxserver.v2.annotations.Instantiation;
import com.smartfoxserver.v2.annotations.Instantiation.InstantiationMode;
import com.smartfoxserver.v2.entities.Room;
import com.smartfoxserver.v2.entities.User;
import com.smartfoxserver.v2.entities.data.ISFSObject;
import com.smartfoxserver.v2.entities.data.SFSObject;
import com.smartfoxserver.v2.entities.variables.SFSUserVariable;
import com.smartfoxserver.v2.entities.variables.UserVariable;
import com.smartfoxserver.v2.exceptions.SFSRuntimeException;
import com.smartfoxserver.v2.extensions.BaseClientRequestHandler;
import java.io.IOException;
import java.util.Arrays;
import java.util.logging.Level;
import java.util.logging.Logger;


@Instantiation(InstantiationMode.SINGLE_INSTANCE)
public class busterKill extends BaseClientRequestHandler
{
	 
	
	@Override
	public void handleClientRequest(User user, ISFSObject params)
	{
		// Check params
               
		if (!params.containsKey("p")){
                        throw new SFSRuntimeException("Invalid request KILL BUSTER, one mandatory param is missing. Required 'p'");
                }
                  LudoExtension gameExt = (LudoExtension) getParentExtension();
                  ISFSObject resObj = new SFSObject();
                 
                  int fromRoom = user.getVariable("playIn").getIntValue();
                  String fbId=user.getVariable("uId").getStringValue();
                  Room gameRoom=gameExt.getParentZone().getRoomById(fromRoom);
                  int DICENUM=gameRoom.getVariable("dN").getIntValue();
                  roomInfo ROOM= gameExt.gRooms.get(fromRoom);
                  int TURN=gameRoom.getVariable("Trn").getIntValue();
                  int KILLPLACE=(int)params.getInt("p");
                  int rc=0;
                  Boolean FOUND=false;
                  if(TURN==user.getPlayerId()){
                        
                       if(ROOM.allPawns[KILLPLACE]>10) {
                                   
                                       
                                        
                                        int SHIELD=gameExt.checkShield(KILLPLACE, ROOM, TURN);
                                        int SH2=SHIELD;
                                        Boolean Hit=false;
                                        int LEAVE=0;
                                        
                                             
                                            int FOUNDCOLOR2=(int) Math.floor(ROOM.allPawns[KILLPLACE]/10);
                                            int pawnNum2=ROOM.allPawns[KILLPLACE]-FOUNDCOLOR2*10;
                                            
                                            
                                    if (FOUNDCOLOR2 != TURN&&SH2<1) { ///////HIT
                                                
                                             if(ROOM.tweenNum[FOUNDCOLOR2-1]>1&&ROOM.allPawns[KILLPLACE]>FOUNDCOLOR2*10+1){
                                                    ROOM.tweens[FOUNDCOLOR2-1] = 0;
                                                    ROOM.tweenNum[FOUNDCOLOR2-1]= 1;
                                                }
                                                
                                                ROOM.allPawns[KILLPLACE]--;
                                                
                                                ROOM.pawnsOUT[FOUNDCOLOR2-1]++;
                                                if(ROOM.allPawns[KILLPLACE]==FOUNDCOLOR2*10)ROOM.allPawns[KILLPLACE]=-1;
                                                 if(ROOM.sticky[KILLPLACE]>0){
                                                     ROOM.sticky[KILLPLACE]=0;
                                                  }
                                                
                                                 rc=ROOM.removeMoveCount(FOUNDCOLOR2, KILLPLACE);
                                                 gameExt.removeGhost(ROOM, FOUNDCOLOR2, KILLPLACE);
                                                Hit=true;

                                
                                
                                FOUND=true;
                                 ISFSObject respObj = new SFSObject();
		                 respObj.putInt("p", KILLPLACE);
                                 respObj.putInt("c", user.getPlayerId());
                                 respObj.putInt("r", rc);
                                 respObj.putUtfString("mc", ROOM.getMoveCountAll());
                                 send("kB", respObj, gameRoom.getUserList());
                                        
                                 try {
                                     int plId=user.getVariable("pId").getIntValue();
                                     int B3=user.getVariable("B_3").getIntValue();
                                     if(B3>0){
                                         B3-=1;
                                         UserVariable B_3 = new SFSUserVariable("B_3",B3,true);
                                         getApi().setUserVariables(user, Arrays.asList(B_3));
                                         gameExt.sendStatisticB(fbId,plId,"kill",3,ROOM.GID,user.getVariable("LT").getIntValue());
                                     }
                                  } catch (IOException ex) {
                                   Logger.getLogger(LudoExtension.class.getName()).log(Level.SEVERE, null, ex);
                                  }
                                 }
                         }
                       
                      
                  }else{
                      gameExt.trace("NOT YOUR TURN TO KILL");
                  }
                  
                  if(!FOUND){
                      
                       ISFSObject respObj2 = new SFSObject();
		        respObj2.putUtfString("D", Arrays.toString(ROOM.allPawns));
                        respObj2.putUtfString("I", Arrays.toString(ROOM.pawnsIn[0])+"|"+Arrays.toString(ROOM.pawnsIn[1])+"|"+Arrays.toString(ROOM.pawnsIn[2])+"|"+Arrays.toString(ROOM.pawnsIn[3]));
                        respObj2.putInt("DC", DICENUM);
                        respObj2.putInt("T", TURN);
                        String shArr=Arrays.toString(ROOM.shieldPlace[0])+"|"+Arrays.toString(ROOM.shieldPlace[1])+"|"+Arrays.toString(ROOM.shieldPlace[2])+"|"+Arrays.toString(ROOM.shieldPlace[3]);
                            shArr=shArr.replace(" ","");
                        respObj2.putUtfString("SH", shArr);
                        String gsArr=Arrays.toString(ROOM.ghosts[0])+"|"+Arrays.toString(ROOM.ghosts[1])+"|"+Arrays.toString(ROOM.ghosts[2])+"|"+Arrays.toString(ROOM.ghosts[3]);
                            gsArr=gsArr.replace(" ","");
                     respObj2.putUtfString("GH", gsArr);
                        respObj2.putUtfString("GM", Arrays.toString(ROOM.tweenNum));
                        respObj2.putUtfString("H", ROOM.pawnsOUT[0]+"|"+ROOM.pawnsOUT[1]+"|"+ROOM.pawnsOUT[2]+"|"+ROOM.pawnsOUT[3]);
                        respObj2.putUtfString("TD", Arrays.toString(ROOM.teleportDirection));   
                       send("ERR", respObj2, user);
                  }
                			 
	}
	

}
