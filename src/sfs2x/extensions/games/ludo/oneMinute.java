package sfs2x.extensions.games.ludo;


import com.smartfoxserver.v2.annotations.Instantiation;
import com.smartfoxserver.v2.annotations.Instantiation.InstantiationMode;
import com.smartfoxserver.v2.entities.Room;
import com.smartfoxserver.v2.entities.User;
import com.smartfoxserver.v2.entities.data.ISFSObject;
import com.smartfoxserver.v2.entities.data.SFSObject;
import com.smartfoxserver.v2.entities.variables.SFSUserVariable;
import com.smartfoxserver.v2.entities.variables.UserVariable;
import com.smartfoxserver.v2.extensions.BaseClientRequestHandler;
import java.io.IOException;
import java.util.Arrays;
import java.util.logging.Level;
import java.util.logging.Logger;


@Instantiation(InstantiationMode.SINGLE_INSTANCE)
public class oneMinute extends BaseClientRequestHandler
{
	 
	
	@Override
	public void handleClientRequest(User user, ISFSObject params)
	{
		// Check params
               
		    
                    String fbId=user.getVariable("uId").getStringValue();
                    int fromRoom = user.getVariable("playIn").getIntValue();
                     
                     
                    LudoExtension gameExt = (LudoExtension) getParentExtension();
                    Room gameRoom=gameExt.getParentZone().getRoomById(fromRoom);
                    roomInfo ROOM= gameExt.gRooms.get(fromRoom);
                    int TURN=gameRoom.getVariable("Trn").getIntValue();
                    if(TURN==user.getPlayerId()){
                        int B6=user.getVariable("B_6").getIntValue();
                           if(B6>0){
                               
                               B6-=1;
                               UserVariable B_6 = new SFSUserVariable("B_6",B6,true);
                               getApi().setUserVariables(user, Arrays.asList(B_6));
                                 ROOM.gameTime+=60;
                                 try {
                                     int plId=user.getVariable("pId").getIntValue();
                                     gameExt.sendStatisticB(fbId,plId,"one_minute",6,ROOM.GID,user.getVariable("LT").getIntValue());
                                      
                                 } catch (IOException ex) {
                                   Logger.getLogger(LudoExtension.class.getName()).log(Level.SEVERE, null, ex);
                                 }
                                ISFSObject respObj = new SFSObject();
		                  
                                 respObj.putInt("c", user.getPlayerId());
                                 respObj.putInt("gt", ROOM.gameTime);
                                 send("oM", respObj, gameRoom.getUserList());
                               
                           }
                    }
                    
                    
                			 
	}
        
        
        
        
        
        
        
        
	

}
