package sfs2x.extensions.games.ludo;


import com.smartfoxserver.v2.annotations.Instantiation;
import com.smartfoxserver.v2.annotations.Instantiation.InstantiationMode;
import com.smartfoxserver.v2.entities.Room;
import com.smartfoxserver.v2.entities.User;
import com.smartfoxserver.v2.entities.data.ISFSArray;
import com.smartfoxserver.v2.entities.data.ISFSObject;
import com.smartfoxserver.v2.entities.data.SFSArray;
import com.smartfoxserver.v2.entities.data.SFSObject;
import com.smartfoxserver.v2.entities.variables.RoomVariable;
import com.smartfoxserver.v2.entities.variables.SFSRoomVariable;
import com.smartfoxserver.v2.entities.variables.SFSUserVariable;
import com.smartfoxserver.v2.entities.variables.UserVariable;
import com.smartfoxserver.v2.exceptions.SFSRuntimeException;
import com.smartfoxserver.v2.exceptions.SFSVariableException;
import com.smartfoxserver.v2.extensions.BaseClientRequestHandler;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.Iterator;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;


@Instantiation(InstantiationMode.SINGLE_INSTANCE)
public class setShield extends BaseClientRequestHandler
{
	 
	
	@Override
	public void handleClientRequest(User user, ISFSObject params)
	{
		// Check params
               
		if (!params.containsKey("p")){
                        throw new SFSRuntimeException("Invalid request SET SHIELD, one mandatory param is missing. Required 'p'");
                }
                  LudoExtension gameExt = (LudoExtension) getParentExtension();
                  ISFSObject resObj = new SFSObject();
                 
                  int fromRoom = user.getVariable("playIn").getIntValue();
                  String fbId=user.getVariable("uId").getStringValue();
                  Room gameRoom=gameExt.getParentZone().getRoomById(fromRoom);
                  roomInfo RI=gameExt.gRooms.get(fromRoom);
                  int TURN=gameRoom.getVariable("Trn").getIntValue();
                  int SHIELDPLACE=(int)params.getInt("p");
                  //String SHIELDS = gameRoom.getVariable("sS").getStringValue();
                  
                  
                  
                  //if(TURN==user.getPlayerId()){
                       
                       int timeStay=gameExt.shieldInt;
                       //SHIELDS+=SHIELDPLACE+"|"+user.getPlayerId()+"|"+RI.Round+",";
                        
                           for(int S=0;S<3;S++){
                            if(RI.shields[user.getPlayerId()-1][S]==-1){
                                RI.shields[user.getPlayerId()-1][S]=RI.Round;
                                RI.shieldPlace[user.getPlayerId()-1][S]=SHIELDPLACE;
                                int pawnNum2=RI.allPawns[SHIELDPLACE]-user.getPlayerId()*10;
                                RI.moveShield[SHIELDPLACE]=pawnNum2;
                                RI.HAVESHIELD++;
                                break;
                            }
                           }
                       
                       ISFSObject respObj = new SFSObject();
		        respObj.putInt("p", SHIELDPLACE);
                        respObj.putInt("c", user.getPlayerId());
                        send("sS", respObj, gameRoom.getUserList());
                       
                       try {
                                   int plId=user.getVariable("pId").getIntValue();
                                   int B2=user.getVariable("B_2").getIntValue();
                                     if(B2>0){
                                         B2-=1;
                                         UserVariable B_2 = new SFSUserVariable("B_2",B2,true);
                                         getApi().setUserVariables(user, Arrays.asList(B_2));
                                         
                                         gameExt.sendStatisticB(fbId,plId,"shield",2,RI.GID,user.getVariable("LT").getIntValue());
                                     }
                       } catch (IOException ex) {
                        Logger.getLogger(LudoExtension.class.getName()).log(Level.SEVERE, null, ex);
                       }
               
                			 
	}
	

}
